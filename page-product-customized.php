<?php require_once 'header.php';?>
<?php
$product_id   = mitch_get_product_id_by_slug(sanitize_text_field($_GET['product_name']));
$product_data = mitch_get_product_data($product_id);
mitch_validate_customized_product($product_data);
$branches_ids = mitch_get_branches_ids();
// WC()->cart->add_to_cart(25, 2);
// $variation_id         = 126;
// $variation_attributes = array(
//   "attribute_pa_%d8%a7%d9%84%d9%8a%d8%a7%d9%82%d8%a9"       => "",
//   "attribute_pa_%d8%a7%d9%84%d9%83%d9%85"                   => "",
//   "attribute_pa_%d8%a7%d9%84%d8%a3%d8%b2%d8%b1%d8%a7%d8%b1" => "%d9%81%d8%b1%d9%86%d8%b3%d9%8a",
//   "attribute_pa_%d8%a7%d9%84%d8%ac%d9%8a%d8%a8"             => ""
// );
// WC()->cart->add_to_cart($product_id, 1, $variation_id, wc_get_product_variation_attributes( $variation_id ));
// wc_print_notices();
// echo '<pre>';
// var_dump();
// echo '</pre>';
// WC()->cart->empty_cart();
// global $woocommerce;
// $amount = floatval(preg_replace('#[^\d]#', '', $woocommerce->cart->get_cart_total()));
// echo '<pre>';
// var_dump();
// echo '</pre>';
// $product_obj  = get_page_by_path($product_name, OBJECT, 'product');
//mitch_test_vars(array($product_data['main_data']->get_available_variations())); //$product_data['main_data']->get_children()
?>
<div id="page" class="site" >
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_customize">
    <div class="step one active">
      <div class="header_customize">
        <div class="grid">
          <div class="content_header_customize">
            <div class="nav_customize">
              <ul class="breadcramb">
                  <?php
                    $attributes_keys    = array();
                    $attributes_names   = array();
                    $attributes_count   = 2;
                    if(!empty($product_data['main_data']->get_attributes())){
                    foreach($product_data['main_data']->get_attributes() as $attribute_key => $attribute_obj){
                      $attribute_name = str_replace('pa_', '', $attribute_obj['name']);
                      $attributes_keys[]  = $attribute_key;
                      $attributes_names[] = $attribute_name;
                  ?>
                  <li class="step-nav-<?php echo mitch_get_number_name($attributes_count);?> disabled" data-value="<?php echo $attribute_name;?>">
                    <p><?php echo $attribute_name;?></p>
                    <div class="min_box emty">
                      <img src="">
                    </div>
                  </li>
                  <?php
                  $attributes_count++; }  } ?>
              </ul>
            </div>
            <div class="item_info_customize">
              <div class="sec_item">
                <div class="img">
                  <img src="<?php echo $product_data['images']['thumb'][0];?>" alt="">
                </div>
                <div class="text">
                  <h4><?php echo $product_data['main_data']->get_name();?></h4>
                  <p><?php echo $product_data['main_data']->get_price();?> <?php echo $theme_settings['current_currency'];?></p>
                  <a href="<?php echo $product_data['product_url'];?>">رجوع للمنتج</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="hover_page">
        <div class="grid">

          <div class="content">
            <div class="sec_loader">
              <span class="loader"></span>
            </div>
            <div class="right">
              <div class="title_step_one">
                <img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'>
                <h2>
                  <p class="customize_subtitle"><?php echo $fixed_string['checkout_steps_page_title'];?></p>
                  <?php echo $fixed_string['checkout_step_one_sizes'];?>
                  <span class="border_step_one_title"></span>
                </h2>
              </div>
              <p>
                <?php echo $fixed_string['product_custom_title_1'];?>
                <span>
                  <?php echo $fixed_string['product_custom_title_2'];?>
                  <strong><?php echo $fixed_string['product_custom_title_3'];?></strong>
                  <?php echo $fixed_string['product_custom_title_4'];?>
                </span>
              </p>
              <form action="">
                <div class='option'>
                  <input type="radio" onchange="checkout_location();" id="home_checkbox" name="demo" value="home_checkbox" data-id="home_checkbox" checked>
                  <label for="home_checkbox"><?php echo $fixed_string['product_custom_home_visit'];?></label>
                  <div class="home_checkbox_content date">
                    <p for="date"><?php echo $fixed_string['global_date_field_name'];?></p>
                    <input id="date" type="date" name="date">
                  </div>
                </div>
                <div class='option'>
                  <input type="radio" onchange="checkout_location();" id="branch_checkbox" name="demo" value="branch_checkbox" data-id="branch_checkbox">
                  <label for="branch_checkbox">
                    <?php echo $fixed_string['product_custom_branch_booking'].' ('.count($branches_ids).' '.$fixed_string['product_custom_branchs_available'].')';?>
                  </label>
                  <div class="branch_checkbox_content branch select_arrow" style="display:none;">
                    <p for="branch"><?php echo $fixed_string['checkout_step_one_branch'];?></p>
                    <select name="branch" id="branches_select">
                      <option value=""><?php echo $fixed_string['product_custom_choose_branch'];?></option>
                      <?php

                      if(!empty($branches_ids)){
                        foreach($branches_ids as $branch_id){
                          $branch_name = get_the_title($branch_id);
                          ?>
                          <option data-branch-address="<?php echo trim(get_field('branch_details_address', $branch_id));?>" value="<?php echo $branch_name;?>"><?php echo $branch_name;?></option>
                          <?php
                        }
                      }
                      ?>
                    </select>
                    <p id="branch_address" class="note"></p>
                  </div>
                </div>
                <button id="next_button" class="disabled" type="button"><?php echo $fixed_string['product_custom_continue_button'];?></button>
              </form>
            </div>
            <div class="left">
              <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/step_one_img.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($attributes_keys)){
      $keys_count  = 0;
      $class_count = 2;
      foreach($attributes_keys as $attribute_key){
        ?>
        <div class="step <?php echo mitch_get_number_name($class_count);?>" data-step="<?php echo $attributes_names[$keys_count];?>" data-next="<?php echo $attributes_names[$keys_count + 1];?>">
          <div class="header_customize">
            <div class="grid">
              <div class="content_header_customize">
                <div class="nav_customize">
                  <ul class="breadcramb">
                      <?php
                        $attributes_keys    = array();
                        $attributes_names   = array();
                        $attributes_count   = 2;
                        if(!empty($product_data['main_data']->get_attributes())){
                        foreach($product_data['main_data']->get_attributes() as $attribute_key => $attribute_obj){
                          $attribute_name = str_replace('pa_', '', $attribute_obj['name']);
                          $attributes_keys[]  = $attribute_key;
                          $attributes_names[] = $attribute_name;
                      ?>
                      <li class="step-nav-<?php echo mitch_get_number_name($attributes_count);?> disabled" data-value="<?php echo $attribute_name;?>">
                        <p><?php echo $attribute_name;?></p>
                        <div class="min_box emty">
                          <img src="">
                        </div>
                      </li>
                      <?php
                      $attributes_count++; }  } ?>
                  </ul>
                </div>
                <div class="item_info_customize">
                  <div class="sec_item">
                    <div class="img">
                      <img src="<?php echo $product_data['images']['thumb'][0];?>" alt="">
                    </div>
                    <div class="text">
                      <h4><?php echo $product_data['main_data']->get_name();?></h4>
                      <p><?php echo $product_data['main_data']->get_price();?> <?php echo $theme_settings['current_currency'];?></p>
                      <a href="<?php echo $product_data['product_url'];?>">رجوع للمنتج</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hover_page">
            <div class="grid">

              <div class="content">
                <div class="sec_loader">
                  <span class="loader"></span>
                </div>
                <div class="title">
                  <img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'>
                  <h2><p class="customize_subtitle"><?php echo $fixed_string['checkout_steps_page_title'];?></p>أختار نوع  <?php echo $attributes_names[$keys_count];?></h2>
                </div>
                <div class="slide_customize">
                  <?php
                  mitch_get_customized_product_variations_steps($product_data['main_data']->get_available_variations(), $attributes_keys[$keys_count], mitch_get_number_name($class_count), $class_count);
                  //$product_data['main_data']->get_children()
                  ?>
                </div>
              </div>
            </div>
          </div>
          <div class="customize_button_step">
                <?php if($keys_count == count($attributes_keys) - 1){ ?>
                  <div class="single_button">
                    <button id="customized_product_add_to_cart" style="display:none;" class="button_last_step" type="button"><?php echo $fixed_string['product_single_page_add_to_cart'];?></button>
                  </div>
                  <?php } else { ?>
                <div class="single_button">
                  <button class="disabled" id="next" type="button"><?php echo $fixed_string['product_custom_next_button'];?></button>
                </div>
                <?php } ?>
                <div class="single_button">
                  <button id="prev" type="button"><?php echo $fixed_string['product_custom_prev_button'];?></button>
                </div>
          </div>

        </div>
        <?php
        $keys_count++;
        $class_count++;
      }
    }
    ?>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script>
var attrs_count = <?php echo $attributes_count;?> - 2;
var parent_id   = <?php echo $product_id;?>;
customized_product_add_to_cart(attrs_count, parent_id);
checkout_location();
next_button_proceed();
</script>
