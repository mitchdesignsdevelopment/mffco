<?php
function mitch_get_product_id_by_slug($product_slug)
{
	global $wpdb;
	return $wpdb->get_row("SELECT ID FROM wp_posts WHERE post_name = '$product_slug'")->ID;
}

function mitch_validate_single_product($product_obj)
{
	if (empty($product_obj->get_price()) || $product_obj->get_status() != 'publish') {
		wp_redirect(home_url());
		exit;
	}
}

function mitch_validate_customized_product($product_data)
{
	if (
		empty($product_data['main_data']->get_price())        ||
		$product_data['main_data']->get_type() != 'variable'  ||
		$product_data['main_data']->get_status() != 'publish' ||
		$product_data['extra_data']['product_customized'] == false
	) {
		wp_redirect(home_url());
		exit;
		// global $wp_query;
		// $wp_query->set_404();
		// status_header(404);
		// get_template_part(404);
		//exit();
	}
}

function mitch_get_product_data($product_id)
{
	$main_data = wc_get_product($product_id);
	return array(
		'main_data'  => $main_data,
		'video_url' => get_field('video_url', $product_id),
		'video_thumbnail' => get_field('video_thumbnail', $product_id),
		'images'     => mitch_get_product_images($main_data->get_image_id(), $main_data->get_gallery_image_ids())
	);
}

function mitch_get_product_images($featured_image_id, $gallery_images_ids)
{
	// var_dump(wp_get_attachment_image($featured_image_id, array(100, 100)));
	// exit;
	$product_full_images   = array();
	$product_thum_images   = array();
	// $product_full_images[] = wp_get_attachment_image($featured_image_id, array(600, 600));//wp_get_attachment_image_src($featured_image_id, 'full');
	// $product_thum_images[] = wp_get_attachment_image($featured_image_id, array(100, 100));//wp_get_attachment_image_src($featured_image_id, 'thumbnail');
	if (!empty($gallery_images_ids)) {
		foreach ($gallery_images_ids as $gallery_image_id) {
			$product_full_images[] = wp_get_attachment_image_src($gallery_image_id, 'full')[0]; //wp_get_attachment_image($gallery_image_id, array(600, 600));
			$product_thum_images[] = wp_get_attachment_image_src($gallery_image_id, 'thumbnail')[0]; //wp_get_attachment_image($gallery_image_id, array(100, 100));
		}
	}
	return array(
		'full'  => $product_full_images,
		'thumb' => $product_thum_images
	);
}

function mitch_get_product_attribute_name($attribute_slug)
{
	global $wpdb;
	return $wpdb->get_row("SELECT name FROM wp_terms WHERE slug = '$attribute_slug'")->name;
}

function mitch_get_product_attribute_name_by_id($attribute_id)
{
	global $wpdb;
	return $wpdb->get_row("SELECT name FROM wp_terms WHERE term_id = $attribute_id")->name;
}





function mitch_get_short_product_data($product_id)
{
	$product = wc_get_product($product_id);
	global $theme_settings;
	// if(has_term('variable','product_type',$product_id)){
	// 	$variation_id = 0;
	// 	$product = wc_get_product($product_id);
	// 	$default_attributes = $product->get_default_attributes();
	// 	// Loop through available variations
	// 	foreach($product->get_available_variations() as $variation){
	// 		 $found = true; // Initializing
	// 		 // Loop through variation attributes
	// 		 foreach( $variation['attributes'] as $key => $value ){
	// 			  $taxonomy = str_replace( 'attribute_', '', $key );
	// 			  // Searching for a matching variation as default
	// 			  if( isset($default_attributes[$taxonomy]) && $default_attributes[$taxonomy] != $value ){
	// 					$found = false;
	// 					break;
	// 			  }
	// 		 }
	// 		 // When it's found we set it and we stop the main loop
	// 		 if( $found ) {
	// 			  $variation_id = $variation['variation_id'];
	// 			  break;
	// 		 } // If not we continue
	// 		 else {
	// 			  continue;
	// 		 }
	// 	}

	// 	return array(
	// 		'product_id'            => $product_id,
	// 		'product_title'         => get_the_title($product_id),
	// 		'product_price'         => get_post_meta($variation_id, '_price', true),
	// 		'product_price_b4_sale' => get_post_meta($variation_id, '_regular_price', true),
	// 		'product_sale_price' 	 => get_post_meta($variation_id, '_sale_price', true),
	// 		'product_image'         => wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0],
	// 		'product_url'           => get_permalink($product_id),
	// 	 );
	// }else{
	return array(
		'product_id'            => $product_id,
		'product_title'         => get_the_title($product_id),
		'product_type'         => $product->get_type(),
		'product_price'         => $product->get_price_html(),
		'product_widget_background'  => get_field('widget_background_color', $product_id),
		'hide_add_to_cart'  => get_field('hide_add_to_cart_button', $product_id),
		'product_cat'       => get_the_terms($product_id, 'product_cat'),

		'product_price_b4_sale' => $product->get_price(),
		'product_sale_price' 	 => $product->get_sale_price(),
		'product_image'         => (wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0]) ? wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0] : $theme_settings['theme_url'] . '/assets/img/place-holder.jpg',
		'product_url'           => get_permalink($product_id),
	);
	// }
}

function mitch_get_products_by_category($category, $page_type, $number_of_posts)
{

	if ($page_type == "filter-child") {
		// print_r($category_id);
		if (is_array($category)) :
			if ($category['child']) {
				$category_id = $category['child'];
				$category_parent = $category['parent'];
				if ($category_id->parent != 0) {
					if ($category_parent) :
						$parent = $category_parent;
					endif;
				}
			}
		endif;
		$category_id = $category_id->term_id;
	} elseif ($page_type == "filter-parent") {
		$category_id = $category->term_id;
	} else {
		$category_id = $category;
	}
	$products_ids = array(
		'numberposts' => $number_of_posts,
		'fields'      => 'ids',
		// 'cat'		  => $term_id,
		'post_type'   => 'product',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'desc',
		'relation' => 'AND',
		'tax_query'   => array(
			'relation' => 'AND',
			array(
				'taxonomy'         => 'product_visibility',
				'terms'            => array('exclude-from-catalog', 'exclude-from-search'),
				'field'            => 'name',
				'operator'         => 'Not IN',
				'include_children' => false,
			),
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'term_id',
				'terms'    => array($category_id), /*category name*/
				'operator' => 'IN',
			),
			// ,
			// array(
			// 	'taxonomy' => 'product_cat',
			// 	'field'    => 'slug',
			// 	'terms'    => $category->slug, /*category name*/
			// )
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_thumbnail_id',
				'value' => '0',
				'compare' => '>',
			),
			// array(
			// 	'key' => '_thumbnail_id',
			// 	'value' => '0',
			// 	'compare' => '=',
			// ), 
		),
	);
	// if ($term_id) {
	// 	$product_ids['cat'] = $term_id;
	// }
	if ($parent) {
		$products_ids[$parent->taxonomy] = $parent->slug;
	}
	//   print_r($products_ids);

	$products_ids = get_posts($products_ids);
	return $products_ids;
}

function mitch_get_products_list()
{
	return get_posts(array(
		'numberposts' => -1,
		'fields'      => 'ids',
		'post_type'   => 'product',
		'post_status' => 'publish',
		'relation' => 'AND',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_thumbnail_id',
				'value' => '0',
				'compare' => '>',
			),
		)
	));
}

add_action('wp_ajax_mitch_make_product_review', 'mitch_make_product_review');
add_action('wp_ajax_nopriv_mitch_make_product_review', 'mitch_make_product_review');
function mitch_make_product_review()
{
	$response       = array();
	$post_form_data = $_POST['form_data'];
	parse_str($post_form_data, $form_data);
	$product_id   = intval($form_data['product_id']);
	$name         = sanitize_text_field($form_data['name']);
	$email        = sanitize_text_field($form_data['email']);
	$comment      = sanitize_text_field($form_data['comment']);
	$rating       = sanitize_text_field($form_data['rating']);
	$comment_data = array(
		'comment_post_ID'      => $product_id,
		'comment_author'       => $name,
		'comment_author_email' => $email,
		'comment_content'      => $comment,
		'comment_date'         => current_time('Y-m-d H:i:s'),
		'user_id'              => get_current_user_id(),
		'comment_approved'     => 0,
		'comment_type'         => 'review',
	);
	$comment_id = wp_insert_comment($comment_data);
	if ($comment_id) {
		update_comment_meta($comment_id, 'rating', $rating);
		$response = array('status' => 'success', 'comment_data' => $comment_data, 'msg' => 'حفظ تقييمك وسيتم مراجعته ونشره قريبا جدا, شكرا لك.');
	} else {
		$response = array('status' => 'error');
	}
	echo json_encode($response);
	wp_die();
}

function mitch_get_bought_together_products($pids, $exclude_pids = 0)
{
	$sub_exsql2   = '';
	$all_products = array();
	$pids_count   = count($pids);
	$pid          = implode(',', $pids);
	global $wpdb, $table_prefix;
	if ($pids_count > 1 ||  ($pids_count == 1 && !$all_products = wp_cache_get('bought_together_' . $pid, 'ah_bought_together'))) {
		$subsql = "SELECT oim.order_item_id FROM " . $table_prefix . "woocommerce_order_itemmeta oim where oim.meta_key='_product_id' and oim.meta_value in ($pid)";
		$sql = "SELECT oi.order_id from  " . $table_prefix . "woocommerce_order_items oi where oi.order_item_id in ($subsql) limit 100";
		$all_orders = $wpdb->get_col($sql);
		if ($all_orders) {
			$all_orders_str = implode(',', $all_orders);
			$subsql2 = "select oi.order_item_id FROM " . $table_prefix . "woocommerce_order_items oi where oi.order_id in ($all_orders_str) and oi.order_item_type='line_item'";
			if ($exclude_pids) {
				$sub_exsql2 = " and oim.meta_value not in ($pid)";
			}
			$sql2 = "select oim.meta_value as product_id,count(oim.meta_value) as total_count from " . $table_prefix . "woocommerce_order_itemmeta oim where oim.meta_key='_product_id' $sub_exsql2 and oim.order_item_id in ($subsql2) group by oim.meta_value order by total_count desc limit 15";
			$all_products = $wpdb->get_col($sql2);
			if ($pids_count == 1) {
				wp_cache_add('bought_together_' . $pid, $all_products, 'ah_bought_together');
			}
		}
	}
	if (!empty($all_products)) {
		shuffle($all_products);
	}
	return $all_products;
}

function mitch_get_reviews_stars($rating_avg)
{
	echo '<div class="starssss">';
	if ($rating_avg == 0) {
?>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
	<?php
	} elseif ($rating_avg >= 1 && $rating_avg < 2) {
	?>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
	<?php
	} elseif ($rating_avg >= 2 && $rating_avg < 3) {
	?>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
	<?php
	} elseif ($rating_avg >= 3 && $rating_avg < 4) {
	?>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_outline</span>
		<span class="material-icons">star_outline</span>
	<?php
	} elseif ($rating_avg >= 4 && $rating_avg < 5) {
	?>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_outline</span>
	<?php
	} elseif ($rating_avg == 5) {
	?>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<span class="material-icons">star_rate</span>
		<?php
	}
	echo '</div>';
}

function mitch_remove_decimal_from_rating($rating_avg)
{
	if (in_array($rating_avg, array('1.00', '2.00', '3.00', '4.00', '5.00'))) {
		$rating_avg = intval($rating_avg);
	}
	return $rating_avg;
}

function mitch_get_best_selling_products_ids($limit)
{
	$args = array(
		'post_type'      => 'product',
		'post_status'     => 'publish',
		'meta_key'       => 'total_sales',
		'orderby'        => 'meta_value_num',
		'fields'         => 'ids',
		'posts_per_page' => $limit,
	);
	return get_posts($args);
}

function mitch_update_product_total_sales($product_id, $new_quantity)
{
	$old_total_sales = (int)get_post_meta($product_id, 'total_sales', true);
	$new_total_sales = $old_total_sales + $new_quantity;
	update_post_meta($product_id, 'total_sales', $new_total_sales);
}

function mitch_get_new_arrival_products_ids($limit)
{
	$start_date = array(
		'year'  => date("Y", strtotime("first day of previous month")),
		'month' => date("n", strtotime("first day of previous month")),
		'day'   => date("j", strtotime("first day of previous month"))
	);
	$end_date = array(
		'year'  => date("Y", strtotime("last day of this month")),
		'month' => date("n", strtotime("last day of this month")),
		'day'   => date("j", strtotime("last day of this month"))
	);
	$args = array(
		'post_type'  => 'product',
		// 'date_query' => array(
		//   array(
		//     'after'     => $start_date,
		//     'before'    => $end_date,
		//     'inclusive' => true,
		//   ),
		// ),
		'post_status'     => 'publish',
		'relation' => 'AND',
		'orderby' => 'date',
		'order' => 'DESC',
		'fields'         => 'ids',
		'posts_per_page' => $limit,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => '_thumbnail_id',
				'value' => '0',
				'compare' => '>',
			),
		),
	);
	return get_posts($args);
}

// function mitch_get_product_price_after_rate($product_price){
//   global $wpdb, $theme_settings;
//   $current_curr = $theme_settings['current_currency'];
//   $default_curr = get_field('default_currency', 'options');
//   if($current_curr == $default_curr){
//     return $product_price;
//   }else{
//     $def_rate  = $wpdb->get_row("SELECT rate FROM wp_mitch_currencies_rates WHERE code = '$default_curr'")->rate;
//     $curr_rate = $wpdb->get_row("SELECT rate FROM wp_mitch_currencies_rates WHERE code = '$current_curr'")->rate;
//     $calc_rate = $curr_rate / $def_rate;
//     return round($product_price * $calc_rate, 2);
//   }
// }

// load more products
add_action('wp_ajax_nopriv_get_products_ajax', 'get_products_ajax');
add_action('wp_ajax_get_products_ajax', 'get_products_ajax');
function get_products_ajax()
{
	global $language;
	$action = sanitize_text_field($_POST['fn_action']);
	$count    = intval($_POST['count']);
	$page     = intval($_POST['page']);
	$offset   = ($page) * $count;
	$order    = sanitize_text_field($_POST['order']);
	$type = sanitize_text_field($_POST['type']);
	$slug = sanitize_text_field($_POST['slug']);
	$cat = sanitize_text_field($_POST['cat']);
	$search = sanitize_text_field($_POST['search']);
	$ids = $_POST['ids'];
	$lang = $_POST['lang'];
	$ajaxArgs = array();
	// if($lang == "en"){
	// 	$language = "en";
	// }else{
	// 	$language = "";
	// }
	// if($type=="sale"){
	// $ajaxQuery = get_on_sale_products_ids($count,$page,$slug,78);
	// if($ajaxQuery):
	// 	foreach ($ajaxQuery as $key => $product) {
	// 		get_template_part( 'template-parts/content', 'on_sale_product_widget',$product ); 
	// 	}
	// endif;
	// }
	// else{
	if ($type == "sale") {


		$ajaxArgs = array(
			'status' => 'publish',
			'limit' => 20,
			'return' => 'ids',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => '_sale_price',
					'value' => 0,
					'compare' => '>',
					'type' => 'numeric',
				),
				array(
					'key' => '_min_variation_sale_price',
					'value' => 0,
					'compare' => '>',
					'type' => 'numeric',
				),
			),
		);
		// $products_ids = wc_get_products($ajaxArgs);
	} else {
		$ajaxArgs = array(
			"post_type" => 'product',
			'post_status' => 'publish',
			"fields" => 'ids',
			"suppress_filters" => false,
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy'         => 'product_visibility',
					'terms'            => array('exclude-from-catalog', 'exclude-from-search'),
					'field'            => 'name',
					'operator'         => 'Not IN',
					'include_children' => false,
				),
			),
			'meta_query' => array(
				'relation' => 'AND',
				// array(
				// 	'key' => '_stock_status',
				// 	'value' => 'instock',
				// 	'compare' => '=',
				// ),
				array(
					'key' => '_thumbnail_id',
					'value' => '0',
					'compare' => '>',
				),
			),
		);
	}
	if ($search != "") {
		$ajaxArgs["s"] = $search;
	}
	if ($action == "loadmore") {
		$ajaxArgs["offset"] = $offset;
		$ajaxArgs["posts_per_page"] = $count;
	} else {
		$ajaxArgs["posts_per_page"] = $offset;
	}
	if ($type == 'product_cat') {
		$ajaxArgs['product_cat'] = $_POST['slug'];
	} else if ($type == 'new') {
		$ajaxArgs['date_query'] = array(
			array(
				'after' => date('Y-m-d', strtotime('-60 days'))
			)
		);
		$ajaxArgs['meta_query'] = array(
			array(
				'key' => '_stock_status',
				'value' => 'instock',
				'compare' => '=',
			)
		);
	} elseif ($type == 'type') {
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => 'type',
			'field' => 'term_id',
			'terms' => array($_POST['slug'])
		);
	} elseif ($type == 'shop') {
	} elseif ($type == 'sale') {
		// $ajaxArgs['meta_query'] = WC()->query->get_meta_query();
		// $ajaxArgs['post__in'] = get_on_sale_products_ids_only($count,$page,'',78);
	} else if ($type == 'total-sales') {
	} else {
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => $type,
			'field' => 'term_id',
			'terms' => array($slug),
		);
	}
	if ($order == 'price') {
		$orderby = array(
			'meta_value_num' => 'asc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = '_price';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'asc';
	} else if ($order == 'price-desc') {
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = '_price';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	} else if ($order == 'featured') {
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = 'featured';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	} else if ($order == 'date') {
		// $orderby = array(
		// 	'meta_value_num' => 'desc',
		// 	'date' => 'desc',
		// );
		// $ajaxArgs['meta_key'] = 'featured';
		$ajaxArgs['orderby'] = 'date';
		$ajaxArgs['order'] = 'desc';
	} else if ($order == 'popularity') {
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = 'total_sales';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	} else if ($order == 'stock') {
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = '_stock_status';
		$ajaxArgs['orderby'] = 'meta_value';
		$ajaxArgs['order'] = 'ASC';
	} else {
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = 'total_sales';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	}
	if ($_POST['min_price'] != '') {
		$min_price = intval($_POST['min_price']);
		$max_price = intval($_POST['max_price']);
		if ($max_price != 0) {
			$ajaxArgs['meta_query'][] = array(
				'relation' => 'AND',
				array(
					'key' => '_price',
					'value' => array($min_price, $max_price),
					'compare' => 'between',
					'type' => 'numeric'
				),
			);
		} else {
			$ajaxArgs['meta_query'][] = array(
				'relation' => 'AND',
				array(
					'key' => '_price',
					'value' => $min_price,
					'compare' => '>=',
					'type' => 'numeric'
				)
			);
		}
	}
	if ($_POST['brand'] && count($_POST['brand']) > 0) {
		$brand = $_POST['brand'];
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => 'brand',
			'field' => 'slug',
			'terms' => $brand,
		);
	}
	if ($_POST['cats'] && count($_POST['cats']) > 0) {
		$cats = $_POST['cats'];
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => $cats,
		);
	}
	if ($_POST['label'] && count($_POST['label']) > 0) {
		$labels = $_POST['label'];
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => 'label',
			'field' => 'slug',
			'terms' => $labels,
		);
	}
	if ($_POST['collections'] && count($_POST['collections']) > 0) {
		$collections = $_POST['collections'];
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => 'collections',
			'field' => 'term_id',
			'terms' => $collections,
		);
	}

	$products_ids = ($type != "sale") ? get_posts($ajaxArgs) : wc_get_products($ajaxArgs);

	if (!empty($products_ids)) {
		if ($type != "sale") {
			foreach ($products_ids as $product_id) {

				$product_data = mitch_get_short_product_data($product_id);
				// print_r($product_data);
				wc_get_template('../theme-parts/product-widget.php', $product_data);
			}
		} else {
			foreach ($products_ids as $product_id) {

				$product = wc_get_product($product_id);
				$product_data = array(
					'product_id'            => $product_id,
					'product_title'         => get_the_title($product_id),
					'product_type'         => $product->get_type(),
					'product_price'         => $product->get_price_html(),
					'product_widget_background'  => get_field('widget_background_color', $product_id),
					'hide_add_to_cart'  => get_field('hide_add_to_cart_button', $product_id),
					// 'produ÷ct_price_b4_sale' => $product->get_regular_price(),
					'product_sale_price'    => $product->get_sale_price(),
					'product_image'         => (wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0]) ? wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0] : $theme_settings['theme_url'] . '/assets/img/place-holder.jpg',
					'product_url'           => get_permalink($product_id),
				);

		?>
				<li id="product_<?php echo $product_data['product_id']; ?>_block" class="product_widget" <?php if (isset($prod_anim_count)) {
																												echo 'data-aos="fade-left" data-aos-duration="' . $prod_anim_count . '"';
																											} ?>>


					<a href="<?php echo $product_data['product_url']; ?>" class="product_widget_box <?php //echo $product_data['product_widget_background']; 
																									?>">
						<div class="img">
							<img src="<?php echo $product_data['product_image']; ?>" alt="<?php echo $product_data['product_title']; ?>">
						</div>
						<div class="text">
							<div class="sec_info">
								<h3 class="title">
									<?php echo $product_data['product_title']; ?>
								</h3>
								<?php if (!$product_data['hide_add_to_cart']) : ?>
									<div class="price-content"><?php echo $product_data['product_price']; ?></div>

								<?php endif; ?>
							</div>
							<div class="open_widget">
								<span></span>
							</div>
						</div>
					</a>
				</li>
<?php
			}
		}
	}
	// }
	wp_die();
}

// load more products
add_action('wp_ajax_nopriv_get_products_ajax_count', 'get_products_ajax_count');
add_action('wp_ajax_get_products_ajax_count', 'get_products_ajax_count');

function get_products_ajax_count(/*$posts_per_page = -1*/)
{
	global $language;
	$action = sanitize_text_field($_POST['fn_action']);
	$count    = intval($_POST['count']);
	$page     = intval($_POST['page']);
	$offset   = ($page) * $count;
	$order    = sanitize_text_field($_POST['order']);
	$type = sanitize_text_field($_POST['type']);
	$slug = sanitize_text_field($_POST['slug']);
	$cat = sanitize_text_field($_POST['cat']);
	$search = sanitize_text_field($_POST['search']);
	$ids = $_POST['ids'];
	$lang = $_POST['lang'];
	$ajaxArgs = array();
	$post_per_page = 20;
	// if($lang == "en"){
	// 	$language = "en";
	// }else{
	// 	$language = "";
	// }
	if ($type == "sale") {
		$ajaxArgs = array(
			'status' => 'publish',
			'limit' => -1,
			'return' => 'ids',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => '_sale_price',
					'value' => 0,
					'compare' => '>',
					'type' => 'numeric',
				),
				array(
					'key' => '_min_variation_sale_price',
					'value' => 0,
					'compare' => '>',
					'type' => 'numeric',
				),
			),
		);
		$ajaxQuery =  wc_get_products($ajaxArgs);
	} elseif ($type == "less_100") {
		// $ajaxQuery = get_products_less_100_ids(-1,0,$slug,'');
	} else {
		$ajaxArgs = array(
			"post_type" => 'product',
			"posts_per_page" => -1,
			"fields" => 'ids',
			'post_status' => 'publish',
			"suppress_filters" => false,
			"offset" => $offset,
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy'         => 'product_visibility',
					'terms'            => array('exclude-from-catalog', 'exclude-from-search'),
					'field'            => 'name',
					'operator'         => 'Not IN',
					'include_children' => false,
				),

			),

			'meta_query' => array(
				'relation' => 'AND',
				// array(
				// 	'key' => '_stock_status',
				// 	'value' => 'instock',
				// 	'compare' => '=',
				// ),
				array(
					'key' => '_thumbnail_id',
					'value' => '0',
					'compare' => '>',
				),
			),
		);
		if ($search != "") {
			$ajaxArgs["s"] = $search;
		}
		// if($action == "loadmore"){
		// 	$ajaxArgs["offset"] = $offset;
		// 	$ajaxArgs["posts_per_page"] = $count;
		// }else{
		// 	$ajaxArgs["posts_per_page"] = $offset;
		// }
		if ($type == 'product_cat') {
			$ajaxArgs['product_cat'] = $_POST['slug'];
		} else if ($type == 'new') {
			$ajaxArgs['date_query'] = array(
				array(
					'after' => date('Y-m-d', strtotime('-30 days'))
				)
			);
		} elseif ($type == 'type') {
			$ajaxArgs['tax_query'][] = array(
				'taxonomy' => 'type',
				'field' => 'term_id',
				'terms' => array($_POST['slug'])
			);
		} elseif ($type == 'shop') {
		} elseif ($type == 'sale') {
			// $ajaxArgs['post__in'] = get_on_sale_products_ids_only(0,24,'',78);
		} else if ($type == 'products-list') {
			$ajaxArgs['post__in'] = $ids;
		} else if ($type == 'total-sales') {
		} elseif ($type == 'less_100') {
			// $ajaxArgs['post__in'] = get_products_less_100_ids_only(0,24,'','');

		} else {
			$ajaxArgs['tax_query'][] = array(
				'taxonomy' => $type,
				'field' => 'term_id',
				'terms' => array($slug),
			);
		}
		if ($order == 'price') {
			$orderby = array(
				'meta_value_num' => 'asc',
				'date' => 'desc',
			);
			$ajaxArgs['meta_key'] = '_price';
			$ajaxArgs['orderby'] = $orderby;
			$ajaxArgs['order'] = 'asc';
		} else if ($order == 'price-desc') {
			$orderby = array(
				'meta_value_num' => 'desc',
				'date' => 'desc',
			);
			$ajaxArgs['meta_key'] = '_price';
			$ajaxArgs['orderby'] = $orderby;
			$ajaxArgs['order'] = 'desc';
		} else if ($order == 'featured') {
			$orderby = array(
				'meta_value_num' => 'desc',
				'date' => 'desc',
			);
			$ajaxArgs['meta_key'] = 'featured';
			$ajaxArgs['orderby'] = $orderby;
			$ajaxArgs['order'] = 'desc';
		} else if ($order == 'date') {
			// $orderby = array(
			// 	'meta_value_num' => 'desc',
			// 	'date' => 'desc',
			// );
			// $ajaxArgs['meta_key'] = 'featured';
			$ajaxArgs['orderby'] = 'date';
			$ajaxArgs['order'] = 'desc';
		} else if ($order == 'popularity') {

			$orderby = array(
				'meta_value_num' => 'desc',
				'date' => 'desc',
			);
			$ajaxArgs['meta_key'] = 'total_sales';
			$ajaxArgs['orderby'] = $orderby;
			$ajaxArgs['order'] = 'desc';
		} else if ($order == 'stock') {
			$orderby = array(
				'meta_value_num' => 'desc',
				'date' => 'desc',
			);
			$ajaxArgs['meta_key'] = '_stock_status';
			$ajaxArgs['orderby'] = $orderby;
			$ajaxArgs['order'] = 'ASC';
		} else {
			$orderby = array(
				// 'meta_value_num' => 'desc',
				'date' => 'desc',
			);

			// $ajaxArgs['meta_key'] = 'total_sales'; 
			$ajaxArgs['orderby'] = $orderby;
			$ajaxArgs['order'] = 'desc';
		}
		if ($_POST['min_price'] != '') {
			$min_price = intval($_POST['min_price']);
			$max_price = intval($_POST['max_price']);
			if ($max_price != 0) {
				$ajaxArgs['meta_query'][] = array(
					'relation' => 'AND',
					array(
						'key' => '_price',
						'value' => array($min_price, $max_price),
						'compare' => 'between',
						'type' => 'numeric'
					),
				);
			} else {
				$ajaxArgs['meta_query'][] = array(
					'relation' => 'AND',
					array(
						'key' => '_price',
						'value' => $min_price,
						'compare' => '>=',
						'type' => 'numeric'
					)
				);
			}
		}
		if ($_POST['brand'] && count($_POST['brand']) > 0) {
			$brand = $_POST['brand'];
			$ajaxArgs['tax_query'][] = array(
				'taxonomy' => 'brand',
				'field' => 'slug',
				'terms' => $brand,
			);
		}
		if ($_POST['cats'] && count($_POST['cats']) > 0) {
			$cats = $_POST['cats'];
			// $cats_decoded = array();
			// foreach($cats as $cat_dec){
			// 	$cats_decoded[] = urldecode($cat_dec);
			// }
			// print_r($cats_decoded);
			$ajaxArgs['tax_query'][] = array(
				'taxonomy' => 'product_cat',
				'field' => 'slug',
				'terms' => $cats,
			);
		}
		if ($_POST['label'] && count($_POST['label']) > 0) {
			$labels = $_POST['label'];
			$ajaxArgs['tax_query'][] = array(
				'taxonomy' => 'label',
				'field' => 'slug',
				'terms' => $labels,
			);
		}
		if ($_POST['collections'] && count($_POST['collections']) > 0) {
			$collections = $_POST['collections'];
			$ajaxArgs['tax_query'][] = array(
				'taxonomy' => 'collections',
				'field' => 'term_id',
				'terms' => $collections,
			);
		}
		// print_r($ajaxArgs);
		// $posts=-1;
		$ajaxQuery =  get_posts($ajaxArgs);
		// $post_count=count($ajaxQuery);
		// $ajaxQuery =  get_posts($ajaxArgs(21));

	}
	if ($ajaxQuery) {
		echo count($ajaxQuery);
	} else {
		echo "0";
	}
	wp_die();
}

add_filter('woocommerce_get_price_html', 'change_displayed_sale_price_html', 10, 2);
function change_displayed_sale_price_html($price, $product)
{
	// Only on sale products on frontend and excluding min/max price on variable products
	if ($product->is_on_sale()) {
		if ($product->is_type('variable')) {
			$variation_id = 0;
			$default_attributes = $product->get_default_attributes();
			// Loop through available variations
			foreach ($product->get_available_variations() as $variation) {
				$found = true; // Initializing
				// Loop through variation attributes
				foreach ($variation['attributes'] as $key => $value) {
					$taxonomy = str_replace('attribute_', '', $key);
					// Searching for a matching variation as default
					if (isset($default_attributes[$taxonomy]) && $default_attributes[$taxonomy] != $value) {
						$found = false;
						break;
					}
				}
				// When it's found we set it and we stop the main loop
				if ($found) {
					$variation_id = $variation['variation_id'];
					break;
				} // If not we continue
				else {
					continue;
				}
			}
			if ($variation_id) {
				$product = wc_get_product($variation_id);
			} else {
				$product = wc_get_product($product->get_ID());
			}
		}
		// Get product prices
		$regular_price = (float) $product->get_regular_price(); // Regular price
		$sale_price = (float) $product->get_price(); // Active price (the "Sale price" when on-sale)

		// "Saving price" calculation and formatting
		$saving_price = round(100 - (intval($sale_price) / intval($regular_price) * 100)) . '%';
		// Append to the formated html price
		$price .= sprintf(__('<p class="saved-sale" style="display:none;">%s</p>', 'woocommerce'), $saving_price);
	}
	return $price;
}
//add_action( 'woocommerce_product_query', 'njengah_hide_products_without_image' );

//function njengah_hide_products_without_image( $query ) {
//	$query->set( 'relation','AND');
//		 $query->set( 'meta_query', array(
//			'relation' => 'AND',
//			array(
//		   'key' => '_thumbnail_id',
//		   'value' => '0',
//		   'compare' => '>'
//			)
//		),
//	 );
//  }

function wc_get_related_productss($product_id, $limit = 5, $exclude_ids = array())
{

	$product_id     = absint($product_id);
	$limit          = $limit > 0 ? $limit : 5;
	$exclude_ids    = array_merge(array(0, $product_id), $exclude_ids);
	$transient_name = 'wc_related_' . $product_id;
	$query_args     = http_build_query(array(
		'limit'       => $limit,
		'exclude_ids' => $exclude_ids,

	));

	$transient     = get_transient($transient_name);
	$related_posts = $transient && isset($transient[$query_args]) ? $transient[$query_args] : false;

	// We want to query related posts if they are not cached, or we don't have enough.
	if (false === $related_posts || count($related_posts) < $limit) {

		$cats_array = apply_filters('woocommerce_product_related_posts_relate_by_category', true, $product_id) ? apply_filters('woocommerce_get_related_product_cat_terms', wc_get_product_term_ids($product_id, 'product_cat'), $product_id) : array();
		$tags_array = apply_filters('woocommerce_product_related_posts_relate_by_tag', true, $product_id)      ? apply_filters('woocommerce_get_related_product_tag_terms', wc_get_product_term_ids($product_id, 'product_tag'), $product_id) : array();

		// Don't bother if none are set, unless woocommerce_product_related_posts_force_display is set to true in which case all products are related.
		if (empty($cats_array) && empty($tags_array) && !apply_filters('woocommerce_product_related_posts_force_display', false, $product_id)) {
			$related_posts = array();
		} else {
			$data_store    = WC_Data_Store::load('product');
			$related_posts = $data_store->get_related_products($cats_array, $tags_array, $exclude_ids, $limit + 10, $product_id);
		}

		if ($transient) {
			$transient[$query_args] = $related_posts;
		} else {
			$transient = array($query_args => $related_posts);
		}

		set_transient($transient_name, $transient, DAY_IN_SECONDS);
	}

	$related_posts = apply_filters('woocommerce_related_products', $related_posts, $product_id, array(
		'limit'        => $limit,
		'excluded_ids' => $exclude_ids,
		'relation' => 'AND',
		'meta_query', array(
			'relation' => 'AND',
			array(
				'key' => '_thumbnail_id',
				'value' => '0',
				'compare' => '>'
			)
		),
	));

	shuffle($related_posts);

	return array_slice($related_posts, 0, $limit);
}
// //////////////////////////////////////////////////////
add_filter('woocommerce_sale_flash', '__return_false');

// Removing archives prices
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

// Add the saved discounted percentage to variable products
add_filter('woocommerce_format_sale_price', 'add_sale_price_percentage', 20, 3);
// add_filter('woocommerce_format_sale_price', 'regular_sale_prices', 20, 3);

if (!function_exists('add_sale_price_percentage')) {
	function add_sale_price_percentage($price, $regular_price, $sale_price)
	{
		// Strip html tags and currency (we keep only the float number)
		$regular_price = strip_tags($regular_price);
		$regular_price = (float) preg_replace('/[^0-9.]+/', '', $regular_price);
		$sale_price = strip_tags($sale_price);
		$sale_price = (float) preg_replace('/[^0-9.]+/', '', $sale_price);

		// Percentage text and calculation
		// $percentage  = __('Save', 'woocommerce') . ' ';
		$percentage = round(($regular_price - $sale_price) / $regular_price * 100);

		// return on sale price range with "Save " and the discounted percentage
		return $price . ' <span class="save-percent" style="display:none">' . $percentage . '%</span>';
	}
}
// =========================================================================
// if (!function_exists('regular_sale_prices')) {
// 	function regular_sale_prices($price, $regular_price, $sale_price)
// 	{
// 		$prices = array();
// 		// Strip html tags and currency (we keep only the float number)
// 		$regular_price = strip_tags($regular_price);
// 		$regular_price = (float) preg_replace('/[^0-9.]+/', '', $regular_price);
// 		$sale_price = strip_tags($sale_price);
// 		$sale_price = (float) preg_replace('/[^0-9.]+/', '', $sale_price);

// 		// Percentage text and calculation
// 		// $percentage  = __('Save', 'woocommerce') . ' ';
// 		$percentage = round(($regular_price - $sale_price) / $regular_price * 100);
// 		$prices[0] = $regular_price;
// 		$prices[1] = $sale_price;
// 		// return on sale price range with "Save " and the discounted percentage
// 		// return $price . ' <span class="save-percent" style="display:none">' . $percentage . '%</span>';
// 		return $prices;
// 	}
// }
