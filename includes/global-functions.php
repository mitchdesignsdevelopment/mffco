<?php
$theme_settings = mitch_theme_settings();
require_once $theme_settings['theme_abs_url'].'languages/'.$theme_settings['current_lang'].'.php';
function mitch_get_number_name($number){
  $numbers_names_list = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten');
  return $numbers_names_list[$number];
}

function mitch_theme_settings(){
  $currency_symbol = get_woocommerce_currency(); //_symbol
  // if($currency_symbol == 'ر.ق'){
  //   $currency_symbol = 'QAR';
  // }
  return array(
    'site_url'                => site_url(),
    'theme_url'               => get_template_directory_uri(),
    'theme_abs_url'           => preg_replace('/wp-content.*$/','',__DIR__).'wp-content/themes/mffco/',
    'theme_logo'              => get_field('logo', 'options'),
    'theme_favicon'           => get_field('fav_icon', 'options'),
    'theme_favicon_black'     => get_field('fav_icon_black', 'options'),
    'current_lang'            => 'ar',
    'current_currency'        => 'EGP',
    'default_country_code'    => 'EG',
    'default_country_name'    => 'Egypt',
    'default_shipping_method' => 'filters_by_cities_shipping_method',
  );
}
function mitch_test_vars($vars){
  echo '<h2 style="direction:ltr;background: #222;color: #fff;border: 2px solid #fff;padding: 5px;margin: 5px;">Data Debug:</h2>';
  if(is_array($vars)){
    foreach($vars as $var){
      echo '<pre style="direction:ltr;background: #222;color: #fff;border: 2px solid #fff;padding: 5px;margin: 5px;">';
      var_dump($var);
      echo '</pre>';
    }
  }else{
    echo '<pre style="direction:ltr;background: #222;color: #fff;border: 2px solid #fff;padding: 5px;margin: 5px;">';
    var_dump($vars);
    echo '</pre>';
  }
}

function mitch_get_active_page_class($page_name){
  if($page_name == basename(get_permalink())){
    return 'active';
  }
}

function mitch_validate_logged_in(){
  if(!is_user_logged_in()){
    wp_redirect(home_url());
    exit;
  }
}
add_action( 'wp_ajax_nopriv_custom_search', 'custom_search' );
add_action( 'wp_ajax_custom_search', 'custom_search' );

function custom_search(){
	$lang = $_POST['lang'];
	$s = $_POST['s'];
	global $language;
	if($lang == '_en') {
		$language = 'en';
	}
	$args = array(
		'posts_per_page'  => 5,
		'post_type'       => 'product',
		's'    => $s,
		'post_status'     => 'publish',
		'meta_key' => '_stock_status',
		'orderby' => 'meta_value',
		'order' => 'ASC',
		'relation' => 'AND',
		'meta_query', array(
			'relation' => 'AND',
			array(
		   'key' => '_thumbnail_id',
		   'value' => '0',
		   'compare' => '>'
			),
		),
	);
	$new_prods1 =  new WP_Query($args);
	$restOfProd = 5 - (int)$new_prods1->post_count;
	// if($restOfProd>0){
	// 	$args = array(
	// 		'posts_per_page'  => $restOfProd,
	// 		'post_type'       => 'product',
	// 		'post_status'     => 'publish',
	// 		'meta_query' => array(
	// 			'relation'=>'OR',
	// 			array(
	// 				'key' => '_sku',
	// 				'value' => $s,
	// 				'compare' => 'LIKE'
	// 			),
	// 		),
	// 		'meta_key' => '_stock_status',
	// 		'orderby' => 'meta_value',
	// 		'order' => 'ASC',
	// 	);
	// 	$new_prods2 = new WP_Query($args);
	// 	$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count;
	// 	$restOfProd = 5 - (int)$new_prods1->post_count;
	// 	$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts),SORT_REGULAR);
	// 	if($restOfProd>0){
	// 		$args = array(
	// 			'posts_per_page'  => $restOfProd,
	// 			'post_type'       => 'product',
	// 			'post_status'     => 'publish',
	// 			'meta_query' => array(
	// 				'relation'=>'OR',
	// 					array(
	// 					'key' => 'post_content',
	// 					'value' => $s, 
	// 					'compare' => 'LIKE'
	// 				),
	// 			),
	// 			'meta_key' => '_stock_status',
	// 			'orderby' => 'meta_value',
	// 			'order' => 'ASC',
	// 		);
	// 		$new_prods3 = new WP_Query($args);
	// 		$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count;
	// 		$restOfProd = 5 - (int)$new_prods1->post_count;
	// 		$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts),SORT_REGULAR);
			
	// 	}
	// }
	if($new_prods1->have_posts()):
		while ($new_prods1->have_posts()) :
			$new_prods1->the_post(); 
			if(!empty(get_the_ID())){
          $product_data = mitch_get_short_product_data(get_the_ID());
          wc_get_template('../mffco/theme-parts/product-widget.php',$product_data);
          // include '../mffco/theme-parts/product-widget.php';
			}
		endwhile; wp_reset_postdata(); 
	else: global $language;?>
    <sec class="emty_filter">
		 <p > <?php echo ($language=="en")? 'No results found' : 'لا توجد نتائج' ; ?></p>
    </sec>
	<?php endif;
wp_die();
}
add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

// function price_format($price){
// 	$price = number_format($price,0);
// 	$price = str_replace()
// 	return $price
// }

function wc_varb_price_range( $wcv_price, $product ) {

	$prefix = sprintf('%s: ', __('From', 'wcvp_range'));
	
	$wcv_reg_min_price = $product->get_variation_regular_price( 'min', true );
	
	$wcv_min_sale_price    = $product->get_variation_sale_price( 'min', true );
	
	$wcv_max_price = $product->get_variation_price( 'max', true );
	
	$wcv_min_price = $product->get_variation_price( 'min', true );
	
	$wcv_price = ( $wcv_min_sale_price == $wcv_reg_min_price ) ?
	
	wc_price( $wcv_reg_min_price ) :
	
	'<del>' . wc_price( $wcv_reg_min_price ) . '</del>' . '<ins>' . wc_price( $wcv_min_sale_price ) . '</ins>';
	
	return ( $wcv_min_price == $wcv_max_price ) ?
	
	$wcv_price :
	
	sprintf('%s', $wcv_price);
	
	}
	
	add_filter( 'woocommerce_variable_sale_price_html', 'wc_varb_price_range', 10, 2 );
	
	add_filter( 'woocommerce_variable_price_html', 'wc_varb_price_range', 10, 2 );

	// Change the upload size to 200KB
	add_filter('upload_size_limit', 'wpse_163236_change_upload_size');
	function wpse_163236_change_upload_size()
	{
		return 10000 * 1024;
	}


// Create Taxonomy in product to label branch
add_action( 'init', 'custom_product_branch_cat' );
function custom_product_branch_cat()  {
	$labels = array(
		'name'                       => 'Product Branchs',
		'singular_name'              => 'Product Branch',
		'menu_name'                  => 'Product Branchs',
		'all_items'                  => 'All Product Branchs',
		'parent_item'                => 'Parent Product Branch',
		'parent_item_colon'          => 'Parent Product Branch:',
		'new_item_name'              => 'New Product Branch Name',
		'add_new_item'               => 'Add New Product Branch',
		'edit_item'                  => 'Edit Product Branch',
		'update_item'                => 'Update Product Branch',
		'separate_items_with_commas' => 'Separate Product Branch with commas',
		'search_items'               => 'Search Product Branchs',
		'add_or_remove_items'        => 'Add or remove Product Branch',
		'choose_from_most_used'      => 'Choose from the most used Product Branchs',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product-branchs', 'product', $args );
	register_taxonomy_for_object_type( 'product-branchs', 'product' );
}

// check select one 
add_action( 'admin_footer', 'limit_product_branch_categories_js' );
function limit_product_branch_categories_js() {
    // Check if we're on the product edit screen
    global $pagenow;
    if ( $pagenow === 'post.php' && isset( $_GET['post'] )) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                // Get the checkboxes for the product branches taxonomy
                var checkboxes = $('input[name="tax_input[product-branchs][]"]');

                // Listen for changes in the checkboxes
                checkboxes.change(function(){
                    // Count the number of checked checkboxes
                    var checkedCount = checkboxes.filter(':checked').length;

                    // If more than two checkboxes are checked, disable the unchecked ones
                    if (checkedCount >= 1) {
                        checkboxes.not(':checked').prop('disabled', true);
                    } else {
                        // Otherwise, enable all checkboxes
                        checkboxes.prop('disabled', false);
                    }
                });
            });
        </script>
        <?php
    }
}

// check select one 
add_action( 'admin_footer-edit.php', 'limit_product_branch_categories_js_on_product_listing_with_quick_view' );
function limit_product_branch_categories_js_on_product_listing_with_quick_view() {
    global $pagenow;
    if ( $pagenow === 'edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type'] === 'product' ) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                // Function to handle the checkbox logic
                function handleCheckboxes() {
                    // Get the checkboxes for the product branches taxonomy
                    var checkboxes = $('input[name="tax_input[product-branchs][]"]');
                    // Count the number of checked checkboxes
                    var checkedCount = checkboxes.filter(':checked').length;
                    // If more than one checkbox is checked, disable the unchecked ones
                    if (checkedCount >= 1) {
                        checkboxes.not(':checked').prop('disabled', true);
                    } else {
                        // Otherwise, enable all checkboxes
                        checkboxes.prop('disabled', false);
                    }
                }

                // Listen for changes in the checkboxes
                $(document).on('change', 'input[name="tax_input[product-branchs][]"]', function(){
                    handleCheckboxes();
                });

                // Listen for clicks on the "Quick View" button
                $(document).on('click', '.quick-view-button', function(){
                    // Delay execution to ensure the Quick View modal is fully loaded
                    setTimeout(function(){
                        handleCheckboxes();
                    }, 500); // Adjust delay as needed
                });
            });
        </script>
        <?php
    }
}