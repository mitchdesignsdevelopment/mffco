<?php
//branches - register custom post type
function mitch_mffco_fabric() {
	$supports = array(
		'title', // post title
		'thumbnail', // featured images
		'excerpt', // post excerpt
	);
	$labels = array(
		'name'           => _x('Fabrics', 'plural'),
		'singular_name'  => _x('Fabric', 'singular'),
		'menu_name'      => _x('Fabrics', 'admin menu'),
		'name_admin_bar' => _x('Fabrics', 'admin bar'),
		'add_new'        => _x('Add New', 'add new'),
		'add_new_item'   => __('Add New Fabric'),
		'new_item'       => __('New Fabric'),
		'edit_item'      => __('Edit Fabric'),
		'view_item'      => __('View Fabrics'),
		'all_items'      => __('All Fabrics'),
		'search_items'   => __('Search Fabric'),
		'not_found'      => __('No Fabrics found.'),
	);

	$values = array(
		'supports'     => $supports,
		'labels'       => $labels,
		'public'       => true,
		'query_var'    => true,
		'rewrite'      => array('slug' => 'fabric'),
		'has_archive'  => true,
		'hierarchical' => false,
	);
	register_post_type('fabric', $values);
}
add_action('init', 'mitch_mffco_fabric');
//branches - register custom post type
function mitch_naseej_branches() {
	$supports = array(
		'title', // post title
		'thumbnail', // featured images
		'excerpt', // post excerpt
	);
	$labels = array(
		'name'           => _x('Branches', 'plural'),
		'singular_name'  => _x('Branch', 'singular'),
		'menu_name'      => _x('Branches', 'admin menu'),
		'name_admin_bar' => _x('Branches', 'admin bar'),
		'add_new'        => _x('Add New', 'add new'),
		'add_new_item'   => __('Add New Branch'),
		'new_item'       => __('New Branch'),
		'edit_item'      => __('Edit Branch'),
		'view_item'      => __('View Branches'),
		'all_items'      => __('All Branches'),
		'search_items'   => __('Search Branch'),
		'not_found'      => __('No Branches found.'),
	);

	$values = array(
		'supports'     => $supports,
		'labels'       => $labels,
		'public'       => true,
		'query_var'    => true,
		'rewrite'      => array('slug' => 'branch'),
		'has_archive'  => true,
		'hierarchical' => false,
	);
	register_post_type('branches', $values);
}
add_action('init', 'mitch_naseej_branches');

add_action('init', 'mitch_add_category_taxonomy_to_branches', 0);
function mitch_add_category_taxonomy_to_branches(){
  // Labels part for the GUI
  $labels = array(
    'name'          => __('Branches Language'),
    'singular_name' => __('Branches Language'),
    'menu_name'     => __('Branches Language'),
  );
  // Now register the non-hierarchical taxonomy like tag
  register_taxonomy('branches_languages','branches',array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_in_rest'          => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array('slug' => 'branches_languages'),
  ));
}
//
function mitch_get_branches(){
  // global $wpdb;
  // return $wpdb->get_results("SELECT ID, post_title, post_excerpt FROM wp_posts WHERE post_type = 'branches' AND post_status = 'publish'");
	global $theme_settings;
  $args = array(
    'tax_query' => array(
      array(
        'taxonomy' => 'branches_languages',
        'field' => 'slug',
        'terms' => array(''.$theme_settings['current_lang'].'-lang')
      )
    ),
    'post_type'     =>'branches',
    'order'         => 'ASC',
    'posts_per_page'=> -1,
  );
  return get_posts($args);
}

function mitch_get_branches_ids($exclude_main = false){
	global $theme_settings;
  $args = array(
    'tax_query' => array(
      array(
        'taxonomy' => 'branches_languages',
        'field' => 'slug',
        'terms' => array(''.$theme_settings['current_lang'].'-lang')
      )
    ),
    'post_type'     =>'branches',
    'order'         => 'ASC',
    'posts_per_page'=> -1,
		'fields'        => 'ids'
  );
	if($exclude_main){
		$args['exclude'] = get_field('main_branch', 'options');
	}
  return get_posts($args);
}
function mitch_get_countries(){
	global $states_cities;
	// $WC_Countries    = new WC_Countries();
	// return $WC_Countries->get_states('EG');
	return $states_cities;
}

function mitch_get_shipping_data(){
	$zones_names      = array();
	$shipping_methods = array();
	$all_zones        = WC_Shipping_Zones::get_zones();
	if(!empty($all_zones)){
		foreach($all_zones as $zone){
			$zones_names[] = $zone['zone_name'];
			if(!empty($zone['shipping_methods'])){
				foreach($zone['shipping_methods'] as $zone_shipping_method){
					$shipping_methods[$zone_shipping_method->id] = array(
						'ins_id' => $zone_shipping_method->instance_id,
						'id'     => $zone_shipping_method->id,
						'title'  => $zone_shipping_method->title,
						'cost'   => $zone_shipping_method->cost,
						'cities' => $zone_shipping_method->cities
					);
				}
			}
		}
	}
	return array(
		'zones_names'      => $zones_names,
		'shipping_methods' => $shipping_methods
	);
}

function mitch_get_available_payment_methods_data(){
	$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
	$enabled_gateways   = [];
	if($available_gateways){
		foreach($available_gateways as $gateway){
			if($gateway->enabled == 'yes'){
				$enabled_gateways[] = array(
					'id'    => $gateway->id,
					'title' => $gateway->title,
					'obj' => $gateway,
				);
			}
		}
	}
	return $enabled_gateways;
}

add_action('wp_ajax_mitch_create_order', 'mitch_create_order');
add_action('wp_ajax_nopriv_mitch_create_order', 'mitch_create_order');
function mitch_create_order(){
	global $fixed_string, $theme_settings;
	$response       = array();
	$post_form_data = $_POST['form_data'];
	parse_str($post_form_data, $form_data);
	$user_email   = sanitize_text_field($form_data['email']);
	$phone_number = sanitize_text_field($form_data['phone']);
	// echo '<pre>';
	// var_dump($form_data);
	// echo '</pre>';
	// exit;
	if(isset($form_data['new_account_check']) && $form_data['new_account_check'] == 'yes'){
	  if(email_exists($user_email)){
	    $response = array('status' => 'error', 'msg' => $fixed_string['alert_profile_setting_exist_email']);
	  }
	  if(empty($response)){
	    $filtered_phone_number = filter_var($phone_number, FILTER_SANITIZE_NUMBER_INT);
	    if(strlen($filtered_phone_number) < 11){
	      $response = array('status' => 'error', 'code' => '401', 'msg' => $fixed_string['alert_profile_set_phone_err']);
	    }
	  }
	  if(empty($response)){
	    if(!empty(mitch_check_phone_number_exist($phone_number))){
	      $response = array('status' => 'error', 'msg' => $fixed_string['alert_profile_setting_exist_phone']);
	    }
	  }
		if(empty($response)){
			$user_password    = esc_attr($form_data['new_password']);
	    $confirm_password = esc_attr($form_data['confirm_password']);
			if(!empty($user_password) && !empty($confirm_password)){
				$pass_number       = preg_match('@[0-9]@', $user_password);
	      $pass_uppercase    = preg_match('@[A-Z]@', $user_password);
	      $pass_lowercase    = preg_match('@[a-z]@', $user_password);
	      $pass_specialChars = preg_match('@[^\w]@', $user_password);
	      if(strlen($user_password) < 8 || !$pass_number || !$pass_uppercase || !$pass_lowercase || !$pass_specialChars) {
	        $response = array('status' => 'error', 'code' => '401', 'msg' => $fixed_string['alert_profile_set_pass_validate']);
	      }else{
	        if($user_password != $confirm_password){
	          $response = array('status' => 'error', 'msg' => $fixed_string['alert_profile_set_pass_not_match2']);
	        }
	      }
			}else{
				$response = array('status' => 'error', 'msg' => $fixed_string['alert_profile_set_password_empty']);
			}
		}
		if(empty($response)){
			$result = wp_create_user($phone_number, $user_password, $user_email);
	    if(is_wp_error($result)){
	      $response = array('status' => 'error', 'msg' => $result->get_error_message());
	    }else{
				$user = get_user_by('id', $result);
	      // Add role
	      // Remove role
	      $user->remove_role('subscriber');
	      $user->remove_role('shop_manager');
	      $user->remove_role('administrator');
	      $user->add_role('customer');
	      update_user_meta($user->ID, 'first_name', sanitize_text_field($form_data['firstname']));
	      update_user_meta($user->ID, 'last_name', sanitize_text_field($form_data['lastname']));
	      update_user_meta($user->ID, 'phone_number', $phone_number);
	      wp_set_current_user($user->ID);
	      wp_set_auth_cookie($user->ID);
				if(empty(mitch_get_user_main_address($user->ID))){
					$operation = mitch_add_user_address(array(
			      'country'      => sanitize_text_field($form_data['country']),
			      'city'         => sanitize_text_field($form_data['city']),
			      'building'     => sanitize_text_field($form_data['building']),
			      'street'       => sanitize_text_field($form_data['street']),
			      'area'         => sanitize_text_field($form_data['area']),
						'address_type' => 0,
			      'user_id'      => $user->ID,
			    ));
				}
				$current_user_id = $user->ID;
			}
		}
	}else{
		$current_user_id = get_current_user_id();
	}
	if(empty($response)){
		$order_data = array(
		 'status'      => apply_filters('woocommerce_default_order_status', 'processing'),
		 'customer_id' => $current_user_id
	  );
	  $new_order = wc_create_order($order_data);
		$new_order->set_currency($theme_settings['current_currency']);
		//set Line items
		global $wpdb;
	  foreach(WC()->cart->get_cart() as $cart_item_key => $values){
			// $values['data']->set_price(mitch_get_product_price_after_rate($values['data']->price));
			mitch_update_product_total_sales($values['product_id'], $values['quantity']);
		  	$product = wc_get_product( isset($values['variation_id']) && $values['variation_id'] > 0 ? $values['variation_id'] : $values['product_id'] );
			$product->set_price($product->get_price());
			// $product->set_price(mitch_get_product_price_after_rate($product->get_price()));
		  $item_id = $new_order->add_product($product, $values['quantity']);
		  $item    = $new_order->get_item($item_id, false);
			if(!empty($values['custom_cart_data'])){
				$items_data = $values['custom_cart_data'];
				if(!empty($items_data['visit_type'])){
					update_post_meta($new_order->get_id(), 'order_visit_type', $items_data['visit_type']);
				}
				if(!empty($items_data['visit_branch'])){
					update_post_meta($new_order->get_id(), 'order_visit_branch', $items_data['visit_branch']);
				}
				if(!empty($items_data['visit_home'])){
					update_post_meta($new_order->get_id(), 'order_visit_home', $items_data['visit_home']);
				}
				$item->update_meta_data('custom_cart_data', $items_data);
				$item->set_subtotal($items_data['custom_total']);
	    		$item->set_total($items_data['custom_total']);
				if(!empty($items_data['attributes_keys'])){
					$i = 0;
					foreach($items_data['attributes_keys'] as $meta_attr_key){
						// var_dump($meta_attr_key); var_dump(); echo '<br>';
						$attr_value = $items_data['attributes_vals'][$i];
						$wpdb->query("UPDATE wp_woocommerce_order_itemmeta SET meta_value = '$attr_value' WHERE order_item_id = $item_id AND meta_key = '$meta_attr_key'");
						$i++;
					}
				}
				// echo '<pre>';
				// var_dump($values['custom_cart_data']);
				// echo '</pre>';
			}else{
				// $item->set_subtotal(mitch_get_product_price_after_rate($values['data']->price));
			}
		  $item->save();
	  }
		$coupon_code = sanitize_title($form_data['coupon_code']);
		$coupon_id   = wc_get_coupon_id_by_code($coupon_code);
		if(!empty($coupon_id)){
			$new_order->apply_coupon($coupon_code);
			update_post_meta($new_order->get_id(), 'order_applied_coupon', $coupon_code);
		}
		$new_order->calculate_totals();
	  // Coupon items
	  // if( isset($data['coupon_items'])){
	  //     foreach( $data['coupon_items'] as $coupon_item ) {
	  //         $order->apply_coupon(sanitize_title($coupon_item['code']));
	  //     }
	  // }
		$address = array(
		  'first_name' => sanitize_text_field($form_data['firstname']),
		  'last_name'  => sanitize_text_field($form_data['lastname']),
		  'company'    => '',
		  'email'      => $user_email,
		  'phone'      => $phone_number,
		  'address_1'  => sanitize_text_field($form_data['building']).', '.sanitize_text_field($form_data['street']).', '.sanitize_text_field($form_data['area']),
		  'address_2'  => '',
		  'city'       => sanitize_text_field($form_data['city']),
		  'postcode'   => '',
		  'country'    => sanitize_text_field($form_data['country']),
		  'building'   => sanitize_text_field($form_data['building']),
		  'street'     => sanitize_text_field($form_data['street']),
		  'area'       => sanitize_text_field($form_data['area'])
		);
	  $new_order->set_address($address, 'billing');
	  $new_order->set_address($address, 'shipping');
		//set shipping
		$selected_shipping_method = mitch_get_shipping_data()['shipping_methods'][explode(':', $form_data['shipping_method'][0])[0]];
		$shipping_item = new WC_Order_Item_Shipping();
	  $shipping_item->set_method_title($selected_shipping_method['title']);
	  $shipping_item->set_method_id($selected_shipping_method['id']); // set an existing Shipping method rate ID // was flat_rate:12
	  $shipping_item->set_instance_id($selected_shipping_method['ins_id']); // set an existing Shipping method rate ID // was flat_rate:12
	  $shipping_item->set_total((float)$selected_shipping_method['cost']); // (optional)
	  $new_order->add_item($shipping_item);
		//set payment
		$new_order->set_payment_method(WC()->payment_gateways->payment_gateways()[$form_data['payment_method'][0]]);
		//set notes
		if(!empty($form_data['notes'])){
			$new_order->add_order_note(sanitize_text_field($form_data['notes']));
			update_post_meta($new_order->get_id(), 'customer_notes', sanitize_text_field($form_data['notes']));
		}
		$new_order->calculate_totals();
		if(!empty($current_user_id)){
			if(empty(get_user_meta($current_user_id, 'phone_number', true))){
				update_user_meta($current_user_id, 'phone_number', sanitize_text_field($form_data['phone']));
			}
			if(empty(get_user_meta($current_user_id, 'first_name', true))){
				update_user_meta($current_user_id, 'first_name', sanitize_text_field($form_data['firstname']));
			}
			if(empty(get_user_meta($current_user_id, 'last_name', true))){
				update_user_meta($current_user_id, 'last_name', sanitize_text_field($form_data['lastname']));
			}
			update_post_meta($new_order->get_id(), '_customer_user', $current_user_id);
		}
		update_post_meta($new_order->get_id(), 'order_processing_date', $new_order->get_date_created()->date("F j, Y"));
		// echo '<pre>';
		// var_dump($product->get_price());
		// echo '</pre>';
		// exit;
		WC()->cart->empty_cart();
		if(!empty($new_order->get_id())){
			$response = array('status'   => 'success', 'order_id' => $new_order->get_id(), 'redirect_to' => home_url('thankyou/?order_id='.$new_order->get_id().''));
		}
	}
	echo json_encode($response);
	wp_die();
}
function mitch_create_order_from($order_id){
	if(!empty($order_id)){
    $r_order_obj     = wc_get_order($order_id);
    $order_data      = array(
		 'status'        => apply_filters('woocommerce_default_order_status', 'processing'),
		 'customer_id'   => get_current_user_id()
	  );
	  $new_order = wc_create_order($order_data);
		//set Line items
		global $wpdb;
	  foreach($r_order_obj->get_items() as $cart_item_key => $values){
			mitch_update_product_total_sales($values['product_id'], $values['quantity']);
		  $product = wc_get_product( isset($values['variation_id']) && $values['variation_id'] > 0 ? $values['variation_id'] : $values['product_id'] );
		  $item_id = $new_order->add_product($product, $values['quantity']);
		  $item    = $new_order->get_item($item_id, false);
			if(!empty($values['custom_cart_data'])){
				$items_data = $values['custom_cart_data'];
				if(!empty($items_data['visit_type'])){
					update_post_meta($new_order->get_id(), 'order_visit_type', $items_data['visit_type']);
				}
				if(!empty($items_data['visit_branch'])){
					update_post_meta($new_order->get_id(), 'order_visit_branch', $items_data['visit_branch']);
				}
				if(!empty($items_data['visit_home'])){
					update_post_meta($new_order->get_id(), 'order_visit_home', $items_data['visit_home']);
				}
				$item->update_meta_data('custom_cart_data', $items_data);
				$item->set_subtotal($items_data['custom_total']);
	    	$item->set_total($items_data['custom_total']);
				if(!empty($items_data['attributes_keys'])){
					$i = 0;
					foreach($items_data['attributes_keys'] as $meta_attr_key){
						// var_dump($meta_attr_key); var_dump(); echo '<br>';
						$attr_value = $items_data['attributes_vals'][$i];
						$wpdb->query("UPDATE wp_woocommerce_order_itemmeta SET meta_value = '$attr_value' WHERE order_item_id = $item_id AND meta_key = '$meta_attr_key'");
						$i++;
					}
				}
				// echo '<pre>';
				// var_dump($values['custom_cart_data']);
				// echo '</pre>';
			}
		  $item->save();
	  }
		$new_order->calculate_totals();
    //set address details
    $address = array(
			'first_name' => $r_order_obj->get_billing_first_name(),
		  'last_name'  => $r_order_obj->get_billing_last_name(),
		  'company'    => '',
		  'email'      => $r_order_obj->get_billing_email(),
		  'phone'      => $r_order_obj->get_billing_phone(),
		  'address_1'  => $r_order_obj->get_billing_address_1(),
		  'address_2'  => '',
		  'city'       => $r_order_obj->get_billing_city(),
		  'postcode'   => '',
		  'country'    => $r_order_obj->get_billing_country(),
			'building'   => get_post_meta($order_id, '_billing_building', true),
			'street'     => get_post_meta($order_id, '_billing_street', true),
			'area'       => get_post_meta($order_id, '_billing_area', true)
		);
	  $new_order->set_address($address, 'billing');
	  $new_order->set_address($address, 'shipping');
		//set shipping
    $shipping_methods = mitch_get_shipping_data()['shipping_methods'];
    $shipping_item    = new WC_Order_Item_Shipping();
    if(!empty($shipping_methods)){
      foreach($shipping_methods as $shipping_method){
        if($shipping_method['title'] == $r_order_obj->get_shipping_method()){
          $shipping_item->set_method_title($shipping_method['title']);
      	  $shipping_item->set_method_id($shipping_method['id']); // set an existing Shipping method rate ID // was flat_rate:12
      	  $shipping_item->set_instance_id($shipping_method['ins_id']); // set an existing Shipping method rate ID // was flat_rate:12
      	  $shipping_item->set_total((float)$shipping_method['cost']); // (optional)
      	  $new_order->add_item($shipping_item);
          break;
        }
      }
    }
		//set payment
    if(!empty($r_order_obj->get_payment_method())){
      $new_order->set_payment_method(WC()->payment_gateways->payment_gateways()[$r_order_obj->get_payment_method()]);
    }
		//set notes
    $old_customer_notes = sanitize_text_field(get_post_meta($order_id, 'customer_notes', true));
		if(!empty($old_customer_notes)){
			$new_order->add_order_note($old_customer_notes);
			update_post_meta($new_order->get_id(), 'customer_notes', $old_customer_notes);
		}
		$new_order->calculate_totals();
		update_post_meta($new_order->get_id(), 'order_processing_date', $new_order->get_date_created()->date("F j, Y"));
		update_post_meta($new_order->get_id(), 'auto_created_from', $order_id);
		return $new_order->get_id();
  }
	return;
}

global $states_cities, $states,$cities_st;
if(have_rows('states_cities','option')){
	while(have_rows('states_cities','option')){
		the_row();
		$states['ar'][get_sub_field('state_en','option')] = get_sub_field('state_ar','option');
		$states['en'][get_sub_field('state_en','option')] = get_sub_field('state_en','option');
				$cities_ar = (get_sub_field('cities_ar','option'))? explode(",",get_sub_field('cities_ar','option')) : array();
				$cities_en = (get_sub_field('cities_en','option'))? explode(",",get_sub_field('cities_en','option')) : array();
				if($cities_en){
					//echo '<div style="display:none">'.var_dump($cities_en).'</div>';
					for($i=0;$i<=count($cities_en);$i++){
						if($cities_ar[$i]){
							$state = get_sub_field('state_en','option');
							$states_cities['ar'][$state][$cities_en[$i]] = $cities_ar[$i];
							$states_cities['en'][$state][$cities_en[$i]] = $cities_en[$i];
						}
					}
				}
				// else{
				// 	$states_cities['ar'][$state][$cities_en] = array();
				// 	$states_cities['en'][$state][$cities_en] = array();
				// }
		
	};
				// if($_GET['new']=="test"):	echo '<div class="jims" style="display:none">'.print_r($states_cities).'<br>'.print_r($cities_st).'</div>'; endif;
};
// print_r($states);
function custom_woocommerce_states( $states_s ) {
global $language,$states;
 $states_s['EG'] = ($language=="en")? $states['en'] : $states['ar'];
  return $states_s;
}
add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );

function change_default_checkout_country() {
  return 'EG'; // country code
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
	global $language, $states;
	WC()->customer->set_billing_country('EG');

	global $current_user;
	get_currentuserinfo();
	unset($fields['shipping']['shipping_first_name']);
	unset($fields['shipping']['shipping_last_name']);
	unset($fields['shipping']['shipping_phone']);
	unset($fields['shipping']['shipping_email']);
	unset($fields['shipping']['shipping_company']);
	unset($fields['shipping']['shipping_postcode']);
	unset($fields['shipping']['shipping_city']);
	unset($fields['shipping']['shipping_country']);
	unset($fields['shipping']['shipping_state']);
	unset($fields['shipping']['shipping_address_1']);
	unset($fields['shipping']['shipping_address_2']);
	unset($fields['shipping']['shipping_address_name']);
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_country']);
	unset($fields['shipping']['shipping_address_1']);
	unset($fields['shipping']['shipping_address_2']);
	$fields['billing']['billing_phone']['default']= $current_user->phone;
	

	
	// Adding new Building field
    // $fields['billing']['billing_building'] = array(
    //     'label' => _x('Building No.', 'label', 'woocommerce'), // Add custom field placeholder
    //     'required' => true, // if field is required or not
    //     'type' => 'text', // add field type
    //     // 'priority'=> 80,
    //     'placeholder'=> '',
    //     'class'=> array('form-row-customII','building-number')
	// );
	// Adding new notes field

    $fields['billing']['billing_notes'] = array(
        'label' => _x('Message', 'label', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
        'type' => 'textarea', // add field type
         'priority'=> 50,
        'placeholder'=> '',
        'class'=> array('form-full-row')
	);
		
	/* dropdown area input in checkout page */

	$city_args = wp_parse_args( array(
		'type' => 'select',
		'options' => ($language=="en")? $states['en'] : $states['ar'],
		'input_class' => array(
			'wc-enhanced-select',
		)
	), $fields['billing']['billing_state'] );
	
	$fields['account']['account_password']['placeholder'] = '';
	$fields['billing']['billing_state'] = $city_args; // Also change for billing field
	// wc_enqueue_js( "
	// jQuery( ':input.wc-enhanced-select' ).filter( ':not(.enhanced)' ).each( function() {
	// 	var select2_args = { minimumResultsForSearch: 5 };
	// 	jQuery( this ).select2( select2_args ).addClass( 'enhanced' );
	// });" );

	// $fields['billing']['billing_first_name']['class']= array('form-row-half');
	// $fields['billing']['billing_last_name']['class']= array('form-row-half');
	// $fields['billing']['billing_phone']['class']= array('form-full-row');
	// $fields['billing']['billing_email']['class']= array('form-full-row');
	// $fields['billing']['billing_city']['class']= array('form-full-row','blocked');
	 $fields['billing']['billing_state']['class']= array('update_totals_on_change');
	 $fields['billing']['billing_city']['class']= array('update_totals_on_change');
	 $fields['billing']['billing_area']['class']= array('update_totals_on_change');
	// $fields['billing']['billing_address_1']['class']= array('form-row-customI');
	// $fields['billing']['billing_address_2']['class']= array('form-row-customII');
	// $fields['billing']['billing_country']['class'] = array('hide');

	$fields['billing']['billing_city']['required'] = true;	
	$fields['billing']['billing_area']['required'] = true;	
	$fields['billing']['billing_address_2']['required'] = false;	
	$fields['billing']['billing_address_2']['placeholder']='';
	$fields['billing']['billing_address_1']['placeholder']='';

	$fields['billing']['billing_phone']['type'] = 'number';
	// 	// set labels
	// $gift_checker = gift_checker_gift_fn();
	// $fields['billing']['shipping_gift_type'] = array(
	// 	// 'label' => _x('طريقه الشحن', 'label', 'woocommerce'), // Add custom field placeholder
	// 	'required' => false, // if field is required or not
	// 	'type' => 'radio', // add field type
	// 	'priority'=>1,
	// 	'options'         => array(
	// 		'Normal Shipping'         =>($language=="")? _x('توصيل عادي', 'label', 'woocommerce'):_x('Regular Delivery', 'label', 'woocommerce'),
	// 		'Gift Shipping'    => ($language=="")? _x('توصيل في بوكس هدية', 'label', 'woocommerce'):_x('Deliver as a Gift', 'label', 'woocommerce'),
	// 	  ),
	// 	'default' => 'Normal Shipping'
	// );
	// $fields['billing']['shipping_type'] = array(
	// 	// 'label' => _x('طريقه الشحن', 'label', 'woocommerce'), // Add custom field placeholder
	// 	'required' => false, // if field is required or not
	// 	'type' => 'radio', // add field type
	// 	'priority'=>1,
	// 	'options'         => array(
	// 		'Ship To Someone'         =>($language=="")? _x('التوصيل لشخص اخر', 'label', 'woocommerce'):_x('Ship To Someone', 'label', 'woocommerce'),
	// 		'Ship To Me'    => ($language=="")? _x('التوصيل الي عنواني', 'label', 'woocommerce'):_x('Ship To Me', 'label', 'woocommerce'),
	// 	  ),
	// 	'default' => 'Ship To Me' // for gift products only
	// );
	// $fields['billing']['recipient_fname'] = array(
	// 	'label' =>($language=="")? _x('اسم مستلم الهدية', 'label', 'woocommerce'):_x('Recipient First Name', 'label', 'woocommerce'), // Add custom field placeholder
	// 	'required' => false, // if field is required or not
	// 	'type' => 'text', // add field type
	// 	'priority'=>2
	// );
	// $fields['billing']['recipient_lname'] = array(
	// 	'label' =>($language=="")? _x('اسم العائله', 'label', 'woocommerce'):_x('Last Name', 'label', 'woocommerce'), // Add custom field placeholder
	// 	'required' => false, // if field is required or not
	// 	'type' => 'text', // add field type
	// 	'priority'=>4
	// );
	$fields['billing']['second_mobile_number'] = array(
		'label' => ($language=="")? _x('رقم موبايل آخر', 'label', 'woocommerce'):_x('Mobile Number', 'label', 'woocommerce'),// Add custom field placeholder
		'required' => true, // if field is required or not
		'type' => 'number', // add field type
		'priority'=>3
	);
	$fields['billing']['national_id'] = array(
		'label' => _x('الرقم القومي', 'label', 'woocommerce'),// Add custom field placeholder
		'required' => true, // if field is required or not
		'type' => 'number', // add field type
		'priority'=>5
	);
	$fields['billing']['greeting_card_message'] = array(
        'label' =>($language=="")? _x('رسالة كارت التهنئة', 'label', 'woocommerce') :_x('Greeting Card Message', 'label', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
		'type' => 'textarea', // add field type
		'maxlength' =>200,
        'priority'=>80,
    );

	if($language == 'en'|| $_POST['lang'] == 'en') {
		$fields['billing']['billing_phone']['label']="Mobile Number";	
		$fields['billing']['billing_first_name']['label']="First Name ";	
		$fields['billing']['billing_last_name']['label']="Last Name";	
		$fields['billing']['billing_email']['label']="Email Address ";	
		$fields['billing']['billing_city']['label']="City";	
		$fields['billing']['billing_state']['label']="State";	
		$fields['billing']['billing_area']['label']="Area";	
		$fields['billing']['billing_address_1']['label']="Details Address";
		$fields['billing']['billing_building_2']['label']="Building No.";	
		$fields['billing']['billing_building']['label']="Floor No. ";	
		$fields['billing']['billing_address_2']['label']="Apartment No.";	
		$fields['billing']['billing_address_2']['class']=array('same-row require-build');	
		$fields['account']['account_password']['label'] = 'Password';
		//$fields['billing']['billing_address_1']['description'] = 'يرجى كتابة عنوانك بالتفصيل حتى نتمكن من توصيل طلبك في أسرع وقت.';
		$fields['billing']['billing_comments']['label']="Note";
		$fields['billing']['billing_building_2'] = array(
			'label' => _x('Building No. ', 'label', 'woocommerce'), // Add custom field placeholder
			'class'	=> array('same-row require-build'),
			'required' => true, // if field is required or not
			'type' => 'text', // add field type
			'priority'=>40,
		);
		$fields['billing']['billing_building'] = array(
			'label' => _x('Floor No.  ', 'label', 'woocommerce'), // Add custom field placeholder
			'class'	=> array('same-row require-build'),
			'required' => true, // if field is required or not
			'type' => 'text', // add field type
			'priority'=>40
		);
		$fields['billing']['property_type'] = array(
			'type' => 'radio',
			'label' => _x('Property Type', 'label', 'woocommerce'), // Add custom field placeholder
			'required' => true, 
			'options' => array(
				'apart' => 'Flat',
				'villa' => 'Villa'	
			),
			'priority'=>70,
			'default' => 'apart',
			'class' => array('property-type')
		);
		
	}else{

		$fields['billing']['billing_phone']['label']="رقم الموبايل";	
		$fields['billing']['billing_first_name']['label']="الإسم الأول";	
		$fields['billing']['billing_last_name']['label']="إسم العائلة";	
		$fields['billing']['billing_email']['label']="الإيميل";	
		$fields['billing']['billing_city']['label']="المدينة";	
		$fields['billing']['billing_state']['label']="المحافظة";	
		$fields['billing']['billing_area']['label']="المنطقة";	
		$fields['billing']['billing_address_1']['label']="العنوان بالتفصيل";
		$fields['billing']['billing_building_2']['label']="رقم العقار";	
		$fields['billing']['billing_building']['label']="الدور";	
		$fields['billing']['billing_address_2']['label']="رقم الشقة";	
		$fields['billing']['billing_address_2']['class']=array('same-row require-build');	
		$fields['account']['account_password']['label'] = 'كلمة السر';
		// $fields['billing']['billing_building']['description'] = 'لساكني العمارات برجاء ادخال رقم الدور والشقة';
		$fields['billing']['billing_comments']['label']="ملاحظات";

		$fields['billing']['billing_building_2'] = array(
			'label' => _x('رقم العقار', 'label', 'woocommerce'), // Add custom field placeholder
			'class'	=> array('same-row require-build'),
			'required' => true, // if field is required or not
			'type' => 'text', // add field type
			'priority'=>40,
		);

		$fields['billing']['billing_building'] = array(
			'label' => _x('الدور', 'label', 'woocommerce'), // Add custom field placeholder
			'class'	=> array('same-row require-build'),
			'required' => true, // if field is required or not
			'type' => 'text', // add field type
			'priority'=>40,
		);

		$fields['billing']['property_type'] = array(
			'type' => 'radio',
			'label' => _x('نوع العقار', 'label', 'woocommerce'), // Add custom field placeholder
			'required' => true, 
			'options' => array(
				'apart' => 'شقه',
				'villa' => 'فيلا'	
			),
			'priority'=>70,
			'default' => 'apart',
			'class' => array('property-type')
		);
		$fields['billing']['billing_building_2']['type'] = 'text';
		$fields['billing']['billing_building']['type'] = 'text';
		$fields['billing']['billing_address_2']['type'] = 'text';
		$fields['account']['account_password']['type'] = 'password';
	}

	if(is_user_logged_in()) {
		global $current_user; get_currentuserinfo();
		$fields['billing']['billing_first_name']['default'] = $current_user->user_firstname;
		$fields['billing']['billing_last_name']['default'] = $current_user->user_lastname;
		$fields['billing']['billing_phone']['default'] = $current_user->billing_phone;
		$fields['billing']['billing_email']['default'] = $current_user->user_email;		
		if(!empty($current_user->user_email)){
			$fields['billing']['billing_email']['class']= array('form-row-wide','hide-fld');
		}
		if(!empty($current_user->phone)){
			$fields['billing']['billing_phone']['class']= array('form-row-wide','hide-fld');
		}
		if(!empty($current_user->user_firstname)){
			$fields['billing']['billing_first_name']['class']= array('form-row-first','hide-fld');
		}
		if(!empty($current_user->user_lastname)){
			$fields['billing']['billing_last_name']['class']= array('form-row-last','hide-fld');
		}
	}
	// $fields['billing']['billing_country']['default'] = 'EG';
	//$fields['billing']['billing_city']['default'] = 'type a city';
return $fields;
}
add_action( 'wp_ajax_nopriv_get_city', 'get_city' );
add_action( 'wp_ajax_get_city', 'get_city' );

function get_city() {
	global $language, $states_cities,$cities_st;
	$state = $_POST['state'];
	$lang = $_POST['lang'];
	$selected = '';
	if(is_user_logged_in()){
		$user = wp_get_current_user();
		$selected = $user->billing_city;
	}
	// $language = $lang;
	$cities = ($language == "en")? $states_cities['en'][$state]: $states_cities['ar'][$state];

	$area = ($language == "en")?  'Choose city' : 'اختر المدينة' ;
	$label = ($language == "en")?  'City' : 'المدينة';
	$start = '<label for="billing_city" class="">'. $label .' <abbr class="required" title="مطلوب">*</abbr></label><select name="billing_city" id="billing_city" class="city_select" autocomplete="address-level2" placeholder="" tabindex="-1" aria-hidden="true"><option value="">'.$area.'</option>';
	$end = '</select>';

	if($cities){
		echo $start;
		$keys = array_keys($cities);
		for($i=0; $i<count($cities);$i++){
			?>
			<option value="<?php echo $keys[$i]; ?>" data-lang="<?php echo $language; ?>"<?php echo ($selected == $keys[$i] || $selected == $cities[$keys[$i]])? 'selected="selected"':''; ?> data-selected="<?php echo  $selected; ?>"><?php echo $cities[$keys[$i]]; ?></option>
			<?php 
		}
		echo $end;
	}else{
		echo '<label for="billing_city" class="">'. $label .'<abbr class="required" title="مطلوب">*</abbr></label>
		<input type="text" class="input-text area-text " placeholder="" name="billing_city" id="billing_city" autocomplete="address-level2">';
		}
	wp_die();
}

remove_action("woocommerce_checkout_order_review","woocommerce_checkout_payment",20);
add_action( 'woocommerce_checkout_shipping', 'woocommerce_checkout_payment', 20 );


add_action( 'woocommerce_after_checkout_validation', 'misha_one_err', 9999, 2);
 
function misha_one_err( $fields, $errors ){
	global $language;
	if(isset($_POST['post_data'])){
		$post_data = explode("&",$_POST['post_data']);
		$language = ($post_data[0] =="lang=en") ? 'en':'';
	}
	// if any validation errors
	if( !empty( $errors->get_error_codes() ) ) {
 
		// remove all of them
		foreach( $errors->get_error_codes() as $code ) {
			$errors->remove( $code );
		}
	}
	if(!$fields[ 'terms' ]){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أقرأ الشروط والأحكام وقم بقبولها لأستكمال الطلب بنجاح');
		}
		else{
			$errors->add( 'validation', 'Please read and accept the terms and conditions to proceed with your order.' ); 				
		}
	}
	if(empty($fields[ 'billing_first_name' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل الأسم الأول' );
		}
		else{
			$errors->add( 'validation', 'Billing First name is a required field.' ); 				
		}
	}
	if(empty($fields[ 'billing_last_name' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation','من فضلك أدخل أسم العائله' );
		}
		else{
			$errors->add( 'validation', 'Billing Last name is a required field.' ); 				
		}
	}
	if(empty($fields[ 'billing_phone' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل رقم التليفون' );
		}
		else{
			$errors->add( 'validation', 'Billing Phone is a required field.' ); 				
		}
	}
	if(empty($fields[ 'second_mobile_number' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل رقم التليفون الثاني' );
		}
		else{
			$errors->add( 'validation', 'Billing Phone 2 is a required field.' ); 				
		}
	}
	if(empty($fields[ 'national_id' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل الرقم القومي' );
		}
		else{
			$errors->add( 'validation', 'Billing Phone is a required field.' ); 				
		}
	}
	if(empty($fields[ 'billing_email' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل البريد الألكتروني' );
		}
		else{
			$errors->add( 'validation', 'Billing Email address is a required field.' ); 				
		}
		//$fields['billing_email'] = "glossgirl@glosscairo.com";
	}
	if(empty($fields[ 'billing_state' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل المحافظة' );
		}
		else{
			$errors->add( 'validation', 'Billing state is a required field.' ); 				
		}
	}
	if(empty($fields[ 'billing_city' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك أدخل المدينة' );
		}
		else{
			$errors->add( 'validation', 'Billing city is a required field.' ); 				
		}
	}
	// if(empty($fields[ 'billing_area' ])){
	// 	if ($_POST['lang']!="en"){
	// 		$errors->add( 'validation', 'من فضلك أدخل المنطقة' );
	// 	}
	// 	else{
	// 		$errors->add( 'validation', 'Billing Area is a required field.' ); 				
	// 	}
	// }
	if(empty($fields[ 'billing_address_1' ])){
		if ($_POST['lang']!="en"){
			$errors->add( 'validation', 'من فضلك ادخل العنوان بالتفصيل' );
		}
		else{
			$errors->add( 'validation', 'Billing Building No. & Street Name is a required field.' ); 				
		}
	}
	if($fields[ 'property_type' ] == 'apart' && empty($fields[ 'billing_address_2' ])){
		if ($_POST['lang']!="en"){
		$errors->add( 'validation', 'من فضلك أدخل رقم الشقه' );
		}
		else{
			$errors->add( 'validation', 'Please enter the apartment number' );
		}
		
	}
	if($fields[ 'property_type' ] == 'apart' && empty($fields[ 'billing_building' ])){


		if ($_POST['lang']!="en"){
		$errors->add( 'validation', 'من فضلك أدخل الدور' );
		}
		else{
			$errors->add( 'validation', 'Please enter the floor number' );
		}
		
		
	}
	if($fields[ 'property_type' ] == 'apart' && empty($fields[ 'billing_building_2' ])){

		if ($_POST['lang']!="en"){
		$errors->add( 'validation', 'من فضلك أدخل رقم العقار' );
		}
		else{
			$errors->add( 'validation', 'Please enter the property number' );
		}
				
	}	

}
add_action('woocommerce_checkout_process', 'custom_validate_billing_phone');

function custom_validate_billing_phone()
{
	$is_correct = preg_match('/^[0-9]{11,12}$/', $_POST['billing_phone']);
	if ($_POST['billing_phone'] && !$is_correct) {

		if ($_POST['lang'] != "en") {
			wc_add_notice(__('يجب أن يكون الهاتف بين 11 و 12 رقمًا.'), 'error');
		} else {
			wc_add_notice(__('The phone must be between 11 and 12 numbers.'), 'error');
		}
	}
	$is_correct = preg_match('/^[0-9]{11,12}$/', $_POST['second_mobile_number']);
	if ($_POST['second_mobile_number'] && !$is_correct) {

		if ($_POST['lang'] != "en") {
			wc_add_notice(__('يجب أن يكون الهاتف بين 11 و 12 رقمًا.'), 'error');
		} else {
			wc_add_notice(__('The phone must be between 11 and 12 numbers.'), 'error');
		}
	}
	$is_correct = preg_match('/^[0-9]{14}$/', $_POST['national_id']);
	if ($_POST['national_id'] && !$is_correct) {
		if ($_POST['lang'] != "en") {
			wc_add_notice(__('يجب أن يكون الرقم القومي 14 رقمًا.'), 'error');
		} else {
			wc_add_notice(__('The phone must be between 11 and 12 numbers.'), 'error');
		}
	}
}



add_action('woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1);

function my_custom_checkout_field_display_admin_order_meta($order)
{
	global $language;
	if (get_post_meta($order->id, '_national_id', true)) {
		echo '<p><strong>' . __('National ID') . ':</strong> <br/>' . get_post_meta($order->id, '_national_id', true) . '</p>';
	}
	if (get_post_meta($order->id, '_second_mobile_number', true)) {
		echo '<p><strong>' . __('Second Mobile Number') . ':</strong> <br/>' . get_post_meta($order->id, '_second_mobile_number', true) . '</p>';
	}
}

add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_remove_shipping_label', 9999, 2 );
   
function bbloomer_remove_shipping_label( $label, $method ) {
    $new_label = preg_replace( '/^.+:/', '', $label );
    return $new_label;
}
global $checkout_chk;
if($checkout_chk){
add_filter( 'woocommerce_coupon_error','coupon_error_message_change',10,3 );
function coupon_error_message_change($err, $err_code, $parm )
{
	global $language_temp;
	switch ( $err_code ) {
		case 105:
		/* translators: %s: coupon code */
		?>
		<script>
		if($('body').hasClass('rtl')){
			$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error" role="alert"><li><?php echo 'كوبون '. $parm->get_code()." غير موجود " ;?></li></ul>');
		}
		else{
			$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error" role="alert"><li><?php echo "Coupon ". $parm->get_code()." does not exist!" ;?></li></ul>');
		}
		</script>
		<?php
		case 107:
			/* translators: %s: coupon code */
			?>
			<script>
			if($('body').hasClass('rtl')){
				$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error" role="alert"><li><?php echo 'لقد انتهت صلاحية الكوبون.' ;?></li></ul>');
			}
			else{
				$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error" role="alert"><li><?php echo "Coupon code expired.";?></li></ul>');
			}
			</script>
			<?php
		break;
	 }

	return $err;
   
}

add_filter( 'woocommerce_coupon_message', 'filter_woocommerce_coupon_message', 10, 3 );
function filter_woocommerce_coupon_message( $msg, $msg_code, $coupon ) {
    if( $msg === __( 'Coupon code applied successfully.', 'woocommerce' ) ) {
	?>
		<script>
		// if($('body').hasClass('rtl')){
			$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-message" role="alert"><li><?php echo 'تم تفعيل كوبون '. $coupon->get_code()." بنجاح" ;?></li></ul>');
		// }
		// else{
			//$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-message" role="alert"><li><?php //echo "Coupon ". $coupon->get_code()." applied successfully." ;?></li></ul>');
		// }
		</script>
	<?php
	}
    elseif(strpos($msg,'removed') !== false || strpos($msg,'إزالة') !== false) {
	?>
			<script>
		// if($('body').hasClass('rtl')){
			$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-message" ><li><?php echo '.تم حذف كوبون ';?></li></ul>');
		// }
		// else{
			//$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-message"><li><?php //echo "Coupon Removed.";?></li></ul>');
		// }
		</script>
	<?php
	}
	else{
	?>
			<script>
			$('.woocommerce-notices-wrapper').html('<ul class="woocommerce-message" role="alert"><li><?php echo $msg ;?></li></ul>');
		</script>
	<?php
	}
    return $msg;
}
function filter_woocommerce_cart_totals_coupon_html( $coupon_html, $coupon, $discount_amount_html ) {
    // Change text
	global $language;
	if($language=="en"){
		$coupon_html = str_replace( '[حذف]', '[Remove]', $coupon_html );
	}
	else{
		$coupon_html = str_replace( '[حذف]', '[حذف]', $coupon_html );
	}
    return $coupon_html;
}
add_filter( 'woocommerce_cart_totals_coupon_html', 'filter_woocommerce_cart_totals_coupon_html', 10, 3 );

}
// add_action( 'woocommerce_thankyou', 'bbloomer_redirectcustom');
// function bbloomer_redirectcustom( $order_id ){
//     $order = wc_get_order( $order_id );
//     $url = 'https://yoursite.com/custom-url';
//     if ( ! $order->has_status( 'failed' ) ) {
//         wp_safe_redirect( $url );
//         exit;
//     }
// }

add_filter('wpnp_fields_labels', 'wpnp_fields_labels', 10, 1);
function wpnp_fields_labels( $translations ) {
	global $language;
    $translations['card_number'] = ($language == 'en')? 'Card Number' : "رقم بطاقة الإئتمان" ;
	 $translations['cardholder_name'] = ($language == 'en')? 'Name on card' : "اسم حامل البطاقة" ;
    $translations['expiry_date'] = ($language == 'en')? 'Expiry Date' : "تاريخ الإنتهاء";
	 $translations['cvc'] = ($language == 'en')? 'CVC' : "الكود السري";
    return $translations;
	 
}

// do_action( 'wp_ajax_woocommerce_checkout', $array, $int ); 
// function on_create_order( $array, $int ) { 
	
// }; 
// add_action( 'wp_ajax_woocommerce_checkout', 'on_create_order', 10, 2 ); 

// add_action('woocommerce_checkout_create_order', function ($order_id) {
// 	global $wpdb;
// 	$order = new WC_order($order_id);
// 	$table_name =  "wp_mitch_users_addresses";
// 	$date_created = $order->get_date_created();
// 	$wpdb->insert( $table_name, array(
// 	  'user_id' => 1,
// 		'address_type' => 0,
// 		'country' => 'EG',
// 		'state' => $order->get_billing_state(),
// 	  'city' => $order->get_billing_city(),
// 	  'property_type' => get_post_meta( $order_id, '_property_type', true ),
// 	  'street' => $order->get_billing_address_1(),
// 	  'floor' => get_post_meta( $order_id, '_billing_building', true ),
// 	  'buidling' => get_post_meta( $order_id, '_billing_building_2', true ),
// 	  'apartment_no' => $order->get_billing_address_2(),
// 	  'created_date' => $date_created->date("Y-m-d H:i:s")
	  
// 	) );


// }, 10, 1);

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
	update_post_meta( $order_id, '_property_type', sanitize_text_field( $_POST['property_type'] ) );
	update_post_meta( $order_id, '_order_id_tmp', sanitize_text_field( $order_id ) );
	if($_POST['national_id']){
		update_post_meta( $order_id, '_national_id', $_POST['national_id'] );
	}
	if($_POST['second_mobile_number']){
		update_post_meta( $order_id, '_second_mobile_number', $_POST['second_mobile_number'] );
	}
};

add_action('woocommerce_checkout_create_order', 'before_checkout_create_order', 10, 2);
function before_checkout_create_order( $order, $data ) { //$data in order 2186 // $post in order 2187
				
		global $woocommerce, $post,  $wpdb;
		//Assign the order ID using the $post->ID 
		//$order = new WC_Order($post->ID);
		// Use the getter function to get order ID  
		$order_id = $order->id;
		// $table_name = $wpdb->prefix.'mitch_users_addresses'; //changed
		$table_name =  "wp_mitch_users_addresses";
		// $date_created = $order->get_date_created();
		$order->update_meta_data('test_order_id',$order_id );
		// $order->update_meta_data('test_order',	$->ID );

		$wpdb->insert( $table_name, array(
		  'user_id' => $order->get_user_id(), //eiad *
			'address_type' => 0, //eiad *
			'country' => 'EG',
			'state' => $order->get_billing_state(),
		  'city' => $order->get_billing_city(),
		  'property_type' => $_POST['property_type'],
		  'street' => $order->get_billing_address_1(),
		  'floor' => $_POST['billing_building'],
		  'building' => $_POST['billing_building_2'],
		  'apartment_no' => $order->get_billing_address_2()
		  ) );
		 
		//   $wpdb->query("INSERT INTO $table_name ( user_id, address_type, country, state, city, property_type, street, floor, building, apartment_no) VALUES (1, 0, 'EG', 'cairo', 'nasr city' ,'test' ,'test' ,'test' ,'test' ,'test')");

		//   $order->update_meta_data('error',$wpdb->last_error );
		//   $order->update_meta_data('last_query',$wpdb->last_query );

}
// function mytheme_add_woocommerce_support() {
// 	add_theme_support( 'woocommerce' );
// }
// if(is_checkout()){
// 	add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );
// }
global $cities_cost,$checkout_chk;
// if($checkout_chk){
if(have_rows('cities_cost','option')):
	$cities_cost = array();
	while(have_rows('cities_cost','option')): the_row();
		$cities_cost[get_sub_field('city')] = get_sub_field('cost');
	endwhile;endif;
// }
function filter_woocommerce_package_rates( $rates, $package ) {
    /* Settings */
	global $cities_cost;
    $min = 25;
    $max = 50;
    $discount_percent = 50;

    // Get cart total
    $cart_total =intval(WC()->cart->cart_contents_total);
	$city = $_POST['city'];
	// print_r($cities_cost);
	$highest_shipping = intval($cities_cost[$city]);
	$custom_percent = intval(ceil($cart_total*0.03));
	// print_r($zones);
    // Condition
    // if ( $cart_total >= $min && $cart_total <= $max ) {
        // (Multiple)
		$items_count  = WC()->cart->get_cart_contents();
		if(WC()->cart->get_cart()){
			$kitchen_check = false;
			//foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item){
			//	if(has_term(338,'product_cat',$cart_item['product_id']) || has_term(315,'product_cat',$cart_item['product_id'])){
			//		$kitchen_check = true;
			//	}
			//	else{
			//		$kitchen_check = false;
			//		break;
			//	}
			//}
		}
        foreach ( $rates as $rate_key => $rate ) {
            // Get rate cost            
            $cost = intval($rates[$rate_key]->cost);
			if(intval($cost) != 0){
			if($highest_shipping):
	// echo 'state '.$city.' // '.$cart_total.' // '.$cost.' // '.$highest_shipping.' // '.$custom_percent.'<br>';
				if($cost>$custom_percent){
					// echo '1'.'<br>';
					// unset( $rates['flat_rate:28'] );
					$rates[$rate_key]->cost = $cost;
				}
				elseif($custom_percent<$highest_shipping){
					// echo '2'.'<br>';
					// unset( $rates['flat_rate:28'] );
					$rates[$rate_key]->cost = floatval($custom_percent);
				}
				else{
					// unset( $rates['flat_rate:28'] );
					$rates[$rate_key]->cost = $highest_shipping;
				}
			endif;
				if($kitchen_check){
					$rates[$rate_key]->cost = 0;
				}
			}
            
            // Set rate cost
            // $rates[$rate_key]->cost = $cost - ( ( $cost * $discount_percent ) / 100 );
        }
    // }
	// WC()->cart->calculate_totals();
    return $rates;
}
add_filter( 'woocommerce_package_rates', 'filter_woocommerce_package_rates', 10, 2 );


add_action('woocommerce_checkout_update_order_review', 'checkout_update_refresh_shipping_methods', 10, 1);
function checkout_update_refresh_shipping_methods($post_data)
{	
	$packages = WC()->cart->get_shipping_packages();
	//print_r($post_data);
	foreach ($packages as $package_key => $package) {
		WC()->session->set('shipping_for_package_' . $package_key, false); // Or true
	}
	WC()->cart->calculate_shipping();
	WC()->cart->calculate_totals();
	// print_r(WC()->cart->total);
}
// sh3'al
// global $wpdb;
// 		// $table_name = $wpdb->prefix.'mitch_users_addresses'; //changed
// 		$table_name =  "wp_mitch_users_addresses";
// 		// $date_created = $order->get_date_created();
// 		$order_id = $order->id;
			//or $order_id = $order->get_id();
// 		$order->update_meta_data('test_order_id',$order_id );
add_action( 'woocommerce_checkout_create_order_line_item', 'save_cart_item_data_as_order_item_meta_data', 20, 4 );
function save_cart_item_data_as_order_item_meta_data( $item, $cart_item_key, $values, $order ) {
    if ( isset( $values['fabrics'] ) ) {
        $item->update_meta_data( __('Date-Time'), $values['fabrics'] );
    }
}