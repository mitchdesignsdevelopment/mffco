<?php
add_action('wp_ajax_customized_product_add_to_cart', 'mitch_customized_product_add_to_cart');
add_action('wp_ajax_nopriv_customized_product_add_to_cart', 'mitch_customized_product_add_to_cart');
function mitch_customized_product_add_to_cart()
{
  $added              = array();
  $parent_id          = intval($_POST['parent_id']);
  $variations_ids     = (array)$_POST['variations_ids'];
  $visit_type         = sanitize_text_field($_POST['visit_type']);
  $visit_branch       = sanitize_text_field($_POST['visit_branch']);
  $visit_home         = sanitize_text_field($_POST['visit_home']);
  $custom_cart_data   = array(
    'custom_cart_data' => array(
      'attributes_keys' => (array)$_POST['attributes_keys'],
      'attributes_vals' => (array)$_POST['attributes_vals'],
      'variations_ids'  => $variations_ids,
      'visit_type'      => $visit_type,
      'visit_branch'    => $visit_branch,
      'visit_home'      => $visit_home
    )
  );
  // mitch_test_vars(array($visit_type, $visit_branch, $visit_home));
  // exit;
  // $product_attributes = array_keys(get_post_meta($parent_id, '_product_attributes', true));
  if (!empty($variations_ids)) {
    //$i = 0;
    $total_price = 0;
    foreach ($variations_ids as $variation_id) {
      $total_price = $total_price + (float)get_post_meta($variation_id, '_price', true);
      // $product_attributes = wc_get_product_variation_attributes($variation_id);
      // $variation_attributes = array();
      // if(!empty($product_attributes)){
      //   foreach($product_attributes as $attribute_key){
      //     if($attribute_key == $attributes_keys[$i]){
      //       $variation_attributes['attribute_'.$attribute_key] = $attributes_vals[$i];
      //     }else{
      //       $variation_attributes['attribute_'.$attribute_key] = 'none';
      //     }
      //   }
      // }
      // echo '<pre>';
      // var_dump($variation_attributes);
      // echo '</pre>';
      //if(!empty($variation_attributes)){
      //  $added[] = WC()->cart->add_to_cart($parent_id, 1, $variation_id, wc_get_product_variation_attributes($variation_id));//14, 1,$variation_attributes
      //}
      //$i++;
    }
  }

  $custom_cart_data['custom_cart_data']['custom_total'] = number_format($total_price, 0);
  $cart_item_key = WC()->cart->add_to_cart($parent_id, 1, $variation_id, wc_get_product_variation_attributes($variation_id), $custom_cart_data); //
  WC()->cart->calculate_totals();

  if ($cart_item_key) {
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'redirect_to'  => home_url('cart'),
      'msg'          => 'Added To Cart Successfully.',
    );
  } else {
    $response = array(
      'status' => 'error',
      'msg'    => wc_print_notices(),
    );
  }
  // var_dump($response);
  // exit;
  echo json_encode($response);
  wp_die();
}

add_action('woocommerce_before_calculate_totals', 'mitch_recalculate_cart_item_price');
function mitch_recalculate_cart_item_price($cart_object)
{
  foreach ($cart_object->get_cart() as $hash => $values) {
    if (!empty($values['custom_cart_data'])) {
      $values['data']->set_price($values['custom_cart_data']['custom_total']);
    }
  }
}

add_action('wp_ajax_simple_product_add_to_cart', 'mitch_simple_product_add_to_cart');
add_action('wp_ajax_nopriv_simple_product_add_to_cart', 'mitch_simple_product_add_to_cart');
function mitch_simple_product_add_to_cart()
{
  $product_id      = intval($_POST['product_id']);
  $quantity_number = intval($_POST['quantity_number']);
  $added_to_cart   = WC()->cart->add_to_cart($product_id, $quantity_number);
  if ($added_to_cart) {
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'msg'          => 'Added To Cart Successfully.',
    );
  } else {
    $errors = WC()->session->get('wc_notices', array())['error'];
    $count  = count($errors);
    if (isset($errors) && !empty($errors)) {
      foreach ($errors as $key => $error_data) {
        $msg .= $error_data['notice'];
        if ($count > 1) {
          $msg = $msg . ', ';
        }
      }
    }
    $response = array(
      'status'  => 'error',
      'code'    => 401,
      'msg'     => $msg,
    );
    wc_clear_notices();
  }
  // WC()->cart->set_shipping_total(50);
  // WC()->cart->calculate_totals();
  // WC()->cart->calculate_shipping();
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_get_availablility_variable_product', 'mitch_get_availablility_variable_product');
add_action('wp_ajax_nopriv_get_availablility_variable_product', 'mitch_get_availablility_variable_product');
function mitch_get_availablility_variable_product()
{
  $attributes      = array();
  $quantity      = array();
  $variation_id    = 0;
  $product_id      = intval($_POST['product_id']);
  $selected_items  = $_POST['selected_items'];
  // $quantity_number = intval($_POST['quantity_number']);
  // var_dump($quantity_number);
  // exit;
  $product_obj     = wc_get_product($product_id);
  if (!empty($selected_items)) {
    foreach ($selected_items as $key => $value) {
      foreach ($value as $arr_k => $arr_v) {
        $attributes[$arr_k] = $arr_v;
      }
    }
  }
  if (!empty($product_obj->get_available_variations())) {
    foreach ($product_obj->get_available_variations() as $variation_obj) {
      if ($variation_obj['attributes'] == $attributes) {
        $variation_id = $variation_obj['variation_id'];
      }
    }
  }
  if (!empty($variation_id)) {
    $variation = new WC_Product_Variation($variation_id);
    $quantity[] = $variation->get_stock_quantity();
    $final_price = number_format($variation->get_price(), 0);
    if ($variation->is_on_sale()) {
      $var_price = number_format($variation->get_regular_price(), 0);
      $sale_price = number_format($variation->get_sale_price(), 0);
      $price_html = '
              <del><span class="woocommerce-Price-amount amount"><bdi>' . $var_price . '<span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span></del>

              <ins><span class="woocommerce-Price-amount amount"><bdi>' . $sale_price . '<span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span></ins>
            ';
    } else {
      $price_html = '<span class="woocommerce-Price-amount amount"><bdi>' . $final_price . '<span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span>';
    }

    $response = array(
      'status'      => 'success',
      'quantity'    => array_sum($quantity),
      'price'       => $price_html,
      'image_id'    => $variation->get_image_id(),
      'msg'         => 'Added To Cart Successfully.'
    );
  } else {
    $response = array(
      'status' => 'error',
      'msg'    => wc_print_notices()
    );
  }
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_variable_product_add_to_cart', 'mitch_variable_product_add_to_cart');
add_action('wp_ajax_nopriv_variable_product_add_to_cart', 'mitch_variable_product_add_to_cart');
function mitch_variable_product_add_to_cart()
{
  $attributes      = array();
  $variation_id    = 0;
  $product_id      = intval($_POST['product_id']);
  $selected_items  = $_POST['selected_items'];
  $quantity_number = intval($_POST['quantity_number']);
  $fabric_meta = intval($_POST['fabric_meta']);
  // var_dump($quantity_number);
  // exit;
  $product_obj     = wc_get_product($product_id);
  if (!empty($selected_items)) {
    foreach ($selected_items as $key => $value) {
      foreach ($value as $arr_k => $arr_v) {
        $attributes[$arr_k] = $arr_v;
      }
    }
  }
  if (!empty($product_obj->get_available_variations())) {
    foreach ($product_obj->get_available_variations() as $variation_obj) {
      // print_r($variation_obj['attributes']);
      // echo 'jims';
      // print_r($attributes);
      if ($variation_obj['attributes'] == $attributes) {
        $variation_id = $variation_obj['variation_id'];
      }
    }
  }
  if (!empty($variation_id)) {
    if (!$quantity_number) {
      $quantity_number = 1;
    }
    if ($fabric_meta) {
      $added_to_cart   = WC()->cart->add_to_cart($product_id, $quantity_number, $variation_id, wc_get_product_variation_attributes($variation_id), array('fabric' => get_the_title($fabric_meta)));
    } else {
      $added_to_cart   = WC()->cart->add_to_cart($product_id, $quantity_number, $variation_id, wc_get_product_variation_attributes($variation_id));
    }
  }
  if ($added_to_cart) {
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'msg'          => 'Added To Cart Successfully.'
    );
  } else {
    $response = array(
      'status' => 'error',
      'msg'    => wc_print_notices()
    );
  }
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_cart_remove_item', 'mitch_cart_remove_item');
add_action('wp_ajax_nopriv_cart_remove_item', 'mitch_cart_remove_item');
function mitch_cart_remove_item()
{
  $product_id = intval($_POST['product_id']);
  if (!empty($product_id)) {
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
      if ($cart_item['product_id'] == $product_id) {
        WC()->cart->remove_cart_item($cart_item_key);
      }
    }
  } else {
    WC()->cart->remove_cart_item(sanitize_text_field($_POST['cart_item_key']));
  }
  echo json_encode(
    array(
      'success'      => true,
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_total'   => WC()->cart->cart_contents_total,
      'cart_content' => mitch_get_cart_content()
    )
  );
  wp_die();
}

add_action('wp_ajax_update_cart_items', 'mitch_update_cart_items');
add_action('wp_ajax_nopriv_update_cart_items', 'mitch_update_cart_items');
function mitch_update_cart_items()
{
  $item_total      = 0;
  $post_cart_key   = sanitize_text_field($_POST['cart_item_key']);
  $quantity_number = intval($_POST['quantity_number']);
  // var_dump($_POST['quantity_number']);
  // exit;
  if (!empty($quantity_number)) {
    WC()->cart->set_quantity($post_cart_key, $quantity_number);
    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
      if ($cart_item_key == $post_cart_key) {
        $item_total = $cart_item['line_subtotal'];
        break;
      }
    }
    echo json_encode(
      array(
        'success'      => true,
        'cart_count'   => WC()->cart->get_cart_contents_count(),
        'cart_total'   => number_format(WC()->cart->cart_contents_total, 0),
        'cart_content' => mitch_get_cart_content(),
        'item_total'   => number_format($item_total, 0),
      )
    );
  }
  wp_die();
}

add_action('wp_ajax_mitch_apply_coupon', 'mitch_apply_coupon');
add_action('wp_ajax_nopriv_mitch_apply_coupon', 'mitch_apply_coupon');
function mitch_apply_coupon()
{
  global $fixed_string;
  $cart_discount_div = '';
  $msg = '';
  $coupon_code       = sanitize_text_field($_POST['coupon_code']);
  $coupon_id         = wc_get_coupon_id_by_code($coupon_code);
  //$coupon_data = new WC_Coupon($coupon_code);
  if (!empty($coupon_id)) {
    WC()->cart->apply_coupon($coupon_code);
    WC()->cart->calculate_totals();
    $errors = WC()->session->get('wc_notices', array())['error'];
    $count  = count($errors);
    // echo 'jims';
    if (isset($errors) && !empty($errors)) {
      // echo 'jims2';
      foreach ($errors as $key => $error_data) {
        $error_trans = $fixed_string[$error_data['notice']];
        if (!empty($error_trans)) {
          $msg .= $error_trans;
        } else {
          $msg .= $error_data['notice'];
        }
        if ($count > 1) {
          $msg = $msg . ', ';
        }
      }
      $response = array(
        'status' => 'error',
        'code'   => 401,
        'msg'    => $msg
      );
      wc_clear_notices();
    } else {
      // echo '<pre>';
      // var_dump();
      // echo '</pre>';
      if ($_POST['coupon_from'] == 'checkout') {
        global $theme_settings;
        $shipping_data     = mitch_get_shipping_data()['shipping_methods'][$theme_settings['default_shipping_method']];
        $shipping_cost     = $shipping_data['cost'];
        $coupon_discount   = WC()->cart->coupon_discount_totals[sanitize_title($coupon_code)];
        $cart_discount_div = '<div class="list_pay discount">
             <p>' . $fixed_string['checkout_page_discount'] . '</p>
             <p><span id="cart_discount">- ' . $coupon_discount . '</span> ' . $theme_settings['current_currency'] . '</p>
         </div>';
      } else {
        $shipping_cost = 0;
      }
      $response = array('status' => 'success', 'cart_total' => WC()->cart->cart_contents_total + $shipping_cost, 'cart_discount_div' => $cart_discount_div);
    }
  } else {
    $response = array('status' => 'error', 'code' => 401, 'msg' => 'كوبون غير صحيح!');
  }
  echo json_encode($response);
  wp_die();
}


add_action('wp_ajax_mitch_remove_coupon', 'mitch_remove_coupon');
add_action('wp_ajax_nopriv_mitch_remove_coupon', 'mitch_remove_coupon');
function mitch_remove_coupon()
{
  $coupon_code   = sanitize_text_field($_POST['coupon_code']);
  if ($_POST['coupon_from'] == 'checkout') {
    global $theme_settings;
    $shipping_data = mitch_get_shipping_data()['shipping_methods'][$theme_settings['default_shipping_method']];
    $shipping_cost = $shipping_data['cost'];
  } else {
    $shipping_cost = 0;
  }
  WC()->cart->remove_coupon($coupon_code);
  WC()->cart->calculate_totals();
  $response        = array(
    'status'       => 'success',
    'cart_total'   => WC()->cart->cart_contents_total + $shipping_cost,
    'cart_count'   => WC()->cart->get_cart_contents_count(),
    'cart_content' => mitch_get_cart_content(),
  );
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_mitch_bought_together_products', 'mitch_bought_together_products');
add_action('wp_ajax_nopriv_mitch_bought_together_products', 'mitch_bought_together_products');
function mitch_bought_together_products()
{
  $form_data = $_POST['form_data'];
  $products_ids = (array)$form_data['products_ids'];
  foreach ($products_ids as $product_id) {
    $added_to_cart   = WC()->cart->add_to_cart($product_id, 1);
  }
  if ($added_to_cart) {
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'msg'          => 'اضافة المنتجات الي سلة المشتريات',
    );
  } else {
    $errors = WC()->session->get('wc_notices', array())['error'];
    $count  = count($errors);
    if (isset($errors) && !empty($errors)) {
      foreach ($errors as $key => $error_data) {
        $msg .= $error_data['notice'];
        if ($count > 1) {
          $msg = $msg . ', ';
        }
      }
    }
    $response = array(
      'status'  => 'error',
      'code'    => 401,
      'msg'     => $msg,
    );
    wc_clear_notices();
  }
  echo json_encode($response);
  // if(!empty($products_ids)){
  //   echo '<pre>';
  //   var_dump($products_ids);
  //   echo '</pre>';
  // }
  wp_die();
}


function mitch_get_cart_content()
{
  global $fixed_string, $theme_settings;
  $items      = WC()->cart->get_cart();
  $cart_total = WC()->cart->cart_contents_total;
  if (!empty(WC()->cart->applied_coupons)) {
    $coupon_code    = WC()->cart->applied_coupons[0];
    $active         = 'active';
    $dis_form_style = 'display:block;';
    $dis_abtn_style = 'display:none;';
    $dis_rbtn_style = 'display:block;';
  } else {
    $coupon_code    = '';
    $active         = '';
    $dis_form_style = '';
    $dis_abtn_style = 'display:block;';
    $dis_rbtn_style = 'display:none;';
  }
  if (WC()->cart->get_cart_contents_count() == 0) {
    $mini_class = 'empty_min_cart';
  } else {
    $mini_class = 'min_cart';
  }
  $price = number_format(WC()->cart->subtotal, 0);
  $cart_content = '
  <div id="mini_cart" class="' . $mini_class . '">
    <div class="top">
      <div class="cart_info">
        <div class="title_min_cart">
          <h2>سلة التسوق</h2>
        </div>
        <div class="info_min_cart">
          <h4>الاجمالي
            <p class="price">' . $price . '</span> ' . $theme_settings['current_currency'] . '</p>
            <span class="qun">' . WC()->cart->get_cart_contents_count() . ' منتج</span>
          </h4>
        </div>
      </div>
      <div class="cart_action">
          <a class="open_checkout" href="' . home_url('checkout') . '">
            <button type="button">' . $fixed_string['cart_submit_button'] . '</button>
          </a>
          <a class="open_cart" href="' . home_url('cart') . '">
            <button type="button">' . $fixed_string['min_cart_submit_button'] . '</button>
          </a>
      </div>
    </div>
    <div class="all_item">';

  if (!empty($items)) {
    foreach ($items as $item => $values) {
      $products_ids[]    = $values['product_id'];
      $cart_product_data = mitch_get_short_product_data($values['product_id']);
      $cart_content .= '
          <div id="mini_cart_' . $item . '" class="single_item">
              <div class="sec_item">
                  <div class="img">
                      <img height="100" src="' . $cart_product_data['product_image'] . '" alt="' . $cart_product_data['product_title'] . '">
                  </div>
                  <div class="info">
                      <div class="text">
                          <a href="' . $cart_product_data['product_url'] . '">
                          <h4>' . $cart_product_data['product_title'] . '</h4></a>';
      if (!empty($values['fabric'])) {
        $cart_content .= '<ul>';
        $cart_content .= '<li>' . $values['fabric'] . '</li>';
        $cart_content .= '</ul>';
      }
      if (!empty($values['custom_cart_data'])) {
        $cart_content .= '<ul>';
        foreach ($values['custom_cart_data']['attributes_vals'] as $attr_val) {
          $cart_content .= '<li>' . mitch_get_product_attribute_name($attr_val) . '</li>';
        }
        $cart_content .= '</ul>';
      } elseif (!empty($values['variation'])) {
        $cart_content .= '<ul>';
        // foreach ($values['variation'] as $key => $value) {
        //   $cart_content .= '<li>' . ucfirst($value) . '</li>';
        // }
        foreach ($values['data']->get_attributes() as $attribute_key => $attribute_arr) {
          $terms = get_term_by('slug', $values['data']->get_attributes()[$attribute_key], $attribute_key);
          $cart_content .= '<li>' . $terms->name . '</li>';
        }
        $cart_content .= '</ul>';
      }
      $line_subtotal = number_format($values['line_subtotal'], 0);
      $classname = '';
      if ($values['quantity'] == 1) {
        $classname = 'disabled';
      }
      $cart_content .= '
                            </div>
                            <div class="section_count">
                              <button class="increase" id="increase" onclick="increaseValueByID(\'number_' . $item . '\');update_cart_items(\'' . $item . '\', \'mini_cart\');" value="Increase Value"></button>
                              <input class="number_count" type="number" id="number_' . $item . '" value="' . $values['quantity'] . '" />
                              <button class="decrease ' . $classname . '" id="decrease" onclick="decreaseValueByID(\'number_' . $item . '\');update_cart_items(\'' . $item . '\', \'mini_cart\');" value="Decrease Value"></button>
                            </div>
                            <p class="total_price"><span id="line_subtotal_' . $item . '">' . $line_subtotal . '</span> ' . $theme_settings['current_currency'] . '</p>
                            <a class="remove_min_cart href="javascript:void(0);" onclick="cart_remove_item(\'' . $item . '\', \'\', \'mini_cart\');">' . $fixed_string['cart_delete_title'] . '</a>
                        </div>
                    </div>
                </div>
                ';
    }
    //"'.$item.'", ''
  } else {
    $cart_content .= '
        <div class="section_emty">
            <img src="' . $theme_settings['theme_url'] . '/assets/img/icons/emty_cart.png" alt="">
            <p>لا يوجد حالياً منتجات تمت إضافتها لحقيبة التسوق</p>
            <a class="shop" href="' . home_url('shop') . '">
              <button type="button">تسوق الآن</button>
            </a>
        </div>';
  }
  $price = number_format(WC()->cart->subtotal, 0);
  $cart_content .= '
    </div>
    <div class="bottom">
      <div class="bottom_info">
        <div class="info_min_cart">
          <h4>الاجمالي
            <p class="price">' . $price . '</span> ' . $theme_settings['current_currency'] . '</p>
            <span class="qun">' . WC()->cart->get_cart_contents_count() . ' منتج</span>
          </h4>
        </div>
      </div>
      <a class="open_checkout" href="' . home_url('checkout') . '">
        <button type="button">' . $fixed_string['cart_submit_button'] . '</button>
      </a>
  </div>
  </div>
  ';
  return $cart_content;
}

function mitch_repeat_order()
{
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['action'] == 'repeat_order') {
      // $new_order_id = mitch_create_order_from(intval($_POST['order_id']));
      // if($new_order_id){
      //   wp_redirect(home_url('my-account/orders-list/?order_id='.$new_order_id.''));
      //   exit;
      // }
      // var_dump($_POST);
      // exit;
      if ($_POST['repeat_action'] == 'no_items') {
        WC()->cart->empty_cart();
      }
      $custom_cart_data = array();
      $r_order_obj      = wc_get_order(intval($_POST['order_id']));
      foreach ($r_order_obj->get_items() as $cart_item_key => $values) {
        if (!empty($values['custom_cart_data'])) {
          $items_data = $values['custom_cart_data'];
          if (!empty($items_data['visit_type'])) {
            $custom_cart_data['visit_type'] = $items_data['visit_type'];
          }
          if (!empty($items_data['visit_branch'])) {
            $custom_cart_data['visit_branch'] = $items_data['visit_branch'];
          }
          if (!empty($items_data['visit_home'])) {
            $custom_cart_data['visit_home'] = $items_data['visit_home'];
          }
          if (!empty($items_data['attributes_keys'])) {
            $custom_cart_data['attributes_keys'] = $items_data['attributes_keys'];
          }
          if (!empty($items_data['attributes_vals'])) {
            $custom_cart_data['attributes_vals'] = $items_data['attributes_vals'];
          }
          $total_price = 0;
          // echo '<pre>';
          // var_dump($values);
          // echo '</pre>';
          // exit;
          if (!empty($items_data['variations_ids'])) {
            foreach ($items_data['variations_ids'] as $variation_id) {
              $total_price = $total_price + (float)get_post_meta($variation_id, '_price', true);
            }
          }
          $custom_cart_data['custom_total'] = number_format($total_price, 0);
          $custom_cart_data_arr             = array('custom_cart_data' => $custom_cart_data);
          // echo '<pre>';
          // var_dump($custom_cart_data_arr);
          // echo '</pre>';
          // exit;
          $added_to_cart = WC()->cart->add_to_cart($values['product_id'], 1, $variation_id, wc_get_product_variation_attributes($variation_id), $custom_cart_data_arr);
        } else {
          if (!empty($values['variation_id'])) {
            $product_id = $values['variation_id'];
          } else {
            $product_id = $values['product_id'];
          }
          $added_to_cart   = WC()->cart->add_to_cart($product_id, $values['quantity']);
        }
      }
      if ($added_to_cart) {
        wp_redirect(home_url('cart'));
        exit;
      }
    }
    wp_redirect(home_url('my-account/orders-list/?order_id=' . intval($_POST['order_id']) . '&response=error'));
    exit;
  }
}

//update cart prices to rate price
add_action('woocommerce_before_calculate_totals', 'add_custom_item_price', 10);
function add_custom_item_price($cart_object)
{
  foreach ($cart_object->get_cart() as $item_values) {
    ## Set the new item price in cart
    // $item_values['data']->set_price(mitch_get_product_price_after_rate($item_values['data']->price));
    $item_values['data']->set_price($item_values['data']->price);
  }
}
// ------------------------------------------------------

add_action('woocommerce_before_cart', 'bbloomer_check_category_in_cart_before');
// ==============================================================================
add_action('woocommerce_before_cart', 'bbloomer_check_bads_in_cart_before');

add_action('woocommerce_proceed_to_checkout', 'bbloomer_check_category_in_cart_before');
add_action('woocommerce_review_order_before_submit', 'bbloomer_check_category_in_cart_before');
add_filter('woocommerce_add_to_cart_validation', 'bbloomer_check_category_in_cart_before');
add_action('woocommerce_check_cart_items', 'bbloomer_check_category_in_cart_before');

function bbloomer_check_bads_in_cart_before()
{


  $ids = [];
  $counter = 0;

  // Loop through all products in the Cart
  foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {

    $cart_product_data = mitch_get_short_product_data($cart_item['product_id']);


    if (has_term(257, 'product_cat', $cart_product_data['product_id'])) {


      $targeted_id = $cart_item['product_id'];
      $counter++;
      $ids[] = array_push($ids, $targeted_id);
      // array_push($ids, $targeted_id);
    }
  }

  return  $counter;
}

function bbloomer_check_category_in_cart_before()
{

  // Set $cat_in_cart to false
  // $cat_in_cart;

  $cat_ids = array();
  // echo "This is D_on_off : ", $D_on_off;



  // Loop through all products in the Cart
  foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $product = $cart_item['data'];

    $cart_product_data = mitch_get_short_product_data($cart_item['product_id']);
    $product_cats = $cart_product_data['product_cat'];
    $beds_exicts = false;
    global $quantity;
    $results = array();
    foreach ($product_cats as $cat) {
      if ($cat->term_id == 280 || $cat->term_id == 316) {
        $targeted_id = $cart_product_data['product_id'];
        if (in_array($targeted_id, array($cart_item['product_id'], $cart_item['variation_id']))) {
          $quantity =  $cart_item['quantity'];
          // return $quantity;
        }
        //   if($quantity == 1 && bbloomer_check_category_in_cart_before() == 1){
        //     $product->set_price($reg_price * 0.80);

        // }
        $results['found'] = 1;
        $results['qnt'] = $quantity;
        return $results;            // echo "beds excits :".$beds_exicts;
      } else {
        // echo "0";
      }
    }
  }
}
function get_parent_category_id($subcategory_id) {
  $subcategory = get_term($subcategory_id, 'product_cat'); // Replace 'product_cat' with your custom taxonomy if needed

  if (is_wp_error($subcategory) || empty($subcategory)) {
      return 0; // Return 0 if the sub-category doesn't exist
  }

  if ($subcategory->parent != 0) {
      return $subcategory->parent; // Return the parent category ID if it exists
  }

  return 0; // Return 0 if the sub-category doesn't have a parent
}


add_action('woocommerce_before_calculate_totals', 'bbloomer_alter_price_cart', 9999);

function bbloomer_alter_price_cart($cart)
{
  $D_on_off = get_field('toggle_discount', 'option');
  if ($D_on_off == 1) {

    $dynamic_discount = get_field('discount', 'option');
    $dynamic_percent = 1 - ($dynamic_discount / 100);
    // echo "This is Discount peercent : ", $dynamic_percent;

    // wp_die();
    global $targeted_id, $quantity, $reg_price;
    $cat_ids = array();
    $bads_count = bbloomer_check_bads_in_cart_before();
    // var_export($cart);
    // wp_die();
    if (is_admin() && !defined('DOING_AJAX')) return;

    if (did_action('woocommerce_before_calculate_totals') >= 2) return;

    // IF CUSTOMER NOT LOGGED IN, DONT APPLY DISCOUNT
    // if ( ! wc_current_user_has_role( 'customer' ) ) return;

    // LOOP THROUGH CART ITEMS & APPLY DISCOUNT
    foreach ($cart->get_cart() as $cart_item_key => $cart_item) {
      $product = $cart_item['data'];

      $beds_quantity = bbloomer_check_category_in_cart_before();

      $cart_product_data = mitch_get_short_product_data($cart_item['product_id']);
      $reg_price = $product->get_regular_price();
      //  $cart_product_data['product_price_b4_sale'];
      // =====================================================================
      $prices_string = strip_tags($product->get_price_html());
      $prices = explode("EGP", $prices_string);
      // $prices=preg_split('[0-9]',$prices);
      $regular_price = (int)str_replace('&nbsp;', '', $prices[0]);
      // $sale_price = (int)str_replace('&nbsp;', '', $prices[1]);
      $sale_price = $cart_product_data['product_sale_price'];
      // echo $sale_price;
      // wp_die();

      // product_sale_price
      $percents = explode("%", $prices[2]);
      $percent = intval($percents[0]);
      $decimal = floatval(1 - ($percent / 100));
      $orignal_price = intval($reg_price / $decimal);
      $simple_string_percent = explode("%", $percent);
      $simple_percent = $simple_string_percent[0];




      $quantity = $cart_item['quantity'];
      $product_cats = $cart_product_data['product_cat'];
      $beds_exicts = false;
      if ($product_cats) {
 
        foreach ($product_cats as $cat) {
          if ($cat->parent == 0) {
          } else {
            $cat_ids = array_merge(
              $cat_ids,
              $cart_item['data']->get_category_ids()
            );




            $parent_category = get_parent_category_id($cat->term_id);
            if($parent_category == 256 || $cat->term_id == 256 ){ //($cat->name == "مراتب" || $cat->term_id == 257 || $cat->term_id == 276 || $cat->term_id == 256 || $cat->term_id == 347  ) {
              if($cart_item['product_id'] == 7582 || $cart_item['product_id'] == 7583 || $cart_item['product_id'] == 7584 || $cart_item['product_id'] == 7585 ||$cart_item['product_id'] == 7587  ){
                return false;
              }
              $targeted_id = $cart_product_data['product_id'];
              $bads_data = mitch_get_short_product_data($targeted_id);
              $prices_string = strip_tags($bads_data['product_price']);
              $prices = explode("EGP", $prices_string);
              // $prices=preg_split('[0-9]',$prices);
              $regular_price = (int)str_replace('&nbsp;', '', $prices[0]);
              // $sale_price = (int)str_replace('&nbsp;', '', $prices[1]);
              $sale_price =  (int)str_replace('&nbsp;', '', $prices[1]);
              // $cart_product_data['product_sale_price'];
              // echo $sale_price;
              // print_r($prices_string);
              // wp_die();

              // product_sale_price
              $percent = $prices[2];
              $simple_string_percent = explode("%", $percent);
              $simple_percent = $simple_string_percent[0];
              // ========================================================================s
              if (in_array($targeted_id, array($cart_item['product_id'], $cart_item['variation_id']))) {
                $quantity =  $cart_item['quantity'];
              }


              // if(bbloomer_check_category_in_cart_before() == 1){
              if ($beds_quantity['found'] == 1) {
                if ($beds_quantity['qnt'] == $quantity) {

                  // $product->set_price(ceil(($reg_price * 0.85 * 0.80)));
                  $product->set_price(ceil(($reg_price  * $dynamic_percent)));
                }
                // elseif ($beds_quantity['qnt'] > 1 && $quantity == 1) {
                elseif ($beds_quantity['qnt'] >  $quantity) {

                  // $product->set_price(ceil((($quantity * ($reg_price * 0.85)) * 0.80) / $quantity));
                  $product->set_price(ceil(($quantity * $reg_price * $dynamic_percent) / $quantity));

                  // $product->set_price(((($quantity-$beds_quantity['qnt'])*$reg_price)+($beds_quantity['qnt'] * $reg_price * 0.80))/$quantity);


                } elseif ($beds_quantity['qnt'] < $quantity) {
                  // $product->set_price(ceil(((($quantity - $beds_quantity['qnt']) * ($reg_price * 0.85)) + ($beds_quantity['qnt'] * ($reg_price * 0.85 * 0.80))) / $quantity));
                  $product->set_price(ceil(((($quantity - $beds_quantity['qnt'])) + ($beds_quantity['qnt'] * ($reg_price * $dynamic_percent))) / $quantity));

                  // $product->set_price(((($quantity-1) * $reg_price)+$reg_price * 0.80)/$quantity);
                  // $product->set_price(($reg_price)-(($reg_price/$quantity) * 0.80));


                }
              }
              if ($quantity > 1 && $beds_quantity['found'] != 1 || $quantity > 1 && $beds_quantity['found'] == 1 || $bads_count > 1) {

                // $product->set_price(ceil((($quantity * $reg_price) * 0.80) / $quantity));
                $product->set_price(ceil((($quantity * $reg_price) * $dynamic_percent) / $quantity));
              }
            }
          }
        }
      }
    }
  } else {
  }
}
