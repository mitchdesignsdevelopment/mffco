<?php

/* Template Name: product-widget */


global $product;
if ($args) {
  $product_data = $args;
  // $product_id=$product_data['product_id'];
  // $discount = get_post_meta($product_id, '_sale_price', true); // Get the discount price
  // $regular_price = get_post_meta($product_id, '_price', true); // Get the regular price
  // if($discount&&$regular_price){
  // $discount_percentage = round((($regular_price - $discount) / $regular_price) * 100); }// Calculate the discount percentage

  // echo $discount_percentage . '%'; // Display the discount percentage with a percent sign

  // strip_tags(wc_price($product->price)) 
 

}
// strip_tags($product_data['product_price']);
$prices_string=strip_tags($product_data['product_price']);
$prices=explode("EGP",$prices_string);
// $prices=preg_split('[0-9]',$prices);
$regular_price=(int)str_replace('&nbsp;', '', $prices[0]);
$sale_price=(int)str_replace('&nbsp;', '', $prices[1]);
$percentage=(float)str_replace('&nbsp;', '', $prices[2]);



// $discount_percentage=round((($regular_price-$sale_price)/1)*100) ."%";
// print_r($product_data);
?>
<?php

?>
<li id="product_<?php echo $product_data['product_id']; ?>_block" class="product_widget" <?php if (isset($prod_anim_count)) {
                                                                                            echo 'data-aos="fade-left" data-aos-duration="' . $prod_anim_count . '"';
                                                                                          } ?>>

                                                                                          
  <?php //if(mitch_check_wishlist_product(get_current_user_id(), $product_data['product_id'])){ 
  ?>
  <!-- <span class="fav_btn favourite" onclick="remove_product_from_wishlist(<?php //echo $product_data['product_id'];
                                                                              ?>);"></span> -->
  <?php //}else{ 
  ?>
  <!-- <span class="fav_btn not-favourite" onclick="add_product_to_wishlist(<?php //echo $product_data['product_id'];
                                                                            ?>);"></span> -->
  <?php //} 
$product_id=$product_data['product_id'];
$product=wc_get_product($product_id);
$prices_string=strip_tags($product_data['product_price']);
$prices=explode("EGP",$prices_string);
// $prices=preg_split('[0-9]',$prices);
$regular_price=(float)str_replace('&nbsp;', '', $prices[0]);
$sale_price=(float)str_replace('&nbsp;', '', $prices[1]);
$percentage=(float)str_replace('&nbsp;', '', $prices[2]);

$check_branch = get_fields($product_id);

  ?>
    <a href="<?php echo $product_data['product_url']; ?>" class="product_widget_box <?php //echo $product_data['product_widget_background']; ?>">
        <div class="img">
            <img src="<?php echo $product_data['product_image']; ?>"
                alt="<?php echo $product_data['product_title']; ?>">
        </div>
        <div class="text">
            <div class="sec_info">
                <h3 class="title">
                    <?php echo $product_data['product_title']; ?>
                </h3>
                <?php if(!$product_data['hide_add_to_cart']):?>
                  <div class="price-content"><?php echo $product_data['product_price']; ?></div>
                  <?php if($percentage&&$product->is_on_sale()): ?>
                    <span class="note_discount">خصم <?php echo $percentage.'%'?></span>
                  <?php endif  ?>
                <?php endif;?>

                 <!-- check branch -->
                <?php $product_labels = get_the_terms($product_id, 'product-branchs');
                      if(!empty($product_labels)){
                ?>
                <ul class="product_label">
                  <?php foreach($product_labels as $label){ ?>
                  <li class="note_branch">
                    <?php echo $label->name ?>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
            </div>
            <div class="open_widget">
                <span></span>
            </div>
        </div>
    </a>
</li>