<?php global $language,$product;
$new_array = wp_get_post_terms( get_the_id(), 'product_cat');
if($new_array):
	$parent_cat = '';
foreach($new_array as $category):
	if($category->parent==0){
		$parent_cat = $category;
		break;
	}
endforeach;
endif;
?>
<div class="product related">
    <div class="grid">
        <div class="section_title">
        <?php if($parent_cat):?>
		<h2><?php echo($language=="en")?'Shop more of '.$parent_cat->name: " تسوق المزيد من ".$parent_cat->name;?></h2>
		<?php else:?>
		<h2><?php echo($language=="en")?'Shop more':"تسوق المزيد";?></h2>
		<?php endif;?>
      </div>
        <div class="product_container">
            <ul class="products_list">
            <?php
            $exclude_ids = array();
            if(!isset($product_id)){
              $product_id = get_the_id();
            }
            $related_products_ids = wc_get_related_productss($product_id, 4, $exclude_ids);
            if(!empty($related_products_ids)){
              foreach($related_products_ids as $r_product_id){
                $product_data = mitch_get_short_product_data($r_product_id);
                include 'product-widget.php';
              }
            }
            ?>
            </ul>
        </div>
    </div>
</div>
