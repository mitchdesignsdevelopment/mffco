<?php //$single_product_price = mitch_get_product_price_after_rate($single_product_data['main_data']->get_price());
?>
<?php $single_product_price = number_format($single_product_data['main_data']->get_price(), 0);
$product_obj     = mitch_get_short_product_data($single_product_data['main_data']->get_id());

$prices_string = strip_tags($product_obj['product_price']);
$prices = explode("EGP", $prices_string);
// $prices=preg_split('[0-9]',$prices);
$regular_price = (int)str_replace('&nbsp;', '', $prices[0]);
$sale_price = (int)str_replace('&nbsp;', '', $prices[1]);
$percent = $prices[2];
$simple_string_percent = explode("%", $percent);
$simple_percent = $simple_string_percent[0];

$cats = get_the_terms($single_product_data['main_data']->get_id(), 'product_cat');
// $fliter_prices=;
?>

<?php
$taxonomy = 'product_cat';
foreach ($cats as $cat_obj) {

  if ($cat_obj->parent == 0) {
  } else {
    $cat = $cat_obj;
    break;
  }
}

if ($cat) :
  $cat_term = get_term_by('id', $cat->term_id, 'product_cat');

  $tmp = array();
  $ex_fab = get_field('ex_fabrics', $single_product_data['main_data']->get_id());
  if ($ex_fab) {
    foreach ($ex_fab as $val) {
      $tmp[$val] = $val;
    }
  }

  if ($cat_term) :


    $fabrics = get_field('fabrics', $cat_term);
    //if (!$fabrics) {
    //  $parent = get_term($cat_term->parent);
    //  $fabrics = get_field('fabrics', $parent);



    //}
?>

<?php

  endif;
endif;

$check = get_fields($single_product_data['main_data']->get_id());
global $wpdb;
?>
<div class="sec_info">

  <div id="ajax_loader_single_product" style="display:none;">
    <div class="loader"></div>
  </div>

  <?php
  woocommerce_breadcrumb(
    array(
      'delimiter' => '',
      'wrap_before' => '<ul class="breadcramb">',
      'wrap_after' => '</ul>',
      'before' => '<li>',
      'after' => '</li>',
      'home' => $fixed_string['product_single_breadcrumb']
    )
  );
  ?>
  <div class="top">
    <div class="sec_top_flex">
      <h3 class="single_title_item">
        <?php echo $single_product_data['main_data']->get_name(); ?>
        <?php
        // if(mitch_check_wishlist_product(get_current_user_id(), $single_product_data['main_data']->get_id())){
        ?>
        <!-- <span class="fav_btn favourite" onclick="remove_product_from_wishlist(<?php //echo $single_product_data['main_data']->get_id();
                                                                                    ?>);"></span> -->
        <?php
        //}else{
        ?>
        <!-- <span class="fav_btn not-favourite" onclick="add_product_to_wishlist(<?php //echo $single_product_data['main_data']->get_id();
                                                                                  ?>);"></span> -->
        <?php
        // }
        ?>
      </h3>
      <div class="sec_label_flex">
        <?php if ($percent && $single_product_data['main_data']->is_on_sale()) : ?>
          <span class="note_discount">خصم<?php echo ($simple_percent) ? $simple_percent . "%" : $percent ?></span>
        <?php endif ?>
         <!-- check branch -->
          <?php $product_labels = get_the_terms(get_the_ID(), 'product-branchs');
				        if(!empty($product_labels)){
          ?>
          <ul class="product_label">
            <?php foreach($product_labels as $label){ ?>
            <li class="note_branch">
              <?php echo $label->name ?>
            </li>
            <?php } ?>
          </ul>
				  <?php } ?>
      </div>
    </div>

    <div class="description">
      <?php echo $single_product_data['main_data']->get_description(); ?>
    </div>

    <!-- <ul class="info">
      <?php
      //if (!empty($single_product_data['extra_data']['product_brand'])) {
      ?>
        <li><?php //echo $fixed_string['product_single_page_brand']; 
            ?></li>
      <?php
      //}
      //if (!empty($single_product_data['extra_data']['product_country'])) {
      ?>
        <li><?php //echo $fixed_string['product_single_page_country']; 
            ?></li>
      <?php
      //}
      //if (!empty($single_product_data['extra_data']['product_texture'])) {
      ?>
        <li><?php //echo $fixed_string['product_single_page_texture']; 
            ?></li>
      <?php
      //}
      ?>
    </ul>
    <ul class="info bold">
      <?php
      //if (!empty($single_product_data['extra_data']['product_brand'])) {
      ?>
        <li><?php //echo $single_product_data['extra_data']['product_brand']; 
            ?></li>
      <?php
      //}
      //if (!empty($single_product_data['extra_data']['product_country'])) {
      ?>
        <li><?php //echo $single_product_data['extra_data']['product_country']; 
            ?>
          <?php //if (!empty($single_product_data['extra_data']['product_country_code'])) { 
          ?>
            <span class="single_flag">
              <img src="<?php //echo $theme_settings['theme_url']; 
                        ?>/assets/img/flag/<?php //echo $single_product_data['extra_data']['product_country_code']; 
                                            ?>.png" alet="<?php //echo $single_product_data['main_data']->get_name(); 
                                                          ?>">
            </span>
          <?php //} 
          ?>
        </li>
      <?php
      //}
      //if (!empty($single_product_data['extra_data']['product_texture'])) {
      ?>
        <li><?php //echo $single_product_data['extra_data']['product_texture']; 
            ?></li>
      <?php
      //}
      ?>
    </ul> -->
  </div>


  <?php
  if (!$check['hide_add_to_cart_button']) {


    if ($single_product_data['main_data']->get_type() == 'simple') {
      if ($fabrics) {
  ?>
        <div class="middle">
          <?php
          $count = 0;
          $number_of_cols = count($fabrics);
          if ($number_of_cols) {
          ?>
            <div class="section_fabrics">



              <label><?php echo 'انواع القماش'; ?></label>
              <div class="fabrics <?php //echo ($number_of_cols > 9) ? ' fabrics_slick' : ''; 
                                  ?>" id="fabric_option">
                <div class="all">
                  <?php
                  foreach ($fabrics as $fabric) :
                    $fabric_stock = get_field('instock', $fabric['fabric']->ID);
                    if (!isset($tmp[$fabric['fabric']->ID]) && $fabric_stock == 1) {
                      $fabric_image = get_the_post_thumbnail_url($fabric['fabric']->ID);
                  ?>
                      <div class="single_fabrics fabric_option <?php echo (!$fabric_stock) ? 'disabled' : ''; ?> <?php echo ($count == 0) ? 'active' : ''; ?>" data-value="<?php echo $fabric['fabric']->post_name; ?>">
                        <span> <img src="<?php echo $fabric_image; ?>" alt="MFFCO" /></span>

                      </div>
                  <?php if ($fabric_stock) {
                        $count++;
                      }
                    }
                  endforeach; ?>
                </div>

              </div>
            </div>
          <?php } ?>
        </div>
    <?php }
    } ?>
    <div class="<?php echo ($single_product_data['main_data']->get_type() == 'simple') ? 'min_middle' : 'middle'; ?>">

      <?php


      if ($single_product_data['main_data']->get_type() == 'simple') {


      ?>
      <?php if(!get_field('disable_add_to_cart_button','7')): ?>
        <div class="section_count">
          <button class="increase" id="increase" onclick="increaseValue()" value="Increase Value"></button>
          <input class="number_count" type="number" id="number" value="1" data-min="1" data-max="<?php echo $single_product_data['main_data']->get_max_purchase_quantity(); ?>" />
          <button class="decrease disabled" id="decrease" onclick="decreaseValue()" value="Decrease Value"></button>
        </div>
      <?php endif; ?>

        <?php if ($single_product_data['main_data']->is_on_sale()) { ?>
          <span class="price">
            <!-- <del><span class="woocommerce-Price-amount amount"><bdi><?php //echo number_format($single_product_data['main_data']->get_regular_price(), 0); 
                                                                          ?><span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span></del>

            <ins><span class="woocommerce-Price-amount amount"><bdi><?php //echo number_format($single_product_data['main_data']->get_sale_price(), 0); 
                                                                    ?><span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span></ins> -->
            <?php

            echo $product_obj['product_price'];
            ?>
          </span>

        <?php } else { ?>
          <p class="price"><?php echo $single_product_price; ?> <?php echo $theme_settings['current_currency']; ?></p>
        <?php } ?>
        <?php $note = get_field('note', $single_product_data['main_data']->get_id());
        if ($note) : ?>
          <span class="product-note"><?php echo $note; ?></span>
        <?php endif; ?>
        <?php if(!get_field('disable_add_to_cart_button','7')): ?>
        <button type="button" onclick="simple_product_add_to_cart(<?php echo $single_product_data['main_data']->get_id(); ?>);">
          <?php echo $fixed_string['product_single_page_add_to_cart']; ?>
        </button>
        <?php endif; ?>
        <?php
      } elseif ($single_product_data['main_data']->get_type() == 'variable') {
        $default_attributes = $single_product_data['main_data']->get_default_attributes();
        if (empty($default_attributes)) {
          $default_attributes = array();
        }
        $variations_attr = array();
        $product_attributes = $single_product_data['main_data']->get_attributes();
        $product_variations = $single_product_data['main_data']->get_available_variations();
        if (!empty($product_variations)) {
          foreach ($product_variations as $variation_obj) {
            foreach ($variation_obj['attributes'] as $var_attr_key => $var_attr_value) {
              $variations_attr[] = mitch_get_product_attribute_name($var_attr_value);
            }
          }
        }
        if (!empty($product_attributes)) {
          foreach ($product_attributes as $attribute_key => $attribute_arr) {
            $attribute_name = str_replace('pa_', '', $attribute_arr['name']);
            if (($attribute_key != 'pa_color')) {
              $number_of_cols = count($attribute_arr['options']);
        ?>
              <div class="section_info section-attribute <?php echo (count($attribute_arr['options']) <= 1) ? "hide" : ''; ?>">
                <label><?php echo $fixed_string[$attribute_name]; ?></label>
                <div class="info" id="height_option">
                  <?php
                  if (!empty($attribute_arr['options'])) {
                    $count = 0;
                    foreach ($attribute_arr['options'] as $option_id) {
                      $active = '';
                      $color_name = mitch_get_product_attribute_name_by_id($option_id);
                      if (isset($default_attributes[$attribute_key])) {
                        if ($default_attributes[$attribute_key] == sanitize_title($color_name)) {
                          $active = 'active';
                        }
                      }
                      if (empty($active) && $count == 0) {
                        $active = 'active';
                      }
                      if (in_array($color_name, $variations_attr)) {
                        $term = get_term($option_id, $attribute_key);
                  ?>
                        <div class="test" style="display: none;"><?php print_r($term); ?></div>
                        <div class="single_info variation_option <?php echo $active; ?>" data-value="<?php echo $term->slug; ?>" data-key="<?php echo 'attribute_' . $attribute_key; ?>" onclick="get_availablility_variable_product(<?php echo $single_product_data['main_data']->get_id(); ?>);">
                          <p><?php echo $color_name; ?></p>
                        </div>
                  <?php
                        $count++;
                      }
                    }
                  }
                  ?>
                </div>
              </div>
            <?php } else { ?>
              <div class="section_color section-attribute <?php echo (count($attribute_arr['options']) <= 1) ? "hide" : ''; ?>">
                <label><?php echo $fixed_string[$attribute_name]; ?></label>
                <div class="colores <?php echo ($number_of_cols > 9) ? ' colore_slick' : ''; ?>" id="color_option">
                  <?php
                  if (!empty($attribute_arr['options'])) {
                    $count = 0;
                    $active_check = false;
                    foreach ($attribute_arr['options'] as $option_id) {
                      $active = '';
                      $color_code = get_field('color_hex_code', 'term_' . $option_id . '');
                      $color_image = get_field('color_image', 'term_' . $option_id . '');
                      $color_name = mitch_get_product_attribute_name_by_id($option_id);
                      if (isset($default_attributes['pa_color'])) {
                        if ($default_attributes['pa_color'] == sanitize_title($color_name)) {
                          $active = 'active';
                          $active_check = true;
                        }
                      } elseif (empty($active) && !$active_check) {
                        $active = 'active';
                        $active_check = true;
                      } elseif (empty($active) && $count == 0 && !$active_check) {
                        $active = 'active';
                        $active_check = true;
                      }
                      if (in_array($color_name, $variations_attr)) {
                        $term = get_term($option_id, $attribute_key);
                        $image_hex = get_field('image_or_hex_code', $term);
                  ?>
                        <div class="single_color variation_option <?php echo $active; ?>" data-value="<?php echo sanitize_title($color_name); ?>" data-count="<?php echo $count; ?>" data-key="<?php echo 'attribute_' . $attribute_key; ?>" onclick="get_availablility_variable_product(<?php echo $single_product_data['main_data']->get_id(); ?>);">
                          <?php if ($image_hex == 'image') : ?>
                            <span class="color-image"><img src="<?php echo $color_image; ?>" alt="MFFCO" /></span>
                          <?php else : ?>
                            <span class="color-border" style="background-color: <?php echo $color_code; ?>;"></span>
                            <!-- <p><?php // echo $color_name;
                                    ?></p> -->
                          <?php endif; ?>
                        </div>
                  <?php
                        $count++;
                      }
                    }
                  }
                  ?>
                </div>
              </div>
            <?php } ?>
            <?php



            if ($fabrics) :

              $count = 0;
              $number_of_cols = count($fabrics);
              if ($number_of_cols) :
            ?>
                <div class="section_fabrics">



                  <label><?php echo 'انواع القماش'; ?></label>
                  <div class="fabrics <?php //echo ($number_of_cols > 9) ? ' fabrics_slick' : ''; 
                                      ?>" id="fabric_option">
                    <div class="all">

                      <?php
                      foreach ($fabrics as $fabric) :
                        $fabric_stock = get_field('instock', $fabric['fabric']->ID);
                        if (!isset($tmp[$fabric['fabric']->ID]) && $fabric_stock == 1) {
                          $fabric_image = get_the_post_thumbnail_url($fabric['fabric']->ID);
                      ?>
                          <div class="single_fabrics fabric_option <?php echo (!$fabric_stock) ? 'disabled' : ''; ?> <?php echo ($count == 0) ? 'active' : ''; ?>" data-value="<?php echo $fabric['fabric']->post_name; ?>">
                            <span> <img src="<?php echo $fabric_image; ?>" alt="MFFCO" /></span>

                          </div>
                      <?php if ($fabric_stock) : $count++;
                          endif;
                        }
                      endforeach; ?>
                    </div>
                  </div>
                </div>
        <?php endif;

            endif;
          }
        } ?>

    </div>


    <div class="min_middle variable_middle">
      <?php if (!$check['kitchen']) : ?>
        <?php if(!get_field('disable_add_to_cart_button','7')): ?>
          <div class="section_count">
            <button class="increase" id="increase" onclick="increaseValue()" value="Increase Value"></button>
            <input class="number_count" type="number" id="number" value="1" data-min="1" data-max="" />
            <button class="decrease disabled" id="decrease" onclick="decreaseValue()" value="Decrease Value"></button>
          </div>
        <?php endif; ?>
      <?php endif; ?>

      <?php if ($single_product_data['main_data']->is_on_sale()) { ?>
        <span class="price">
          <!-- <del><span class="woocommerce-Price-amount amount"><bdi><?php //echo $single_product_data['main_data']->get_regular_price(), 0; 
                                                                        ?><span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span></del>

          <ins><span class="woocommerce-Price-amount amount"><bdi><?php //echo $single_product_data['main_data']->get_sale_price(), 0; 
                                                                  ?><span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span></ins> -->
          <?php

          echo $product_obj['product_price'];
          ?>
        </span>

      <?php } else { ?>
        <p class="price"><?php echo $single_product_price; ?> <?php echo $theme_settings['current_currency']; ?></p>
      <?php } ?>
      <?php $note = get_field('note', $single_product_data['main_data']->get_id());
        if ($note) : ?>
        <span class="product-note"><?php echo $note; ?></span>
      <?php endif; ?>
      
      <?php if(!get_field('disable_add_to_cart_button','7')): ?>
      <button id="variable_product_add_to_cart" onclick="variable_product_add_to_cart(<?php echo $single_product_data['main_data']->get_id(); ?>);" type="button"><?php echo ($check['kitchen'] == '1') ? $fixed_string['kitchen_product_single_page_add_to_cart'] : $fixed_string['product_single_page_add_to_cart']; ?></button>
      <?php endif; ?>
    </div>

  <?php } ?>
  <?php if ($single_product_data['main_data']->get_type() == 'simple') { ?>
</div>
<?php }
  } ?>




<div class="dropdown_info_old">
  <?php if ($check['product_components']) : ?>
    <div class="single_info">
      <h3 class="title_info">مكونات المنتج</h3>
      <div class="content_info">
        <?php
        $components = explode('|', $check['product_components']);
        foreach ($components as $component_sku) {
          $product_id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $component_sku));
        ?>
          <p><?php echo get_the_title($product_id); ?></p>
        <?php } ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($check['made_from']) : ?>
    <div class="single_info">
      <h3 class="title_info">مصنوع من</h3>
      <div class="content_info">
        <p><?php echo $check['made_from']; ?></p>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($check['product_measurements'] && !$check['has_accordion']) : ?>
    <div class="single_info">
      <h3 class="title_info">قياسات المنتج</h3>
      <div class="content_info">
        <p><?php echo $check['product_measurements']; ?></p>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($check['warranty']) : ?>
    <div class="single_info">
      <div class="section_ataraxy">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon_security.png" alt="">
        <h3 class="title"><?php echo $check['warranty']; ?></h3>
      </div>
    </div>
    <!-- <div class="single_info">
          <h3 class="title_info">الضمان</h3>
          <div class="content_info">
            <p><?php //echo $check['warranty']; 
                ?></p>
          </div>
        </div> -->
  <?php endif; ?>
</div>
<?php if ($check['has_accordion']) : ?>
  <div class="dropdown_info_new">
    <div class="single_info">
      <div class="product_details_box">
        <div class="section_title">
          <ul class="titles">
            <?php $count = 1;
            foreach ($check['product_measurements_accordion'] as $single_com) : ?>
              <li>
                <a href="#title_<?php echo $count; ?>" class="single_title <?php echo ($count == 1) ? 'active' : ''; ?>"><?php echo $single_com['title']; ?></a>
              </li>
            <?php $count++;
            endforeach; ?>
          </ul>
        </div>
        <div class="section_details">
          <?php $count = 1;
          foreach ($check['product_measurements_accordion'] as $single_com) : ?>
            <div id="title_<?php echo $count; ?>" class="info_box <?php echo ($count == 1) ? 'active' : ''; ?>">
              <ul class="content_details">
                <?php foreach ($single_com['content'] as $single_com_content) : ?>
                  <li class="single_content">
                    <h4><?php echo $single_com_content['text']; ?></h4>
                    <p><?php echo $single_com_content['value'] . ' cm'; ?></p>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php $count++;
          endforeach; ?>
        </div>
      </div>
    </div>
    <!-- <div class="single_info">
            <h3 class="title">مكونات المنتج</h3>
            <div class="content_info">
              <p>خشب طبيعي وتصميمات عصرية ومريحة مكونة من (بلاكار ـ سرير ١٦٠ ـ ٢ كمود ـ تواليت ) بامكانية تغير مقاس السرير و نوع الخشب</p>
            </div>
        </div> -->

  </div>
<?php endif; ?>
</div>