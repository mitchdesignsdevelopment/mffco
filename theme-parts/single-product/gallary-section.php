<div class="sec_gallary">
  <div class="gallary">
    <div class="product-slider">
      <?php //echo get_the_post_thumbnail_url($single_product_data['main_data']->get_id()); ?>
      <?php
      if(!empty($single_product_data['images']['full'])){
        foreach($single_product_data['images']['full'] as $product_single_full_image){
          ?>
          <div class="product_img">
            <img class="slider-main-img zoom" src="<?php echo $product_single_full_image;?>" data-zoom-image="<?php echo $product_single_full_image;?>"  alet="<?php echo $single_product_data['main_data']->get_name();?>">
          </div>
          
          <?php
        }
      }
      ?>
    </div>
    <div class="slider-nav">
      <?php
      if(!empty($single_product_data['images']['thumb'])){
        $count = 0;
        foreach($single_product_data['images']['thumb'] as $product_single_thumb_image){
          ?>
          <img  class="slider-nav-img-<?php echo $single_product_data['main_data']->get_gallery_image_ids()[$count];?>" src="<?php echo $product_single_thumb_image;?>" alet="<?php echo $single_product_data['main_data']->get_name();?>">
          <?php
          $count++;
        }
      }
      ?>
    </div>
  </div>
  <?php
  if(!empty($single_product_data['video_url'])){
    ?>
    <div class="player">
      <div class="video-box js-videoWrapper">
        <div class="bg" style="background-image: url(<?php echo $single_product_data['video_thumbnail'];?>);"><button id="playvideo" class="player js-videoPlayer"></button></div>
          <div class="youtube-video">

            <video id="videoclip" class="videoIframe js-videoIframe" width="100%" height="100%" controls>
                <source id="video1" src="<?php echo $single_product_data['video_url'];?>" type="video/mp4">
            </video>
            <!-- <iframe id="video1" class="videoIframe js-videoIframe" src=" <?php //echo $single_product_data['video_url'];?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
          </div>
        </div>
    </div>
   
    <?php
  }
  ?>
</div>
