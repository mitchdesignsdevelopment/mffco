<?php global $language,$checkout_chk;
if(is_checkout()){
  $checkout_chk = true;
}
?>
<div class="section_header">
    <div class="section_header_col_one">
        <div class="grid">
            <div class="exstra_link">
            <?php if(get_field('note_shipping' , 'options')): ?>
                <p style="position: absolute ; right:2% ;">
                <?php echo get_field('note_shipping' , 'options') ?>
                  <a href="tel:<?php echo get_field('note_shipping_number' , 'options') ?>"><?php echo get_field('note_shipping_number' , 'options') ?></a>
                </p>
                <?php endif; ?>
                <?php
                    wp_nav_menu(
                      array(
                        'theme_location' => 'top-nav-menu-'.$theme_settings['current_lang'].'',
                        'container'      => 'nav',
                      )
                    );
                ?>
            </div>
        </div>
    </div>
    <div class="section_header_col_two">
        <div class="grid">
            <div class="section_top">
                <div class="side_right">
                    <div class="logo">
                        <a href="<?php echo $theme_settings['site_url'];?>">
                          <img src="<?php echo $theme_settings['theme_logo'];?>" alt="">
                        </a>
                    </div>
                    <div class="new_search search">
                                <div class="loader_search" style="display: none;"></div>
                                <div class="sec_search">
                                <form method="get" id="searchForm" onsubmit="event.preventDefault(); navigateMyForm();" class="search-formm" action="<?php echo home_url( '/' ); ?>">
                                    <input type="search" id="newSearch" class="new-search" placeholder="<?php echo($language=="en")?'Sulfate free Shampoo': 'غرفة نوم رئيسية'; ?>" autocomplete="off" value="<?php echo $_GET['search'];?>" />
                                    <input type="submit" id="searchSubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
                                </form>
                                </div>
                                <div class="search-result" id="searchResult">
                                    </div>
							      </div>	
                    <!-- <div class="search">
                        <form action="">
                            <input id="gsearch" type="search" name="gsearch" placeholder="غرفة نوم رئيسية">
                            <span class="material-icons-outlined">search</span>
                            <div id="search_results" ></div>
                        </form>
                    </div> -->
                </div>
                <div class="side_left">
                  <?php if(is_user_logged_in()){ ?>
                    <div style="display: none;" class="track_order">
                        <a href="<?php echo home_url('tracking-order');?>">تتبع الطلب</a>
                    </div>
                    <div style="display: none;" class="wishlist">
                        <a href="<?php echo home_url('wishlist');?>"><span class="material-icons-outlined">favorite</span></a>
                    </div>
                    <div class="my_account">
                      <?php $current_user = wp_get_current_user();?>
                      <a class="title_myaccount" href="">
                        <img src="<?php echo($current_user && $current_user->ID != 0)? get_avatar_url($current_user->ID):$theme_settings['theme_url'];?>/assets/img/icons/my-account.png" alt="">
                        <p>حسابي </p>
                      </a>
                      <div class="my_account_list">
                        <ul>
                            <li class="<?php echo mitch_get_active_page_class('my-account');?>"><a href="<?php echo home_url('my-account');?>"><?php echo $fixed_string['myaccount_page_sidebare_home'];?></a></li>
                            <li class="<?php echo mitch_get_active_page_class('orders-list');?>"><a href="<?php echo home_url('my-account/orders-list');?>"><?php echo $fixed_string['myaccount_page_sidebare_orders'];?></a></li>
                            <li class="<?php echo mitch_get_active_page_class('addresses');?>"><a href="<?php echo home_url('my-account/addresses');?>"><?php echo $fixed_string['myaccount_page_sidebare_address'];?></a></li>
                            <li class="<?php echo mitch_get_active_page_class('profile');?>"><a href="<?php echo home_url('my-account/profile');?>"><?php echo $fixed_string['myaccount_page_sidebare_profile'];?></a></li>
                            <li><a href="<?php echo wp_logout_url(home_url());?>"><?php echo $fixed_string['myaccount_page_sidebare_logout'];?></a></li>
                        </ul>
                      </div>
                    </div>
                    <?php }else{ ?>
                    <div class="my_account">
                        <a href="#popup-login" class="title_myaccount login js-popup-opener">تسجيل الدخول</a>
                    </div>
                    <?php } if(!is_cart()){ ?>
                    <div class="cart">
                        <!-- <a href="<?php// echo home_url('cart');?>"> -->
                        <a href="#popup-min-cart" class="js-popup-opener">
                        <div class="section_icon_cart">
                          <?php //echo WC()->cart->get_total();?>
                              <span id="cart_total_count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                              <span class="material-icons">shopping_cart</span>
                        </div>
                        </a>
                    </div>
                  <?php  } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section_header_col_three">
        <div class="grid">
            <div class="section_bottom">
              <nav class="main-nav">
                <ul class="main-menu">
                <?php
                $header_items = get_field('header_builder_'.$theme_settings['current_lang'], 'options');
                // echo '<pre>';
                // var_dump($header_items);
                // echo '</pre>';
                if(!empty($header_items)){
                  foreach($header_items as $header_item){
                    if(!$header_item['item_group']['item_has_mega_dropdown']){
                      ?>
                      <li class="single_menu">
                        <a href="<?php if($header_item['item_url_page']){echo $header_item['item_url_page'];}else{echo get_term_link($header_item['item_url_product_cat']->term_id, $header_item['item_url_product_cat']->taxonomy);}?>"
                          class="category_link">
                          <?php echo $header_item['item_name'];?>
                        </a>
                      </li>
                      <?php
                    }else{
                      ?>
                      <li class="single_menu has-mega">
                        <div class="test" style="display:none">
                      <?php 
                      echo "this is header_item['item_url_page']".$header_item['item_url_page'];
                      echo "<br>";
                      echo "this is get_term_link(header_item['item_url_product_cat']->term_id, header_item['item_url_product_cat']->taxonomy)".get_term_link($header_item['item_url_product_cat']->term_id, $header_item['item_url_product_cat']->taxonomy);
                      // echo
                      ?>
                      
                      </div>
                        <a href="<?php if($header_item['item_url_page']&&!$header_item['item_url_product_cat']){echo $header_item['item_url_page'];}else{echo get_term_link($header_item['item_url_product_cat']->term_id, $header_item['item_url_product_cat']->taxonomy);}?>"
                          class="category_link">
                          <?php echo $header_item['item_name'];?>
                        </a>
                        <div class="mega-menu">
                          <div class="box grid">
                            <div class="list_subcategory">
                              <?php
                              $items_data = $header_item['item_group']['mega_menu_options'];
                              foreach($items_data['mega_menu_cols'] as $menu_col){
                                ?>
                                <a class="col" href="<?php echo get_term_link($menu_col['item']);?>">
                                  <h3><?php echo $menu_col['item']->name;?></h3>
                                </a>
                                <?php
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      </li>
                      <?php
                    }
                  }
                }
                ?>
                </ul>
              </nav>
              <!-- .main-nav -->
                <!-- <div class="dropdown_select">
                    <a href="#popup-switch-language" class="switch_language js-popup-opener">
                        <div class="sec_lang">
                            <div class="lang_text">
                              <span class="lang text">Arabic  <strong class="text">(<?php //echo $theme_settings['current_currency'];?>)</strong></span>
                            </div>
                            <img src="<?php //echo $theme_settings['theme_url'];?>/assets/img/flag/ksa.png" alt="">
                        </div>
                    </a>
                </div> -->
            </div>
        </div>
    </div>
</div>