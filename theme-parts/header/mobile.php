<div class="section_header_mobile">
  <?php if(get_field('note_shipping' , 'options')): ?>
    <div class="section_header_col_one">
          <div class="exstra_link">
              <p>
                <?php echo get_field('note_shipping' , 'options') ?>
                <a href="tel:<?php echo get_field('note_shipping_number' , 'options') ?>"><?php echo get_field('note_shipping_number' , 'options') ?></a>
              </p>
          </div>
    </div>
  <?php endif; ?>

  <div class="section_header_col_two">
      <div class="top_mobile">
          <div class="right">
              <div class="logo">
                  <a href="<?php echo $theme_settings['site_url'];?>">
                      <img src="<?php echo $theme_settings['theme_logo'];?>" width="120" alt="">
                  </a>
              </div>
          </div>
          <div class="left">
              <div class="section_search">
                    <div class="icon open_search">
                        <img class="open" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/icon_search_mob.png" alt="">
                    </div>
                 
                    <div class="new_search search">
                          <div class="loader_search" style="display: none;"></div>
                            <div class="sec_search">
                                <form method="get" id="searchForm" onsubmit="event.preventDefault(); navigateMyForm();" class="search-formm" action="<?php echo home_url( '/' ); ?>">
                                    <input type="search" id="newSearch" class="new-search" placeholder="<?php echo($language=="en")?'Sulfate free Shampoo': 'غرفة نوم رئيسية'; ?>" autocomplete="off" value="<?php echo $_GET['search'];?>" />
                                    <input type="submit" id="searchSubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
                                </form>
                                <div class="icon close_search">
                                    <img class="close" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/close_search.png" alt="">
                                </div>
                            </div>
                            <div class="search-result" id="searchResult"></div>
                    </div>
              </div>
            <?php if(!is_cart()){ ?>
              <div class="cart">
                  <!-- <a href="<?php// echo home_url('cart');?>"> -->
                  <a href="#popup-min-cart" class="js-popup-opener">
                    <div class="section_icon_cart">
                      <?php //echo WC()->cart->get_total();?>
                      <span id="cart_total_count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                      <span class="material-icons">shopping_cart</span>
                    </div>
                  </a>
              </div>
            <?php } ?>
              <div class="menu_button_mobile">
                <button type="button" class="menu_mobile_icon open"><i class="material-icons">menu</i></button>
                <button class="menu_mobile_icon close"><i class="material-icons">close</i></button>
              </div>
          </div>
      </div>
  </div>
  <div class="section_header_col_three">
      <div class="section_bottom">
        <nav class="main-nav">
          <ul class="main-menu">
            <?php
            $header_items = get_field('header_builder_'.$theme_settings['current_lang'], 'options');
            // echo '<pre>';
            // var_dump($header_items);
            // echo '</pre>';
            if(!empty($header_items)){
              foreach($header_items as $header_item){
                if(!$header_item['item_group']['item_has_mega_dropdown']){
                  ?>
                  <li class="single_menu">
                    <a href="<?php if($header_item['item_url_page']){echo $header_item['item_url_page'];}else{echo get_term_link($header_item['item_url_product_cat']->term_id, $header_item['item_url_product_cat']->taxonomy);}?>"
                      class="category_link">
                      <?php echo $header_item['item_name'];?>
                    </a>
                  </li>
                  <?php
                }else{
                  ?>
                  <li class="single_menu has-mega">
                  <a href="<?php if($header_item['item_url_page']&&!$header_item['item_url_product_cat']){echo $header_item['item_url_page'];}else{echo get_term_link($header_item['item_url_product_cat']->term_id, $header_item['item_url_product_cat']->taxonomy);}?>"
                      class="category_link">
                      <?php echo $header_item['item_name'];?>
                    </a>
                  </li>
                  <?php } } } ?>
          </ul>
        </nav>
      </div>
  </div>
  <div class="mobile-nav">
    <div class="mobile-menu">
      <nav class="main-navigation">
        <nav class="menu-main-nav-container">
          <ul class="menu">
          <?php
          $header_items = get_field('header_builder_'.$theme_settings['current_lang'], 'options');
          if(!empty($header_items)){
            foreach($header_items as $header_item){
              if(!$header_item['item_group']['item_has_mega_dropdown']){
                ?>
                <li class="menu-item">
                    <a href="<?php if($header_item['item_url_page']){echo $header_item['item_url_page'];}else{echo get_term_link($header_item['item_url_product_cat']->term_id, 'product_cat');}?>">
                      <?php echo $header_item['item_name'];?>
                    </a>
                </li>
                <?php
              }else{
                ?>
                <li class="menu-item menu-item-has-children">
                    <a class="menu-trigger" href="<?php if($header_item['item_url_page']){echo $header_item['item_url_page'];}else{echo get_term_link($header_item['item_url_product_cat']->term_id, 'product_cat');}?>">
                      <?php echo $header_item['item_name'];?>
                    </a>
                    <div class="list_subcategory_mobile">
                      <?php
                      $items_data = $header_item['item_group']['mega_menu_options'];
                      foreach($items_data['mega_menu_cols'] as $menu_col){
                        ?>
                        <div class="col">
                          <a href="<?php echo get_term_link($menu_col['item']);?>"><h3><?php echo $menu_col['item']->name;?></h3></a>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                </li>
                <?php
              }
            }
          }
          ?>
          </ul>
        </nav>
        <div class="supporting_nav">
          <?php
            wp_nav_menu(
              array(
                'theme_location' => 'top-nav-menu-'.$theme_settings['current_lang'].'',
                'container'      => 'nav',
              )
            );
          ?>
             <!-- <div class="supporting_nav more">
                <ul>
                  <li>
                      <a  class="" href="<?php //echo home_url('terms-and-conditions');?>"><?php //echo ($language == 'en')? get_field('page_title_en'): get_the_title(3); ?></a>
                  </li>
                </ul>
              </div> -->
        </div>
     
        <div class="more_support_menu">
          <?php if(is_user_logged_in()){ ?>
              <div class="track_order">
                  <a href="<?php echo home_url('tracking-order');?>">تتبع الطلب</a>
              </div>
              <div class="wishlist">
                  <a href="<?php echo home_url('wishlist');?>">المفضل</a>
              </div>
            <div class="my_account">
              <a class="title_myaccount" href="<?php echo home_url('my-account');?>">
              حسابي
              </a>
            </div>
            <?php }else{ ?>
            <div class="my_account">
                <a href="#popup-login" class="title_myaccount login open_login_mobile js-popup-opener">تسجيل الدخول</a>
            </div>
          <?php } ?>
          <!-- <div class="switch_lang">
              <a href="<?php //echo home_url('tracking-order');?>">English <img src="<?php //echo $theme_settings['theme_url'];?>/assets/img/flag/eng.png" alt=""></a>
          </div> -->
        </div>
      </nav>
    </div>
  </div>
</div>
