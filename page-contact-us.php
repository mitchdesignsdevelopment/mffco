<?php require_once 'header.php';?>
<link rel='stylesheet' id='cf-front-css'  href='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/css/caldera-forms-front.min.css?ver=1.9.6' type='text/css' media='all' />
<link rel='stylesheet' id='cf-render-css'  href='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/clients/render/build/style.min.css?ver=1.9.6' type='text/css' media='all' />
<div id="page" class="site" style="min-height: 1000px;">
  <?php require_once 'theme-parts/main-menu.php'; 

// if(get_field('csv_file','option'))
// {
//   // echo 'jims1';
//   // load csv with SERVER PATH instead of URL
//   $csv = get_attached_file(get_field('csv_file','option')['id']);
//   // var_dump($csv); 
//   if(($handle = fopen($csv, "r")) !== FALSE)
//   {
//     // echo 'jims2';
//     $count=0;
//     $cities = [];
//     while(($data = fgetcsv($handle, 1000, ",")) !== FALSE)
//     $cities[] = $data[$count];
//     {
//       // $count = acf row, $data[0] = csv column 1 value
//       // update_sub_field(array('ci', $count, 'name'), $data[0]);
//       // update_sub_field(array('ci', $count, 'address'), $data[1]);
//       // update_sub_field(array('ci', $count, 'phone'), $data[2]);
//       $count++;
//     }
//     fclose($handle);
//   }
//   $csv = get_attached_file(get_field('csv_file_2','option')['id']);
//   // var_dump($csv); 
//   if(($handle = fopen($csv, "r")) !== FALSE)
//   {
//     // echo 'jims2';
//     $count=0;
//     $cost = [];
//     while(($data = fgetcsv($handle, 1000, ",")) !== FALSE)
//     $cost[] = $data[$count];
//     {
//       // $count = acf row, $data[0] = csv column 1 value
//       // update_sub_field(array('ci', $count, 'name'), $data[0]);
//       // update_sub_field(array('ci', $count, 'address'), $data[1]);
//       // update_sub_field(array('ci', $count, 'phone'), $data[2]);
//       $count++;
//     }
//     fclose($handle);
//   }
//   $count=0;
//   foreach($cities as $city){
  //   $row = array(
  //     'city' => $city,
  //     'cost'   => $cost[$count],
  // );
  // add_row('cities_cost', $row, 'option');
//     $count++;
//   }

// }$csv = get_attached_file(get_field('csv_file_2','option')['id']);
// if(($handle = fopen($csv, "r")) !== FALSE){
//   global $wpdb;
//   $wpdb->query("DELETE FROM `wp_options` WHERE `option_name` LIKE '%cities_cost%'");
//   while(($data = fgetcsv($handle, 1000, ",")) !== FALSE){
//       // echo '<pre>';
//       // var_dump($data);
//       // echo '</pre>';
//       $row = array(
//           'city' => $data[0],
//           'cost' => $data[1],
//       );
//       add_row('cities_cost', $row, 'option');
//   }
// }
// fclose($handle);
$page_content = get_field('contact_page');?>
  <!--start page-->
  <div class="site-content contact_us">
     <div class="section_contact_us">
         <div class="sec_form">
             <div class="grid">
                <div class="section_title">
                    <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/mffco_icon.png" alt="" width="60">
                    <h2><?php echo $page_content['hero_section']['title'];?></h2>
                    <p><?php echo $page_content['hero_section']['subtitle'];?></p>
                </div>
                <div class="form_content">
                    <!-- <h3><?php //echo $page_content['hero_section']['form_title'];?></h3> -->
                    <?php echo do_shortcode('[caldera_form id="CF6241f70f2e55a"]');?>
                </div>
             </div>
         </div>
     </div>

     <div class="section_branches">
         <div class="grid">
              <div class="section_title">
                  <h3 class="title"><?php echo $page_content['branches_section']['title'];?></h3>
                  <div class="text_info">
                    <p><?php echo $page_content['branches_section']['sub_title_branch'];?></p>
                  </div>
              </div>
              <div class="all_branches">
              <?php if($page_content['branches_section']['branches']): foreach($page_content['branches_section']['branches'] as $branch):?>
                <div class="single_branche">
                      <h4><?php echo $branch['title'];?></h4>  
                      <ul>
                          <li class="pin_drop">
                          <?php echo $branch['location_text'];?>
                          </li>
                          <li class="call">
                          <?php echo $branch['phone'];?>
                          </li>
                          <!-- <li class="schedule">
                            <?php // echo $branch['button']['url'];?>
                            <?php // echo $branch['button']['title'];?>
                          </li> -->
                      </ul>
                      <a href="<?php echo $branch['button']['url'];?>" target="_blank" class="google_map">خرائط جوجل</a>
                </div>      
                         <?php endforeach;endif;?>
              </div>
         </div>
     </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/js/jquery-baldrick.min.js?ver=1.9.6' id='cf-baldrick-js'></script>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/js/parsley.min.js?ver=1.9.6' id='cf-validator-js'></script>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/clients/render/build/index.min.js?ver=1.9.6' id='cf-render-js'></script>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/js/caldera-forms-front.min.js?ver=1.9.6' id='cf-form-front-js'></script>
