<?php require_once 'header.php';?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content home">
      <div class="page_home">
            <div class="player">
                <div class="sec_video">
                    <video muted  autoplay loop class="player__video viewer" src="<?php echo $theme_settings['theme_url'];?>/assets/img/hero_video.mp4"></video>
                </div>
                <div class="section_text_hero">
                    <p> تعرف على أصول الفخامة، الدقة و الجودة</p>
                    <h1>مجموعة صيف شاهين 2022</h1>
                    <a href="<?php the_permalink(6);?>">تسوق الآن</a>
                </div>
                <div class="player__controls paused">
                    <button class="player__button toggle" title="Toggle Play"></button>
                </div>
                <div class="section_full_video">
                    <a href="#popup-full-video" class="js-popup-opener">
                        <div class="bg" style="background-image: url(<?php echo $theme_settings['theme_url'];?>/assets/img/bg_full.png);">
                        <button class="player js-videoPlayer"></button>
                        <p>شاهد الفيديو بالكامل</p>
                    </div>
                    </a>

                </div>
            </div>
          <div class="video-box js-videoWrapper">

              <div class="youtube-video">
                <iframe src="https://www.youtube.com/embed/awJ5xFVNTf8?autoplay=1&mute=1&rel=0&fs=0&controls=1&disablekb=1" width="100%" height="650" frameborder="0"></iframe>
              </div>

          </div>
          <div class="section_about_home">
              <div class="grid">
                  <div class="title">
                      <img data-aos="fade-down" data-aos-duration="1000" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/Combined_Shape.png" alt="" width="50">
                      <h3 data-aos="fade-down" data-aos-duration="1500"> تعرف على أصول الفخامة، الدقة و الجودة</h3>
                      <p data-aos="fade-down" data-aos-duration="2000">أنشئت نسيج عام 2006 تجسيداً لرؤية الرئيس التنفيذى القائمة على تغيير المنظور للثوب القطرى من حيث الجودة وأساليب اإلنتاج ، ومستوى الخدمات تطلعاً لبناء أول عالمة تجارية مسجلة فى انتاج الثوب القطرى والخليجى الذي يتميز بقدرته علي مواكبة متطلبات العصر</p>
                  </div>
                  <div class="content">
                      <div class="single" data-aos="fade-left" data-aos-duration="1000">
                          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/home_about_01.png" alt="">
                          <p>نخبة من أكفأ الخياطين و فريق عمل المدرب على أعلي مستوى</p>
                      </div>
                      <div class="single" data-aos="fade-left" data-aos-duration="1500">
                          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/home_about_02.png" alt="">
                          <p> خطوات عديدة لضمان التأكد من حصولك على أجود ثوب</p>
                      </div>
                      <div class="single" data-aos="fade-left" data-aos-duration="2000">
                          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/home_about_03.png" alt="">
                          <p>أفخم خامات أقمشة مستوردة من جميع أنحاء العالم</p>
                      </div>
                  </div>
                  <div class="button_home" data-aos="fade-down" data-aos-duration="2000">
                      <a href="<?php the_permalink(481);?>">من نحن</a>
                  </div>
              </div>
          </div>
          <div class="section_shop_home">
              <div class="grid">
                  <div class="sec_info row_reverse">
                      <div class="text">
                          <img  data-aos="fade-down" data-aos-duration="1000" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/Combined_Shape.png" alt="" width="50">
                          <h3  data-aos="fade-down" data-aos-duration="1500">رؤيتنا</h3>
                          <p  data-aos="fade-down" data-aos-duration="2000">ان نكون فى نسيج المسؤولين عن إنتاج أكثر من نصف أثواب الشعب القطري بحرصنا على أن تكون منتجاتنا وخدماتنا األفضل جودة. </p>
                          <div class="button_home"  data-aos="fade-down" data-aos-duration="2500">
                              <a href="<?php the_permalink(6);?>">تسوق الآن</a>
                          </div>
                      </div>
                      <div class="img" data-aos="fade-left"  data-aos-duration="1500">
                          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/about_01.png" alt="">
                      </div>
                  </div>
              </div>

          </div>
          <div class="section_item_home">
              <div class="grid">
                  <div class="section_title_home">
                      <div class="title">
                          <img src="<?php echo $theme_settings['theme_favicon_black'];?>" alt="">
                          <h2>وصل حديثا</h2>
                      </div>
                      <a href="">جميع منتجات وصل حديثا</a>
                  </div>
                  <div class="product_container">
                      <ul class="products_list">
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-08.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-09.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-10.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-1.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="section_shop_home">
              <div class="grid">
                  <div class="sec_info">
                      <div class="text">
                          <img  data-aos="fade-down" data-aos-duration="1000" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/Combined_Shape.png" alt="" width="50">
                          <h3 data-aos="fade-down" data-aos-duration="1500">جودة نفخر بهامنذ عام 2006</h3>
                          <p data-aos="fade-down" data-aos-duration="2000">أنشئت نسيج عام 2006 تجسيداً لرؤية الرئيس التنفيذى القائمة على تغيير المنظور للثوب القطرى من حيث الجودة وأساليب اإلنتاج ، ومستوى الخدمات تطلعاً لبناء أول عالمة تجارية مسجلة فى انتاج الثوب القطرى والخليجى الذي يتميز بقدرته علي مواكبة متطلبات العصر</p>
                      </div>
                      <div class="img" data-aos="fade-right"  data-aos-duration="1500">
                          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/about_01.png" alt="">
                      </div>
                  </div>
              </div>

          </div>
          <div class="section_item_home">
              <div class="grid">
                  <div class="section_title_home">
                      <div class="title">
                          <img src="<?php echo $theme_settings['theme_favicon_black'];?>" alt="">
                          <h2>منتجات أكثر مبيعا</h2>
                      </div>
                      <a href="">جميع منتجات الأكثر مبيعا</a>
                  </div>
                  <div class="product_container">
                      <ul class="products_list">
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-08.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-09.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-10.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                          <li class="product_widget" data-aos="fade-left" data-aos-duration="1000">
                              <a href="" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img-1.png" alt="">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">نسيج المجلس<span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span></h3>
                                      <p  class="brand">الماركة : <span>شيكيبو</span></p>
                                      <p class="price">200 USD</p>
                                  </div>
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
          <?php include_once 'theme-parts/instagram.php';?>
      </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
