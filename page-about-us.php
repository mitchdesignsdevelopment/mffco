<?php
require_once 'header.php';
$page_content = get_field('about_page');
// var_dump($page_content);
?>
<style>
/* .page_about .section_slide .single_slide .text .content {
  color: #000000;
  font-size: 20px;
  font-weight: normal;
  line-height: 36px;
} */
</style>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
    <div class="site-content about">
      <div class="page_about">
          <div class="section_title">
              <div class="grid">
                  <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/about_logo.png" alt="" width="50">
                  <h1 ><?php echo the_title(); ?></h1>
                  <div class="content">
                     <?php echo the_content(); ?>
                  </div>
              </div>
          </div>
          <div class="grid">
            <div class="hero_about">
              <img src="<?php the_post_thumbnail_url(); ?>" alt="">
            </div>
            <div class="section_slide">
              <?php
              if(!empty($page_content['other_sections'])){
                foreach($page_content['other_sections'] as $other_section){
                  ?>
                  <div class="single_slide <?php if($other_section['direction'] == 'ltr'){echo 'row_reverse';}?>">
                      <div class="text">
                        <div class="box_border">
                            <h3><?php echo $other_section['title'];?></h3>
                            <div class="content">
                              <?php echo $other_section['content'];?>
                            </div>
                        </div>
                      </div>
                      <div class="img">
                          <img src="<?php echo $other_section['image'];?>" alt="">
                      </div>
                  </div>
                  <?php
                }
              }
              ?>
          </div>
          <div class="section_team" style="display: none;">
              <div class="section_title">
                <img src="<?php echo $page_content['team_section']['image'];?>" alt="" width="110">
                <h3><?php echo $page_content['team_section']['title'];?></h3>
              </div>
              <div class="all_team">
              <?php if($page_content['team_section']['team']): foreach($page_content['team_section']['team'] as $team_member):?>
                <div class="single_team">
                  <a>
                    <img src="<?php echo $team_member['image']?>" alt="">
                    <div class="text">
                      <h4><?php echo $team_member['name']?></h4>
                      <p><?php echo $team_member['position']?></p>
                    </div>
                  </a>
                </div>
              <?php endforeach; endif;?>
              </div>
          </div>
      </div>
    </div>
</div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
