<?php require_once 'header.php';?>
<?php
$items  = WC()->cart->get_cart();
if(empty($items)){
  wp_redirect(home_url());
  exit;
}
// WC()->session->set('chosen_shipping_methods', serialize(array( 'filters_by_cities_shipping_method:2' )) );
// WC()->cart->calculate_shipping();
// WC()->cart->calculate_totals();
// $chosen_shippings = WC()->session->get( 'chosen_shipping_methods' );
// if ( WC()->session->__isset('shipping_cost' ) ) $bool = false;
// Mandatory to make it work with shipping methods
// foreach ( WC()->cart->get_shipping_packages() as $package_key => $package ){
//     WC()->session->set( 'shipping_for_package_' . $package_key, $bool );
//     // var_dump($package);
// }
// WC()->cart->calculate_shipping();
// echo '<pre style="direction:ltr;">';
// var_dump(WC()->cart); //
// echo '</pre>';
$countries       = mitch_get_countries()['ar'];
global $states;
$shipping_data   = mitch_get_shipping_data();
$payment_methods = mitch_get_available_payment_methods_data();
$available_gateways = WC()->payment_gateways->get_available_payment_gateways();
if(!empty(WC()->cart->applied_coupons)){
  $coupon_code    = WC()->cart->applied_coupons[0];
  $active         = 'active';
  $dis_form_style = 'display:block;';
  $dis_abtn_style = 'display:none;';
  $dis_rbtn_style = 'display:block;';
}else{
  $coupon_code    = '';
  $active         = '';
  $dis_form_style = '';
  $dis_abtn_style = 'display:block;';
  $dis_rbtn_style = 'display:none;';
}
global $states_cities;
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu-checkout.php';?>
  <!--start page-->
  <div class="page_checkout">
      <div class="grid">
        <div class="old-checkout" style="display: none;">
          <div class="section_title">
            <p>
              <span><?php echo $fixed_string['checkout_sub_title'];?></span>
              <?php echo $fixed_string['checkout_main_title'];?>
            </p>
          </div>
          <div class="checkout">
            <form id="checkout_form" class="woocommerce-checkout" method="post" enctype="multipart/form-data">
              <div class="right">
                <div id="checkout_form_alerts" class="ajax_alerts"></div>
                  <div class="all_con">
                      <div class="con_info">
                          <h4><?php echo $fixed_string['checkout_form_myinfo'];?></h4>
                          <div class="field">
                              <label for=""><?php echo $fixed_string['checkout_form_firstname'];?><span>*</span></label>
                              <input type="text" name="firstname">
                          </div>
                          <div class="field">
                              <label for=""><?php echo $fixed_string['checkout_form_family'];?><span>*</span></label>
                              <input type="text" name="lastname">
                          </div>
                          <div class="field full">
                              <label for=""><?php echo $fixed_string['checkout_form_email'];?><span>*</span></label>
                              <input type="email" name="email">
                          </div>
                          <div class="field full">
                              <label for=""><?php echo $fixed_string['checkout_form_phone'];?><span>*</span></label>
                              <input type="text" name="phone">
                          </div>
                          <div id="new_account_button" class="field checkbox_checkout">
                              <input type="checkbox" name="new_account_check" class="checkbox-box" value="yes" onchange="show_password_fields();">
                              <label><?php echo $fixed_string['checkout_form_new_account'];?></label>
                          </div>
                          <div id="password_fields">
                            <div class="field full ">
                                <label for=""><?php echo $fixed_string['checkout_form_new_pass'];?><span>*</span></label>
                                <input id="new_password" type="password" name="new_password">
                            </div>

                            <div class="field full">
                                <label for=""><?php echo $fixed_string['checkout_form_new_pass_confirm'];?><span>*</span></label>
                                <input id="confirm_password" type="password" name="confirm_password">
                            </div>
                          </div>
                      </div>
                      <div class="con_info">
                          <h4><?php echo $fixed_string['checkout_form_shipping_address'];?></h4>
                          <div class="field checkbox_checkout" style="display:none;">
                            <ul id="shipping_method" class="woocommerce-shipping-methods">
                            <?php
                            if(!empty($shipping_data['shipping_methods'])){
                              foreach($shipping_data['shipping_methods'] as $key => $shipping_method){
                                if($shipping_method['ins_id'] == 2){
                                  $checked          = 'checked';
                                  $default_shipping = $shipping_method;
                                }else{
                                  $checked = '';
                                }
                                ?>
                                <li class="<?php echo $shipping_method['id'];?>">
                                  <input type="radio" name="shipping_method[]" data-cost="<?php echo $shipping_method['cost'];?>" id="<?php echo $shipping_method['id'];?>" value="<?php echo $shipping_method['id'].':'.$shipping_method['ins_id'];?>" class="shipping_method checkbox-box" <?php echo $checked;?>/>
                                  <label for="<?php echo $shipping_method['id'];?>"><?php echo $shipping_method['title'];?><span class="extra-txt"></span></label>
                                </li>
                                <?php
                              }
                            }
                            ?>
                            </ul>
                          </div>
                          <div class="field full select_arrow">
                            <label for=""><?php echo $fixed_string['checkout_form_country'];?><span>*</span></label>
                            <select name="country" id="country">
                            <?php
                            if(!empty($countries)){
                              // print_r($countries);
                              foreach($countries as $code => $name){
                                if($code == $theme_settings['default_country_code']){
                                  $selected = 'selected';
                                }else{
                                  $selected = '';
                                }
                                echo '<option value="'.$code.'" '.$selected.'>'.$states['ar'][$code].'</option>';
                              }
                            }
                            ?>
                            </select>
                            <input type="hidden" name="country" value="<?php echo $theme_settings['default_country_code'];?>">
                          </div>
                          <div class="field full select_arrow">
                              <label for=""><?php echo $fixed_string['checkout_form_city'];?><span>*</span></label>
                              <select id="city" name="city">
                                <option value=""><?php echo $fixed_string['checkout_form_choose_city'];?></option>
                                <?php
                                if(!empty($default_shipping['cities'])){
                                  foreach($default_shipping['cities'] as $city){
                                    echo '<option value="'.$city.'">'.$fixed_string[$city].'</option>';
                                  }
                                }
                                ?>
                              </select>
                          </div>
                          <div class="field half_full">
                              <label for=""><?php echo $fixed_string['checkout_form_building'];?><span>*</span></label>
                              <input type="number" name="building">
                          </div>
                          <div class="field half_full">
                              <label for=""><?php echo $fixed_string['checkout_form_area'];?><span>*</span></label>
                              <input type="number" name="area">
                          </div>
                          <div class="field half_full">
                              <label for=""><?php echo $fixed_string['checkout_form_street'];?><span>*</span></label>
                              <input type="number" name="street">
                          </div>
                          <div class="field full">
                              <label for=""><?php echo $fixed_string['checkout_form_order_notes'];?></label>
                              <textarea class="text_area" name="notes" rows="8"></textarea>
                          </div>
                      </div>
                      <div id="payment" class="woocommerce-checkout-payment con_info method_payment">
                          <div class="payemnt-fields">
                          <h3><?php echo ($language == 'en')? 'Payment details' : 'تفاصيل الدفع' ?></h3>
                          <?php if ( WC()->cart->needs_payment() ) : ?>
                            <ul class="wc_payment_methods payment_methods methods">
                              <?php
                              if ( ! empty( $available_gateways ) ) {
                                foreach ( $available_gateways as $gateway ) {
                                  wc_get_template( 'woocommerce/checkout/payment-method.php', array( 'gateway' => $gateway ) );
                                  
                                }
                              } else {
                                echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
                              }
                              ?>
                            </ul>
                          <?php endif; ?>
                          </div>
                      </div>
                      <div class="con_info">
                          <div class="field checkbox_checkout link_term">
                              <input name="terms-accept" type="checkbox" class="checkbox-box">
                              <label><?php echo $fixed_string['checkout_form_terms'];?></label>
                              <span><a href="<?php echo home_url('terms-and-conditions');?>" target="_blank">عرض</a></span>
                          </div>
                          <input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="<?php echo $fixed_string['checkout_form_order_now'];?>" data-value="Place order" />
                      </div>
                  </div>
              </div>
              <div class="left">
                  <div class="top">
                      <h4><?php echo $fixed_string['checkout_form_order_details'];?></h4>
                      <a href="<?php echo home_url('cart');?>"><?php echo $fixed_string['checkout_form_cart_back'];?></a>
                  </div>
                  <div class="item_cart_list">
                  <?php
                  foreach($items as $item => $values){
                    $cart_product_data = mitch_get_short_product_data($values['product_id']);
                    if(!empty($values['custom_cart_data'])){
                      $class = 'custom_cart_data';
                    }else{
                      $class = '';
                    }
                    ?>
                    <div class="single_item <?php echo $class;?>">
                      <div class="sec_item">
                          <div class="img">
                              <img style="width: 100px;height: 100px;" src="<?php echo $cart_product_data['product_image'];?>" alt="<?php echo $cart_product_data['product_title'];?>">
                          </div>
                          <div class="text">
                              <h4><?php echo $cart_product_data['product_title'];?></h4>
                              <ul>
                                <?php
                                if(!empty($values['custom_cart_data'])){
                                  foreach($values['custom_cart_data']['attributes_vals'] as $attr_val){
                                    ?>
                                    <li><?php echo mitch_get_product_attribute_name($attr_val);?></li>
                                    <?php
                                  }
                                }
                                ?>
                              </ul>
                             
                              <a href=""><?php echo $fixed_string['checkout_form_number'];?>: <?php echo $values['quantity'];?></a>
                          </div>
                      </div>
                      <p class="price_cart">
                        <?php echo $values['line_subtotal'];?>
                        <?php echo $theme_settings['current_currency'];?></p>
                    </div>
                    <?php
                  }
                  ?>
                  </div>
                  <div class="coupon_checkout">
                      <h4 class="open-coupon <?php echo $active;?>"><?php echo $fixed_string['cart_promocode_title1'];?></h4>
                      <div class="discount-form" style="<?php echo $dis_form_style;?>">
                          <button class="close-coupon"><i class="material-icons">close</i></button>
                          <div class="coupon">
                            <label for="coupon_code"><?php echo $fixed_string['cart_promocode_title2'];?></label>
                            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="<?php echo $coupon_code;?>" placeholder="<?php echo $fixed_string['cart_promocode_title1'];?>" />
                            <button style="<?php echo $dis_abtn_style;?>" id="apply_coupon" type="submit" class="button btn">
                              <?php echo $fixed_string['cart_promocode_button'];?>
                            </button>
                            <button style="<?php echo $dis_rbtn_style;?>" id="remove_coupon" type="submit" class="button btn">
                              <?php echo $fixed_string['cart_promocode_remove_button'];?>
                            </button>
                            <input type="hidden" name="lang" id="lang" value="">
                          </div>
                      </div>
                  </div>
                  <div class="info_cart_box">
                      <div class="list_pay" id="car_subtotal_div">
                          <p><?php echo $fixed_string['checkout_form_subtotal'];?></p>
                          <p><span id="cart_subtotal"><?php echo WC()->cart->subtotal;?></span> <?php echo $theme_settings['current_currency'];?></p>
                      </div>
                      <?php if(!empty($coupon_code)){ ?>
                      <div class="list_pay discount">
                          <p><?php echo $fixed_string['checkout_page_discount'];?></p>
                          <p><span id="cart_discount">- <?php echo WC()->cart->coupon_discount_totals[$coupon_code];?></span> <?php echo $theme_settings['current_currency'];?></p>
                      </div>
                      <?php }?>
                      <div class="list_pay">
                        <p><?php echo $fixed_string['checkout_form_shipping_cost'];?></p>
                        <p>
                        <?php
                        //
                        // var_dump($default_shipping['cost']);
                        if(WC()->cart->get_cart_shipping_total() == 'Free!' && $default_shipping['cost'] == 0){
                          ?>
                          <span id="cart_shipping_total"><?php echo $fixed_string['cart_submit_free_shipping'];?></span>
                          <?php
                        }elseif($default_shipping['cost'] != 0){
                          ?>
                          <span id="cart_shipping_total"><?php echo $default_shipping['cost']; //WC()->cart->get_cart_shipping_total();?></span>
                          <?php
                          echo $theme_settings['current_currency'];
                        }
                        ?>
                      </div>
                      <div class="list_pay total">
                          <p><?php echo $fixed_string['checkout_form_total'];?></p>
                          <p><span id="cart_total"><?php echo WC()->cart->cart_contents_total + $default_shipping['cost'];?></span> <?php echo $theme_settings['current_currency'];?></p>
                      </div>
                  </div>
              </div>
            </form>
          </div>
        </div>
          <?php while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
      </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
