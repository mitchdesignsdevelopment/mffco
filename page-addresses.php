<?php
require_once 'header.php';
mitch_validate_logged_in();
$current_user_id = get_current_user_id();
$main_address    = mitch_get_user_main_address($current_user_id);
$other_addresses = mitch_get_user_others_addresses_list($current_user_id);
$countries       = mitch_get_countries()['ar'];
global $states;
$shipping_data   = mitch_get_shipping_data()['shipping_methods'][$theme_settings['default_shipping_method']];
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_myaccount">
    <div class="grid">
      <div class="section_title">
          <h1><?php echo $fixed_string['myaccount_page_title'];?></h1>
      </div>
      <div class="page_content">
        <?php include_once 'theme-parts/myaccount-sidebar.php';?>
        <div class="dashbord">
            <div class="section_addresses">
                <div class="address basic">
                  <h5><?php echo $fixed_string['myaccount_page_addresses_title1'];?></h5>
                  <?php
                  if(!empty($main_address)){
                    ?>
                    <div class="single_address">
                        <p class="text_addres">
                                <?php
                              
                              echo "العنوان بالتفصيل".':'.$main_address->street.' - ';
                              echo "الدور ".':'.$main_address->floor.' - ';
                              echo "رقم العقار".':'.$main_address->building.' - ';
                              echo 'رقم الشقة'.':'.$main_address->apartment_no.' - ';
                              echo "المنطقة".':'.$main_address->city.' - ';
                              echo $fixed_string['checkout_form_city'].':'.$fixed_string[$main_address->state].' - ';
                              echo $fixed_string['checkout_form_country'].':'.'مصر';

                              ?>
                        </p>
                        <div class="adddres_action">
                          <a href="#popup-edit-address"  class="edit s_action load_form_data js-popup-opener"
                            data-id="<?php echo $main_address->ID;?>"
                            data-country="<?php echo $other_address->state;?>"
                            data-city="<?php echo $other_address->city;?>"
                            data-billing_address_1="<?php echo $other_address->street;?>"
                            data-building="<?php echo $other_address->building;?>"
                            data-street="<?php echo $other_address->apartment_no;?>"
                            data-area="<?php echo $other_address->floor;?>"
                            data-main_others="0"
                            >
                              <?php echo $fixed_string['myaccount_page_addresses_edit'];?>
                            </a>
                        </div>
                           
                      </div>
                    <?php
                  }
                  else{
                    ?>
                    <div class="single_address">
                          <p class="text_addres"> ادخل عنوانك الرئيسي في صفحة اتمام الطلب</p>
                    </div>
                  <?php
                  }
                  ?>
                </div>
                <?php
                if(!empty($other_addresses)){
                  ?>
                <div class="address all">
                  <h5>
                    <?php echo $fixed_string['myaccount_page_addresses_title2'];?>
                    <a href="#popup-edit-address" class="btn btn-primary pull-left add_new_address js-popup-opener" type="button"><?php echo $fixed_string['myaccount_page_addresses_add_btn'];?></a>
                  </h5>
                  <?php
                    foreach($other_addresses as $other_address){
                      ?>
                       <div class="single_address">
                        <p class="text_addres">
                          <?php
                              echo "العنوان بالتفصيل".':'.$other_address->street.' - ';
                              echo "الدور ".':'.$other_address->floor.' - ';
                              echo "رقم العقار".':'.$other_address->building.' - ';
                              echo 'رقم الشقة'.':'.$other_address->apartment_no.' - ';
                              echo "المنطقة".':'.$other_address->city.' - ';
                              echo $fixed_string['checkout_form_city'].':'.$fixed_string[$other_address->state].' - ';
                              echo $fixed_string['checkout_form_country'].':'.'مصر';
                          ?>
                        </p>

                        <div class="adddres_action">
                          <a href="#popup-edit-address"  class="s_action edit load_form_data js-popup-opener"
                            data-id="<?php echo $other_address->ID;?>"
                            data-country="<?php echo $other_address->state;?>"
                            data-city="<?php echo $other_address->city;?>"
                            data-billing_address_1="<?php echo $other_address->street;?>"
                            data-building="<?php echo $other_address->building;?>"
                            data-street="<?php echo $other_address->apartment_no;?>"
                            data-area="<?php echo $other_address->floor;?>"
                            data-main_others="1"
                          >
                            <?php echo $fixed_string['myaccount_page_addresses_edit'];?>
                          </a>

                          <a  href="#popup-delete-address" class="s_action delete js-popup-opener "
                            data-id="<?php echo $other_address->ID;?>"
                            data-country="<?php echo $other_address->state;?>"
                            data-city="<?php echo $other_address->city;?>"
                            data-billing_address_1="<?php echo $other_address->street;?>"
                            data-building="<?php echo $other_address->building;?>"
                            data-street="<?php echo $other_address->apartment_no;?>"
                            data-area="<?php echo $other_address->floor;?>"
                            data-main_others="1"
                            id="delete-add"
                          >
                            <?php echo $fixed_string['myaccount_page_addresses_delete'];?>
                          </a>
                        </div>
                       
                      </div>

                        <a href="#popup-edit-address"  class="edit load_form_data js-popup-opener"
                        data-id="<?php echo $other_address->ID;?>"
                        data-country="<?php echo $other_address->state;?>"
                        data-city="<?php echo $other_address->city;?>"
                        data-billing_address_1="<?php echo $other_address->street;?>"
                        data-building="<?php echo $other_address->building;?>"
                        data-street="<?php echo $other_address->apartment_no;?>"
                        data-area="<?php echo $other_address->floor;?>"
                        data-main_others="1"
                        >
                          <?php echo $fixed_string['myaccount_page_addresses_edit'];?>
                        </a>
                        <a  href="#popup-delete-address" class="s_action delete js-popup-opener "
                            data-id="<?php echo $other_address->ID;?>"
                            data-country="<?php echo $other_address->state;?>"
                            data-city="<?php echo $other_address->city;?>"
                            data-billing_address_1="<?php echo $other_address->street;?>"
                            data-building="<?php echo $other_address->building;?>"
                            data-street="<?php echo $other_address->apartment_no;?>"
                            data-area="<?php echo $other_address->floor;?>"
                            data-main_others="1"
                            id="delete-add"
                          >
                            <?php echo $fixed_string['myaccount_page_addresses_delete'];?>
                          </a>
                      <?php
                    }
                  }
                  ?>
                </div>
            </div>
            <!-- <p class="note_address"><?php //echo $fixed_string['myaccount_page_addresses_note'];?></p> -->
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
  <div id="overlay" class="overlay"></div>
  <div id="popup-edit-address" class="popup edit-address"> 
      <div class="popup__window edit">
        <button type="button" class="popup__close material-icons js-popup-closer">close</button>
        <div class="form-content">
          <h3><img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'><?php echo $fixed_string['myaccount_page_addresses_edit_add'];?></h3>
          <form id="edit_address" action="#" method="post">
            <div class="field select_arrow">
              <label for=""><?php echo 'المحافظة';//$fixed_string['checkout_form_country'];?><span>*</span></label>
              <select id="country" name="country">
              <?php
                  if(!empty($countries)){
                    // print_r($countries);
                    foreach($countries as $code => $name){
                      if($code == $theme_settings['default_country_code']){
                        $selected = 'selected';
                      }else{
                        $selected = '';
                      }
                      echo '<option value="'.$code.'" '.$selected.'>'.$states['ar'][$code].'</option>';
                    }
                  }
              ?>
              </select>
            </div>
            <div class="field select_arrow">
              <label for=""><?php echo 'المنطقة';//$fixed_string['checkout_form_city'];?><span>*</span></label>
              <select id="city" name="city" required>
                <option><?php echo $fixed_string['checkout_form_choose_city'];?></option>
                <?php
                if(!empty($shipping_data['cities'])){
                  foreach($shipping_data['cities'] as $city){
                    echo '<option value="'.$city.'">'.$city.'</option>';
                  }
                }
                ?>
              </select>

            </div>
            <label for="property_type_apart" class="">نوع العقار&nbsp;<abbr class="required" title="مطلوب">*</abbr></label>
            <span class="woocommerce-input-wrapper">
              <input type="radio" class="input-radio " value="apart" name="property_type" id="property_type_apart" checked="checked" checked><label for="property_type_apart" class="radio ">شقه</label>
              <input type="radio" class="input-radio " value="villa" name="property_type" id="property_type_villa"><label for="property_type_villa" class="radio ">فيلا</label></span>
            

            <p class="address-field validate-required" id="billing_address_1_field" data-priority="50"><label for="billing_address_1" class="">العنوان بالتفصيل&nbsp;<abbr class="required" title="مطلوب">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="billing_address_1" id="billing_address_1" placeholder="" value="" autocomplete="address-line1"></span></p>
            
              <div class="require-build">    

                <div class="field half_full">
                  <label for=""><?php echo $fixed_string['checkout_form_building'];?><span>*</span></label>
                  <input id="building" type="number" name="building" required>
                </div>

                <div class="field half_full">
                  <label for=""><?php echo 'رقم الشقة';//$fixed_string['checkout_form_area'];?><span>*</span></label>
                  <input id="area" type="number" name="area" required>
                </div>

                <div class="field half_full">
                  <label for=""><?php echo 'اسم الشارع';//$fixed_string['checkout_form_street'];?><span>*</span></label>
                  <input id="street" type="text" name="street" required>
                </div>

              </div>

            <input id="address_id" type="hidden" name="address_id" value="">
            <input id="main_others" type="hidden" name="main_others" value="">
            <input id="operation" type="hidden" name="operation" value="">
            <button type="submit" value=""><?php echo $fixed_string['myaccount_page_addresses_save_add'];?></button>

          </form>
        </div>
      </div>
  </div>
  <div id="popup-delete-address" class="popup delete-address"> 
      <div class="popup__window delete">
        <!-- <button type="button" class="popup__close material-icons js-popup-closer">close</button> -->
        <div class="form-content">
          <h3>هل أنت متأكد من حذف العنوان؟</h3>
          <form id="delete_address" action="#" method="post">
                  
                  <div class="actions_pop">
                      <a href="#" class="s_action_pop yes" 
                      data-id="<?php echo $other_address->ID;?>"
                      
                      >نعم</a>
                      <a href="#" class="s_action_pop no js-popup-closer">لا</a>
                  </div>
          </form>
        </div>
      </div>
  </div>

</div>
<?php require_once 'footer.php';?>
