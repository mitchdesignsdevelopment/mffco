<?php
require_once 'header.php';
$faq_items = get_field('faq_items', get_the_id());
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content style_page_form">
    <div class="grid">
      <div class="section_title career">
        <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/mffco_icon.png" alt="" width="60">
          <p><?php echo get_field('subtitle_careers') ?></p>
        <h1><?php echo $fixed_string['career_page_title'];?></h1>
      </div>
      <div class="section_career">
      <?php
      if(!empty($faq_items)){
        foreach($faq_items as $faq_item){
        ?>
        <div class="single_career">
            <h3 class="title_career"><?php echo $faq_item['title'];?></h3>
            <div class="content career">
              <?php echo $faq_item['content'];?>
                  <div class="section_info">
                    <?php if(!empty($faq_item['content_info'])){
                      foreach($faq_item['content_info'] as $info_item){
                    ?>
                      <div class="inline">
                        <img src="<?php echo $info_item['icon'];?>" alt="">
                        <p><?php echo $info_item['title'];?></p>
                      </div>
                    <?php } } ?>
                  </div>
            </div>
        </div>
        <?php
        }
      }
      ?>
      </div>
    </div>
  </div>
  <!--end page-->
</div>

<?php require_once 'footer.php';?>
