<?php
require_once 'header.php';
$terms_items = get_field('terms_items', get_the_id());
?>
<div id="page" class="site" style="min-height: 1000px;">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content style_page_form">
        <div class="grid">
            <div class="policy_page test site-main">
                <div class="section_title">
                  <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/mffco_icon.png" alt="" width="60">
                  <h1><?php echo ($language == 'en')? get_field('page_title_en'): get_the_title(6836); ?></h1>
                </div>
                <div class="internal_container">
                    <div class="nav_menu">
                    <a  class="nav_single_title <?php echo mitch_get_active_page_class('terms-and-conditions');?>" href="<?php echo home_url('terms-and-conditions');?>"><?php echo ($language == 'en')? get_field('page_title_en'): get_the_title(6836); ?></a>

                      <?php
                        $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            'fields' => 'ids',
                            'post_parent'    => get_the_ID(),
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );
                        $pages_ids = get_posts($args);
                        if(!empty($pages_ids)){ $count=1;
                            foreach($pages_ids as $page_id){
                        ?>
                          <a class="nav_single_title <?php echo($page_id==get_the_ID())? 'active': ''; ?>" href="<?php the_permalink($page_id); echo($language == 'en')? '?lang=en' : '';?>"><?php echo ($language == 'en')? get_field('page_title_en', $page_id) : get_the_title($page_id); ?></a>
                        <?php } } ?>
                    </div>
                    <div class="left">
                        <div class="content">
                            <?php echo ($language == 'en')? get_field('page_content_en') : the_content(); ?>

                            <?php
                              if(!empty($terms_items)){
                                foreach($terms_items as $term_item){
                                  ?>
                                  <div class="min_box">
                                    <h3><?php echo $term_item['title'];?></h3>
                                    <div class="term-content">
                                      <?php echo $term_item['content'];?>
                                    </div>
                                  </div>
                                  <?php
                                }
                              }
                            ?>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
