<?php require_once 'header.php'; ?>
<?php
$items  = WC()->cart->get_cart();
if (empty($items)) {
  wp_redirect(home_url());
  exit;
}
if (!empty(WC()->cart->applied_coupons)) {
  $coupon_code    = WC()->cart->applied_coupons[0];
  $active         = 'active';
  $dis_form_style = 'display:block;';
  $dis_abtn_style = 'display:none;';
  $dis_rbtn_style = 'display:block;';
} else {
  $coupon_code    = '';
  $active         = '';
  $dis_form_style = '';
  $dis_abtn_style = 'display:block;';
  $dis_rbtn_style = 'display:none;';
}
// $new_order = wc_get_order(200);
// $new_order->calculate_totals();
// global $wpdb;
// $wpdb->query("DELETE FROM wp_woocommerce_order_itemmeta WHERE order_item_id = 39 AND (meta_key = '_line_subtotal' OR meta_key = '_line_total');");
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php'; ?>
  <!--start page-->
  <div class="page_cart">
    <div class="cart">
      <div class="grid">
        <div class="sectio_title_cart">
          <h2>
            <p class="cart_subtitle">
              <?php echo $fixed_string['cart_sub_title']; ?>
            </p>
            <?php echo $fixed_string['cart_main_title']; ?>
          </h2>
        </div>
        <div class="section_cart">
          <div class="cart_list">
            <?php
            $products_ids = array();
            $cats = array();
            if (!empty($items)) {
              $bads_count = bbloomer_check_bads_in_cart_before();
              foreach ($items as $item => $values) {
            ?>
                <?php
                $products_ids[]    = $values['product_id'];
                $cats = $values['data']->get_category_ids();

                $cart_product_data = mitch_get_short_product_data($values['product_id']);
                $product_cats = $cart_product_data['product_cat'];



                if ($product_cats) {
                  //   print_r($product_cats);
                  // wp_die();
                  foreach ($product_cats as $cat) {
                    if ($cat->parent == 0) {
                    } else {

                      if ($cat->term_id == 257 || $cat->term_id == 276 || $cat->term_id == 256) {
                        $bads_excits = true;
                        $targeted_id = $cart_product_data['product_id'];
                        $bads_data = mitch_get_short_product_data($targeted_id);
                        $prices_string = strip_tags($bads_data['product_price']);
                        $prices = explode("EGP", $prices_string);
                        $prices = str_replace('&nbsp;', '', $prices);
                        $regular_price = $prices[0];
                        $sale_price =  $prices[1];

                        if (in_array($targeted_id, array($values['product_id'], $values['variation_id']))) {
                          $quantity =  $values['quantity'];
                        }
                      }
                      $beds_quantity = bbloomer_check_category_in_cart_before();
                    }
                  }
                }
                // $price =  WC()->cart->get_product_price( $_product ); // PHPCS: XSS ok.
                // $subtotal = WC()->cart->get_product_subtotal( $product, $cart_item['quantity'] );
                ?>
                <div id="cart_page_<?php echo $item; ?>" class="single_item">
                  <div class="sec_item">
                    <div class="img">
                      <img height="100px" src="<?php echo $cart_product_data['product_image']; ?>" alt="<?php echo $cart_product_data['product_title']; ?>">
                    </div>
                    <div class="info">
                      <div class="text">
                        <a class="title_link" href="<?php echo $cart_product_data['product_url']; ?>">
                          <h4><?php echo $cart_product_data['product_title']; ?></h4>
                        </a>
                        <?php
                        if (!empty($values['custom_cart_data'])) {
                        ?>
                          <ul>
                            <?php
                            foreach ($values['custom_cart_data']['attributes_vals'] as $attr_val) {
                            ?>
                              <li><?php echo mitch_get_product_attribute_name($attr_val); ?></li>
                            <?php
                            }
                            ?>
                          </ul>
                        <?php } elseif (!empty($values['variation'])) {
                        ?>
                          <ul>
                            <?php
                            // foreach ($values['variation'] as $key => $value) {
                              foreach ($values['data']->get_attributes() as $attribute_key => $attribute_arr) {
                                $terms=get_term_by('slug', $values['data']->get_attributes()[$attribute_key], $attribute_key);
                              // }
                            ?>
                              <!-- <li><?php //echo ucfirst($value); ?></li> -->
                              <li><?php echo $terms->name ?></li>

                            <?php
                            }
                            ?>
                          </ul>
                        <?php }
                        $note = get_field('note', $cart_product_data['product_id']);
                        if ($note) : ?>
                          <span class="product-note"><?php echo $note; ?></span>
                        <?php endif; ?>
                        <a class="remove_page_cart" href="javascript:void(0);" onclick="cart_remove_item('<?php echo $item; ?>', '');"><?php echo $fixed_string['cart_delete_title']; ?></a>
                      </div>
                      <p class="test" style="display: none;"><?php
                                                              // $product_obj     = wc_get_product($values['product_id']);
                                                              // $attr=array();
                                                              // if (!empty($product_obj->get_available_variations())) {
                                                              //   foreach ($product_obj->get_available_variations() as $variation_obj) {
                                                              //     print_r($variation_obj['attributes']);
                                                              //   }}                                                            
                                                              // echo "public id : " . $cart_product_data['product_id'];
                                                              // echo "<br>";
                                                              // echo "var id :" . $values['data']->get_ID();
                                                              // echo "<br>";
                                                              // echo "price ::" . $values['data']->get_price();
                                                              // echo "<br>";
                                                              // echo " reg price ::" . $values['data']->get_regular_price();
                                                              foreach ($values['data']->get_attributes() as $attribute_key => $attribute_arr) {
                                                                $terms=get_term_by('slug', $values['data']->get_attributes()[$attribute_key], $attribute_key);
                                                              }
                                                              $term_size = get_term_by('slug', $values['data']->get_attributes()['pa_sizes'], 'pa_sizes');
                                                              $term_color=get_term_by('slug', $values['data']->get_attributes()['pa_color'], 'pa_color');
                                                              print_r($values);
                                                              // [attribute_summary] => مقاسات: 120*200
                                                              echo "<br>";
                                                              echo  $values['variation_id'];
                                                              echo "<br>";
                                                              echo "pa_size : ".$term_size->name;
                                                              echo "<br>";
                                                              
                                                              echo "pa_color : ". $term_color->name;
                                                              echo "<br>";
                                                              
                                                              echo "terms : ". $terms->name;


                                                              // echo get_post_field('post_name', $values['variation_id']);
                                                              echo "<br>";

                                                              echo mitch_get_product_attribute_name(get_post_field('post_name', $values['id']));

                                                              echo "<br>";

                                                              // var_dump($values['data']->get_attributes()['pa_sizes']);
                                                              // var_dump($attr);

                                                              // echo mb_convert_encoding($values['data']->get_attributes()['pa_sizes'], "utf-8");

                                                              ?></p>
                      <p>
                        <?php
                        // if(!empty($values['custom_cart_data'])){
                        //echo $values['line_subtotal'] / $values['quantity'];
                        //}else{
                        // echo (isset($values['variation_id']) && $cart_product_data['product_type'] != 'simple')?number_format(get_post_meta($values['variation_id'],'_price',true),0):number_format(get_post_meta($cart_product_data['product_id'],'_price',true),0);
                        //}
                        // echo ($quantity > 1 ||  $values['quantity'] > 1) ? $regular_price : number_format($values['data']->get_price())
                        // echo (has_term(256, 'product_cat', $values['product_id']) || has_term(257, 'product_cat', $values['product_id']) || has_term(276, 'product_cat', $values['product_id']) && $quantity > 1) ? $regular_price : number_format($values['data']->get_price())
                        // echo ($values['product_id']==7273 && $quantity > 1  || $values['product_id']==7002 && $quantity > 1 || $values['product_id']==7002&&$beds_id || $values['product_id']==7273&&$beds_id) ? $regular_price : number_format($values['data']->get_price());
                        // if ($values['product_id'] == 7273 && $quantity > 1  || $values['product_id'] == 7002 && $quantity > 1) {
                        // if ($values['product_id'] == $targeted_id && $quantity > 1) {

                        //   // echo $regular_price;
                        //   echo $values['data']->get_regular_price();
                        //   // } elseif ($values['product_id'] == 7002 && $beds_quantity['found'] == 1 || $values['product_id'] == 7273 && $beds_quantity['found'] == 1 || $bads_count > 1 && $beds_quantity['found'] == 1 ) {
                        // } else
                        if (
                          $bads_count == 1 && $beds_quantity['found'] == 1 && $values['product_id'] == $targeted_id ||
                          $bads_count > 1 && $beds_quantity['found'] == 1 && $values['product_id'] == $targeted_id ||
                          $bads_count > 1 && $values['product_id'] == $targeted_id && $values['product_id'] == $targeted_id ||
                          $values['product_id'] == $targeted_id && $quantity > 1 && $values['product_id'] == $targeted_id
                        ) {

                          // echo number_format($values['line_subtotal'], 0);
                          // echo number_format($cart_product_data['product_price_b4_sale'], 0);
                          echo number_format($values['data']->get_regular_price(), 0);
                        } else {
                          echo number_format($values['data']->get_price());
                        }
                        // echo number_format($values['data']->get_price());



                        ?>
                        <?php echo $theme_settings['current_currency']; ?>
                      </p>
                    </div>
                  </div>
                  <?php
                  $classname = '';
                  if ($values['quantity'] == 1) {
                    $classname = 'disabled';
                  }
                  ?>
                  <div class="section_count">
                    <button class="increase" id="increase" onclick="increaseValueByID('number_<?php echo $item; ?>');update_cart_items('<?php echo $item; ?>', 'cart_page');" value="Increase Value"></button>
                    <input class="number_count" type="number" id="number_<?php echo $item; ?>" value="<?php echo $values['quantity']; ?>" />
                    <button class="decrease <?php echo $classname; ?>" id="decrease" onclick="decreaseValueByID('number_<?php echo $item; ?>');update_cart_items('<?php echo $item; ?>', 'cart_page');" value="Decrease Value"></button>
                  </div>

                  <p class="total_price"><span id="line_subtotal_<?php echo $item; ?>"><?php echo $values['data']->get_price() ?
                                                                                          // $values['data']->get_price()
                                                                                          number_format($values['line_subtotal'], 0)
                                                                                          : number_format($values['line_subtotal'], 0); ?></span> <?php echo $theme_settings['current_currency']; ?></p>
                </div>
            <?php
              }
            }
            ?>
          </div>
          <p class="total_price"><span id="cart_total"><?php echo number_format(WC()->cart->cart_contents_total, 0); ?></span> <?php echo $theme_settings['current_currency']; ?></p>
          <div class="cart_action">
            <div class="right">
              <a href="<?php echo home_url('checkout'); ?>">
                <button type="button"><?php echo $fixed_string['cart_submit_button']; ?></button>
              </a>
              <!-- <p>تاريخ الشحن المتوقع داخل قطر : 24 أكتوبر 1443 - 24 ربيع الأول 1443</p> -->
            </div>
            <div class="left">
              <div class="coupon_cart">
                <h4 class="open-coupon <?php echo $active; ?>"><?php echo $fixed_string['cart_promocode_title1']; ?></h4>
                <div class="discount-form" style="<?php echo $dis_form_style; ?>">
                  <button class="close-coupon"><i class="material-icons">close</i></button>
                  <div class="coupon">
                    <label for="coupon_code"><?php echo $fixed_string['cart_promocode_title2']; ?></label>
                    <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="<?php echo $coupon_code; ?>" placeholder="<?php echo $fixed_string['cart_promocode_title1']; ?>" />
                    <button style="<?php echo $dis_abtn_style; ?>" id="apply_coupon" type="submit" class="button btn">
                      <?php echo $fixed_string['cart_promocode_button']; ?>
                    </button>
                    <button style="<?php echo $dis_rbtn_style; ?>" id="remove_coupon" type="submit" class="button btn">
                      <?php echo $fixed_string['cart_promocode_remove_button']; ?>
                    </button>
                    <input type="hidden" name="lang" id="lang" value="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    shuffle($products_ids);
    $product_id        = $products_ids[0];
    // mitch_test_vars(array($product_id));
    $new_related_title = $fixed_string['cart_related_products_title'];
    include_once 'theme-parts/related-products.php';
    ?>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php'; ?>