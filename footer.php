  <?php require_once 'theme-parts/footer-banners.php';?>

  <!--start footer-->
  <footer>
      <div class="section_footer">
          <div class="grid">
              <div class="top">
                  <div class="section_footer_col_one">
                      <div class="section one">
                          <div class="logo">
                              <a href="<?php echo $theme_settings['site_url'];?>">
                                  <img src="<?php echo $theme_settings['theme_logo'];?>" alt="">
                              </a>
                          </div>
                      </div>
                      <div class="section two">
                          <div class="menu">
                              <?php
                              $footer_items = get_field('footer_builder_'.$theme_settings['current_lang'], 'options');
                              if(!empty($footer_items)){
                              foreach($footer_items as $footer_item){
                            ?>
                              <ul class="single_menu">
                                  <li><?php echo $footer_item['title'];?></li>
                                  <?php
                              if(!empty($footer_item['items'])){
                                foreach($footer_item['items'] as $sub_item){
                                  if($sub_item['item_type'] == 'page'){
                                    ?>
                                  <li><a
                                          href="<?php echo get_the_permalink($sub_item['item_page']->ID);?>"><?php echo $sub_item['item_page']->post_title;?></a>
                                  </li>
                                  <?php
                                  }elseif($sub_item['item_type'] == 'category'){
                                    ?>
                                  <li><a
                                          href="<?php echo get_term_link($sub_item['item_category']->term_id, 'product_cat');?>"><?php echo $sub_item['item_category']->name;?></a>
                                  </li>
                                  <?php
                                  }
                                }
                              }
                              ?>
                              </ul>
                              <?php } } ?>
                          </div>
                      </div>

                      <div class="company_social">
                          <form class="js-cm-form form" id="subForm"
                              action="https://www.createsend.com/t/subscribeerror?description=" method="post"
                              data-id="5B5E7037DA78A748374AD499497E309E0E289E411849C93C73D2F0B8B6BD129685D92E07D6C07CC3408720DF55B94BFA9162C99CC91D4E9F7ABA83CF3F5C417C">
                              <div size="base" class="sc-jzJRlG bMslyb">
                                  <div size="small" class="sc-jzJRlG liOVdz">
                                      <div>
                                          <h5>سجل في نشرتنا الإخبارية</h5>
                                          <input autocomplete="Email" aria-label="Email"
                                              class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200"
                                              name="cm-bdhlidd-bdhlidd" required="" type="email">
                                      </div>
                                  </div>
                                  <div size="base" class="sc-jzJRlG bMslyb"></div>
                              </div>
                              <button type="submit"
                                  class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo 'أشترك'; ?></button>
                          </form>
                          <script type="text/javascript"
                              src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
                          <!-- <form class="js-cm-form form" id="subForm" class="js-cm-form"
                                action="https://www.createsend.com/t/subscribeerror?description=" method="post"
                                data-id="5B5E7037DA78A748374AD499497E309E0506B8AEAD93A49F2D86569C1FCB400D66E6553818DB50098D34B7CCF7CFFE817CA73EC461C65BD68EDAB7E8D4944F22">
                                <div size="base" class="sc-jzJRlG bMslyb">
                                    <div size="small" class="sc-jzJRlG liOVdz">
                                        <div>
                                        <h5>سجل في نشرتنا الإخبارية</h5>
                                        <input autocomplete="Email" aria-label="Email" class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200" name="cm-biytiij-biytiij" required="" type="email">
                                        </div>
                                    </div>
                                    <div size="base" class="sc-jzJRlG bMslyb"></div>
                                </div>
                                <button type="submit"
                                    class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo 'أشترك'; ?></button>
                            </form> -->
                          <ul class="sm">
                              <li><a href="<?php echo get_field('youtube_link','option');?>" target="_blank"><img
                                          src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/youtube.png"
                                          alt="youtube icon" width="30" height="30" /></a></li>
                              <li><a href="<?php echo get_field('instagram_link','option');?>" target="_blank"><img
                                          src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/instagram.png"
                                          alt="instagram icon" width="30" height="30" /></a></li>
                              <li><a href="<?php echo get_field('facebook_link','option');?>" target="_blank"><img
                                          src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/facebook.png"
                                          alt="facebook icon" width="30" height="30" /></a></li>
                              <li><a href="<?php echo get_field('tiktok_link','option');?>" target="_blank"><img
                                          src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/tiktok.png"
                                          alt="tiktok icon" width="30" height="30" /></a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="section_footer_col_two">
                      <div class="section_one">
                          <h4>
                              Instagram
                              <p>mffcohelwan@</p>
                          </h4>
                          <div class="section_insta">
                              <div class="insta_gallery">
                                  <?php echo do_shortcode( "[instagram-feed feed=1]");?>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>

              <div class="bottom">
                  <p class="copy_right">Copyrights © <?php echo date('Y', strtotime("-1 year", time())); ?> -
                      <?php echo date('Y'); ?> Mffco.com</p>
                  <a href="https://www.mitchdesigns.com" target="_blank" class="powerd">Online Store Powered By
                      MitchDesigns</a>
              </div>
          </div>
      </div>
      <div class="section_footer_mobile">
          <div class="grid">
              <div class="top">
                  <div class="logo">
                      <a href="<?php echo $theme_settings['site_url'];?>">
                          <img src="<?php echo $theme_settings['theme_logo'];?>" alt="">
                      </a>
                  </div>
                  <ul class="sm">
                      <li><a href="" target="_blank"><img
                                  src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/youtube.png"
                                  alt="youtube icon" width="30" height="30" /></a></li>
                      <li><a href="" target="_blank"><img
                                  src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/instagram.png"
                                  alt="instagram icon" width="30" height="30" /></a></li>
                      <li><a href="" target="_blank"><img
                                  src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/facebook.png"
                                  alt="facebook icon" width="30" height="30" /></a></li>
                  </ul>

              </div>
              <div class="bottom">
                  <div class="company_social">
                      <form class="js-cm-form form" id="subForm"
                          action="https://www.createsend.com/t/subscribeerror?description=" method="post"
                          data-id="5B5E7037DA78A748374AD499497E309E0E289E411849C93C73D2F0B8B6BD129685D92E07D6C07CC3408720DF55B94BFA9162C99CC91D4E9F7ABA83CF3F5C417C">
                          <div size="base" class="sc-jzJRlG bMslyb">
                              <div size="small" class="sc-jzJRlG liOVdz">
                                  <div>
                                      <h5>سجل في نشرتنا الإخبارية</h5>
                                      <input autocomplete="Email" aria-label="Email"
                                          class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200"
                                          name="cm-bdhlidd-bdhlidd" required="" type="email">
                                  </div>
                              </div>
                              <div size="base" class="sc-jzJRlG bMslyb"></div>
                          </div>
                          <button type="submit"
                              class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo 'أشترك'; ?></button>
                      </form>
                      <script type="text/javascript"
                          src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
                      <!-- <form class="js-cm-form form" id="subForm" class="js-cm-form"
                                action="https://www.createsend.com/t/subscribeerror?description=" method="post"
                                data-id="5B5E7037DA78A748374AD499497E309E0506B8AEAD93A49F2D86569C1FCB400D66E6553818DB50098D34B7CCF7CFFE817CA73EC461C65BD68EDAB7E8D4944F22">
                                <div size="base" class="sc-jzJRlG bMslyb">
                                    <div size="small" class="sc-jzJRlG liOVdz">
                                        <div>
                                        <h5>سجل في نشرتنا الإخبارية</h5>
                                        <input autocomplete="Email" aria-label="Email" class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200" name="cm-biytiij-biytiij" required="" type="email">
                                        </div>
                                    </div>
                                    <div size="base" class="sc-jzJRlG bMslyb"></div>
                                </div>
                                <button type="submit"
                                    class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo 'أشترك'; ?></button>
                      </form> -->
                  </div>
                  <div class="bottom_text">
                      <p class="copy_right">Copyrights © <?php echo date('Y', strtotime("-1 year", time())); ?> -
                          <?php echo date('Y'); ?> Mffco.com</p>
                      <a href="https://www.mitchdesigns.com" target="_blank" class="powerd">Online Store Powered By
                          MitchDesigns</a>
                  </div>
              </div>

          </div>
      </div>
  </footer>
  <!--end footer-->




  <div id="overlay" class="overlay"></div>
  <?php if(!is_user_logged_in()){ ?>
  <div id="popup-login" class="popup login">
      <div class="popup__window login">
          <button type="button" class="popup__close material-icons js-popup-closer">close</button>
          <div class="form-content">
              <h3><img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt=""
                      width='50'><?php echo $fixed_string['login_popup_title'];?></h3>
              <!-- <p><?php// echo $fixed_string['naseej_welcome_message'];?></p> -->
              <div id="login_form_alerts" class="ajax_alerts"></div>
              <form id="login_form" method="post" action="#">
                  <div class="field">
                      <label for=""><?php echo $fixed_string['checkout_form_email'];?><span>*</span></label>
                      <input id="login_email" type="text" name="user_email">
                  </div>
                  <div class="field">
                      <label for=""><?php echo $fixed_string['login_popup_password'];?><span>*</span></label>
                      <input id="login_password" type="password" name="user_password">
                  </div>
                  <button type="submit" value=""><?php echo $fixed_string['login_popup_title'];?></button>
              </form>
              <a href="#popup-signup"
                  class="signup link_popup js-popup-opener"><?php echo $fixed_string['login_popup_register_title'];?></a>
          </div>
      </div>
  </div>
  <div id="popup-signup" class="popup signup">
      <div class="popup__window signup">
          <button type="button" class="popup__close material-icons js-popup-closer">close</button>
          <div class="form-content">
              <h3><img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt=""
                      width='50'><?php echo $fixed_string['login_popup_register_title'];?></h3>
              <!-- <p><?php //echo $fixed_string['naseej_welcome_message'];?></p> -->
              <div id="register_form_alerts" class="ajax_alerts"></div>
              <form id="register_form" action="#" method="post">
                  <div class="field half_full">
                      <label for=""><?php echo $fixed_string['checkout_form_firstname'];?><span>*</span></label>
                      <input type="text" name="first_name" required>
                  </div>
                  <div class="field half_full">
                      <label for=""><?php echo $fixed_string['checkout_form_family'];?><span>*</span></label>
                      <input type="text" name="last_name" required>
                  </div>
                  <div class="field full">
                      <label for=""><?php echo $fixed_string['checkout_form_email'];?><span>*</span></label>
                      <input type="email" name="user_email" required>
                  </div>
                  <div class="field full">
                      <label for=""><?php echo $fixed_string['checkout_form_phone'];?><span>*</span></label>
                      <input type="text" name="phone_number" required>
                  </div>
                  <div class="field full">
                      <label><?php echo $fixed_string['login_popup_register_password'];?></label>
                      <input type="password" name="user_password" required>
                  </div>
                  <div class="field full">
                      <label
                          for=""><?php echo $fixed_string['login_popup_register_conf_password'];?><span>*</span></label>
                      <input type="password" name="confirm_password" required>
                  </div>
                  <button type="submit" value=""><?php echo $fixed_string['login_popup_register_create_acc'];?></button>
              </form>
              <a href="#popup-login"
                  class="login link_popup js-popup-opener"><?php echo $fixed_string['login_popup_button'];?></a>
          </div>
      </div>
  </div>
  <?php }?>

  <div id="popup-remove-order" class="popup remove_order">
      <div class="popup__window remove_order">
          <div class="form-content">
              <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/img_cancel_order.jpg" alt="" />
              <div class="box_text">
                  <h3><?php echo $fixed_string['myaccount_page_orders_cancel_title'];?></h3>
                  <!--<p><?php //echo $fixed_string['myaccount_page_orders_cancel_desc'];?></p>-->
                  <!--<a id="cancel_order_track_button" class="link_order_track" href=""><?php //echo $fixed_string['myaccount_page_orders_cancel_track'];?></a>-->
                  <div class="form_action">
                      <a class="close_form popup__close js-popup-closer" href="">
                          <?php echo $fixed_string['myaccount_page_orders_cancel_enjoy'];?>
                      </a>
                      <span><?php echo $fixed_string['myaccount_page_orders_cancel_or'];?></span>
                      <a id="cancel_order_proceed_button" class="continue_form"
                          href=""><?php echo $fixed_string['myaccount_page_orders_cancel_proc'];?></a>
                  </div>
              </div>

          </div>
      </div>
  </div>

  <div id="popup-min-cart" data-contents="<?php echo $woocommerce->cart->cart_contents_count;?>" class="popup min_cart">
      <div class="popup__window min_cart">
          <button type="button" class="popup__close material-icons js-popup-closer">close</button>
          <div id="side_mini_cart_content" class="form-content">
              <?php echo mitch_get_cart_content(); //@ includes/cart-functions.php ?>
          </div>
      </div>
  </div>
  <!-- <div id="popup-full-video" class="popup full_video">
        <div class="popup__window full_video">
          <button type="button" class="popup__close material-icons js-popup-closer">close</button>
          <div class="form-content">
            <div class="youtube-video">
              <iframe id="video" src="https://www.youtube.com/embed/awJ5xFVNTf8?&rel=0&fs=0&controls=1&disablekb=1" width="100%" height="650" frameborder="0"></iframe>
            </div>
          </div>
        </div>
    </div> -->
  <div id="popup-repeat-order" class="popup repeat_order">
      <div class="popup__window repeat_order">
          <button type="button" class="popup__close material-icons js-popup-closer">close</button>
          <div class="form-content">
              <span class="material-icons">error_outline</span>
              <h4>يوجد أصناف أخرى تمت إضافتها في عربة التسوق بالفعل.</h4>
              <span class="min_border"></span>
              <form action="" method="post">
                  <div class='option'>
                      <input type="radio" id="test" name="repeat_action" value="with_items" checked>
                      <label for="test">أطلب هذا الطلب مع الأصناف الحالية</label>
                  </div>
                  <div class='option'>
                      <input type="radio" id="test2" name="repeat_action" value="no_items">
                      <label for="test2">أطلب هذا الطلب فقط
                          <p>(سيتم حذف محتويات عربة التسوق الحالية)</p>
                      </label>
                  </div>
                  <input type="hidden" name="action" value="repeat_order">
                  <input type="hidden" name="order_id"
                      value="<?php if(isset($order_obj)){echo $order_obj->get_id();}?>">
                  <button type="submit">تأكيد</button>
              </form>
          </div>
      </div>
  </div>
  <div id="popup-banner" class="popup banner">
      <div class="popup__window banner">
          <button type="button" class="popup__close material-icons js-popup-closer">close</button>
          <img src="<?php echo $theme_settings['theme_url']; ?>/assets/img/pop-up-ramadan.png" alt="popup alt" />
          <a href="https://www.mffco.com/sale/"></a>

      </div>
  </div>
  <script src="<?php echo $theme_settings['theme_url'];?>/assets/js/jquery-3.2.1.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="<?php echo $theme_settings['theme_url'];?>/assets/js/main.js"></script>
  <script src="<?php echo $theme_settings['theme_url'];?>/assets/js/aos.js"></script>
  <!-- <script src="<?php //echo $theme_settings['theme_url'];?>/assets/js/jquery-ui.js"></script> -->
  <?php if(is_singular('product')):?>
  <script src="<?php echo $theme_settings['theme_url'];?>/assets/js/jquery.elevatezoom.js"></script>
  <script>
$(".zoom").elevateZoom({
    zoomType: 'inner',
    cursor: 'crosshair',
    // zoomType              : "lens",
    // lensShape : "round",
    lensSize: 200
});
  </script>
  <?php endif;?>
  <script>
AOS.init();
if ($('body').hasClass('rtl')) {
    var is_ar = false;
} else {
    var is_ar = true;
}
  </script>
  <?php if(is_product_category()):?>
  <script>
jQuery(function($) {
    $(window).bind('load', function() {
        get_products_ajax_count();
    });
});
  </script>
  <?php endif;?>
  <script src="<?php echo $theme_settings['theme_url'];?>/assets/js/slick.min.js" defer></script>
  <script>
var mitch_ajax_url = '<?php echo admin_url('admin-ajax.php');?>';
  </script>
  <script src="<?php echo $theme_settings['theme_url'];?>/backend_functions.js"></script>
  <script src="<?php echo $theme_settings['theme_url'];?>/local/products_json.js"></script>

  <script type="text/javascript">
$(document).ready(function() {
    // $.getJSON( "http://localhost/mitchdesigns/naseej-project/naseej/wp-content/themes/naseej/local/products.json", function( data ) {
    //   var data = [];
    //   $.each( data, function( key, val ) {
    //     //items.push( "<li id='" + key + "'>" + val + "</li>" );
    //     console.log(key); //+' '+val
    //   });
    // });
    $('#gsearch').keyup(function() {
        var searchField = $(this).val();
        if (searchField === '') {
            $('#search_results').html('');
            $('#search_results').hide('slow');
            return;
        }
        $('#search_results').show('slow');
        var regex = new RegExp(searchField, "i");
        var output = '<div class="row">';
        var count = 1;

        $.each(data, function(key, val) {
            if ((val.sku.search(regex) != -1) || (val.product_name.search(regex) != -1)) {
                output += '<a href="' + val.product_url + '"><div class="col-md-6 well">';
                output +=
                    '<div class="col-md-3"><img class="img-responsive" width="80px" src="' + val
                    .product_image + '" alt="' + val.product_name + '" /></div>';
                output += '<div class="col-md-7">';
                output += '<h5>' + val.product_name + '</h5><h6>' + val.product_price + '</h6>';
                output += '</div>';
                output += '</div></a>';
                if (count % 2 == 0) {
                    output += '</div><div class="row">'
                }
                count++;
            }
        });
        if (count == 1) {
            output += '<div class="not_results">عفوا لا يوجد نتيجة!</div>';
        }
        output += '</div>';
        $('#search_results').html(output);
    });
});
var divToHide = document.getElementById('search_results');
if (divToHide) {
    document.onclick = function(e) {
        if (e.target.id !== 'search_results') {
            //element clicked wasn't the div; hide the div
            divToHide.style.display = 'none';
        }
    };
}
  </script>
  </body>

  </html>
  <?php if(is_checkout()):?>
  <script>
jQuery(function($) {

    jQuery(document).on('submit', '.checkout_coupon', function(e) {
        // Get the coupon code
        var code = jQuery('#coupon_code').val();
        var data = {
            coupon_code: code,
            security: '<?php echo wp_create_nonce("apply-coupon") ?>'
        };
        $.ajax({
            method: 'post',
            url: '/?wc-ajax=apply_coupon',
            data: data,
            success: function(data) {
                jQuery('.woocommerce-notices-wrapper').html(data);
                jQuery(document.body).trigger('update_checkout');
            }
        });
        e.preventDefault(); // prevent page from redirecting
    });
});
  </script>
  <?php wp_footer();?>
  <?php endif;?>
  <?php //wp_footer();?>