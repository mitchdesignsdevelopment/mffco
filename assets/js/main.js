$(document).ready(function () {
  $(window).on("load", function () {
    $('a[href="#popup-banner"]').click();
  });

  $(window)
    .on("resize", function () {
      var HeaderFooter = $("header").innerHeight() + $("footer").innerHeight();
      $(".site-content").css({
        "min-height": $(window).outerHeight() - HeaderFooter,
      });
      // console.log('HeaderFooter',HeaderFooter);
    })
    .resize();

  $(".sec_info .middle .section_fabrics .fabrics .all").each(function () {
    if ($(this).width() > 500) {
      $(this).addClass("scroll_fabrics");
    }
  });

  //start slick single_item //
  $(".product-slider").slick({
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    // asNavFor: ".slider-nav",
    accessibility: false,
    nextArrow: '<div class="next-arrow"><span></span></div>',
    prevArrow: '<div class="prev-arrow"><span></span></div>',
    // responsive: [
    // {
    //     breakpoint: 999,
    //     settings: {
    //     rtl:true,
    //     autoplay: false,
    //     dots: true,
    //     dotsClass: 'custom_paging',
    //     customPaging: function (slider, i) {
    //         return  (i + 1) + '/' + slider.slideCount;
    //     }
    //     }
    // },
    // ],
  });

  $(".slider-nav").slick({
    slidesToShow: 5.5,
    slidesToScroll: 1,
    asNavFor: ".product-slider",
    dots: false,
    arrows: true,
    centerMode: false,
    vertical: true,
    verticalSwiping: true,
    focusOnSelect: false,
    infinite: false,
    autoplay: false,
    responsive: [
      {
        breakpoint: 999,
        settings: {
          arrows: false,
          vertical: false,
        },
      },
    ],
  });

  $(".slider-nav .slick-slide").on("click", function (event) {
    $(".slider-nav .slick-slide").removeClass("active");
    $(".product-slider").slick("slickGoTo", $(this).data("slickIndex"));
    $(this).addClass("active");
  });
  $(".product-slider").on(
    "beforeChange",
    function (event, slick, currentSlide, nextSlide) {
      $(".slick-current img").addClass("active");
    }
  );
  $(window).on("load", function () {
    $(".product-slider").addClass("active");
    $(".slider-nav img:nth-child(1)").addClass("active");
    $(".products-slider").addClass("active");
    $(".product_widget .image").addClass("active");
  });

  $(".hero_slider").slick({
    rtl: true,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    infinite: true,
    speed: 800,
    fade: true,
    nextArrow: '<div class="next-arrow"><span></span></div>',
    prevArrow: '<div class="prev-arrow"><span></span></div>',
  });

  // $(".section_fabrics .fabrics_slick").slick({
  //     rtl: true,
  //     autoplay: false,
  //     slidesToShow: 9,
  //     slidesToScroll: 1,
  //     arrows: true,
  //     infinite: false,
  //     nextArrow: '<div class="next arrow"><span></span></div>',
  //     prevArrow: '<div class="prev arrow"><span></span></div>',
  //     responsive: [
  //         {
  //             breakpoint: 767,
  //             settings: {
  //                 slidesToShow: 6,
  //                 slidesToScroll: 4,
  //             }
  //         },
  //     ],
  // });

  $(".section_fabrics .colore_slick").slick({
    rtl: true,
    autoplay: false,
    slidesToShow: 9,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    nextArrow: '<div class="next arrow"><span></span></div>',
    prevArrow: '<div class="prev arrow"><span></span></div>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 4,
        },
      },
    ],
  });
  //End slick single_item //

  //start dropdown_info //
  $(document).on("click", ".single_info .title_info", function () {
    $(".single_info").removeClass("active");
    $(this).parent().addClass("active");
  });
  $(document).on("click", ".single_info.active .title_info", function () {
    $(".single_info").removeClass("active");
  });
  //End dropdown_info //

  //start customize step select //
  // $(document).on('click', '.single_box', function(){
  //     var step_no = $(this).data('variation-step');
  //     var next    = $(this).data('next');
  //     $('.'+step_no+' .single_box').removeClass('active');
  //     $(this).addClass('active');
  //     $('.step-nav-'+step_no).addClass('active');
  //     $('.step-nav-'+next).removeClass('disabled');
  //     $('.step.'+step_no+' button').removeClass('disabled');
  //     if(step_no == 'five'){
  //       $('#customized_product_add_to_cart').show();
  //     }
  // });
  //End customize step select //

  //start select color //
  $(document).on("click", ".colores .single_color", function () {
    $(".single_color").removeClass("active");
    $(this).addClass("active");
  });
  //End  select color //

  //start select size //
  $(document).on("click", ".section_info .info .single_info", function () {
    $(".single_info").removeClass("active");
    $(this).addClass("active");
  });
  //End  select size //

  //start play video //
  jQuery(document).on("click", ".js-videoPlayer", function (e) {
    //отменяем стандартное действие button
    e.preventDefault();
    var poster = $(this);
    // ищем родителя ближайшего по классу
    var wrapper = poster.closest(".js-videoWrapper");
    videoPlay(wrapper);
  });
  function videoPlay(wrapper) {
    var iframe = wrapper.find(".js-videoIframe");
    // Берем ссылку видео из data
    var src = iframe.data("src");
    // скрываем постер
    wrapper.addClass("active");
    // подставляем в src параметр из data
    iframe.attr("src", src);
  }
  //End play video //

  //start open coupon //
  $(document).on("click", ".open-coupon", function () {
    $(".discount-form").slideToggle("active");
    $(this).addClass("active");
    return false;
  });

  $(document).on("click", ".close-coupon", function () {
    $(".open-coupon").click();
    $(".open-coupon").removeClass("active");
    return false;
  });

  $(document).on("click", ".checkout-content .open-coupon", function () {
    $(".checkout_coupon").addClass("active");
    $(this).addClass("active");
    return false;
  });

  $(document).on("click", ".checkout-content .close-coupon", function () {
    $(".checkout_coupon").removeClass("active");
    $(this).removeClass("active");
    return false;
  });

  //End open coupon //

  //start popup //
  $(document).on("click", ".js-popup-opener", function () {
    var ths = $(this);
    var trgt = $($(this).attr("href"));
    // $(".mobile-nav").removeClass("active");
    // $('.menu_mobile_icon.close').removeClass('show');
    // $('.menu_mobile_icon.open').removeClass('hide');
    $(".popup").removeClass("popup_visible");
    $(".menu_mobile_icon.close").click();
    $("html, body").css("overflow", "hidden");
    $("#overlay").addClass("overlay_visible");
    trgt.addClass("popup_visible");

    return false;
  });
  $(document).on("click", ".js-popup-closer", function () {
    $(".popup").removeClass("popup_visible");
    $("#overlay").removeClass("overlay_visible");
    $("html, body").css("overflow", "visible");
    return false;
  });

  $(".overlay").on("click", function () {
    $(".popup").removeClass("popup_visible");
    $("#overlay").removeClass("overlay_visible");
    $("html, body").css("overflow", "visible");
  });

  $(".open_login_mobile").on("click", function () {
    $(".mobile-nav").removeClass("active");
    $(".menu_mobile_icon.close").removeClass("show");
    $(".menu_mobile_icon.open").removeClass("hide");
    $("body").removeClass("no-scroll");
  });

  //End popup //

  // start menu hover //
  var hoverTimer;
  $(document).on("mouseenter", ".single_menu.has-mega", function () {
    var hotspot = $(this);
    hoverTimer = setTimeout(function () {
      $(".mega-menu").removeClass("active");
      // hotspot.find(".mega-menu").addClass('active');
      hotspot.find(".mega-menu").slideDown();
    }, 100);
  });
  $(document).on("mouseleave", ".single_menu.has-mega", function () {
    clearTimeout(hoverTimer);
    // $('.mega-menu').removeClass('active');
    $(".mega-menu").slideUp();
  });
  // End menu hover //

  // start imgae hotspot hover //
  $(document).on("click", ".single_nav_img", function () {
    var ths = $(this);
    var trgt = $($(this).attr("href"));
    $(".image_hotspot").removeClass("active");
    $(".single_nav_img").removeClass("active");
    $(this).addClass("active");
    trgt.addClass("active");

    return false;
  });

  var hoverTimer;
  $(document).on("mouseenter", ".single_point", function () {
    var hotspot = $(this);
    hoverTimer = setTimeout(function () {
      $(".product_widget").removeClass("active");
      hotspot.find(".product_widget").addClass("active");
    }, 300);
  });
  $(document).on("mouseleave", ".single_point", function () {
    clearTimeout(hoverTimer);
    $(".product_widget").removeClass("active");
  });
  // End imgae hotspot hover //

  // start myaccount hover //
  var hoverTimer;
  $(".my_account_list").slideUp();
  $(document).on("mouseenter", ".side_left .my_account", function () {
    hoverTimer = setTimeout(function () {
      $(".my_account_list").slideDown();
      $(".title_myaccount").addClass("active");
    }, 500);
  });

  $(document).on("mouseleave", ".side_left .my_account", function () {
    clearTimeout(hoverTimer);
    $(".my_account_list").slideUp();
    $(".title_myaccount").removeClass("active");
  });
  // End myaccount hover //

  //start dropdown faq //
  $(document).on(
    "click",
    ".single_career:not(.active) .title_career",
    function () {
      $(".single_career").removeClass("active");
      $(".single_career .career").slideUp(500);
      $(this).next().slideDown(500);
      $(this).parent().addClass("active");
    }
  );
  $(document).on("click", ".single_career.active .title_career", function () {
    $(this).next().slideUp(500);
    $(this).parent().removeClass("active");
  });
  //End dropdown faq //

  $(document).on("click", ".product_details_box .single_title", function () {
    var ths = $(this);
    var trgt = $($(this).attr("href"));
    $(".info_box").removeClass("active");
    $(".product_details_box .single_title").removeClass("active");
    $(this).addClass("active");
    trgt.addClass("active");

    return false;
  });

  // start Mobile Nav ///
  $(document).on("click", ".menu_mobile_icon.open", function () {
    $(this).addClass("hide");
    $(".menu_mobile_icon.close").addClass("show");
    $(".mobile-nav").addClass("active");
    $("body").addClass("no-scroll");
    return false;
  });
  $(document).on("click", ".menu_mobile_icon.close", function () {
    $(this).removeClass("show");
    $(".menu_mobile_icon.open").removeClass("hide");
    $(".mobile-nav").removeClass("active");
    $("body").removeClass("no-scroll");
    return false;
  });

  $(document).on(
    "click",
    ".mobile-menu .menu-item-has-children:not(.active) .menu-trigger",
    function () {
      $(this).parent().addClass("active").siblings().removeClass("active");
      $(this).parent().find(".list_subcategory_mobile").slideDown();
      $(this).parent().find(".list_subcategory_mobile").slideDown();
      $(this)
        .parent()
        .closest(".menu-item-has-children")
        .siblings()
        .find(".list_subcategory_mobile")
        .slideUp();
      return false;
    }
  );

  $(document).on(
    "click",
    ".mobile-menu .menu-item-has-children.active .menu-trigger",
    function () {
      $(this).parent().removeClass("active");
      $(this).parent().find(".list_subcategory_mobile").slideUp();
      return false;
    }
  );

  $(document).on("click", ".section_search .open_search", function () {
    // $('.form-checkbox').removeClass('active');
    $(this).parent().addClass("active");
    $(".new_search").slideDown();
  });
  $(document).on("click", ".section_search.active .close_search", function () {
    $(".section_search").removeClass("active");
    $(".new_search").slideUp();
    // $('.form-checkbox').removeClass('active');
  });

  // End Mobile Nav ///
  var youTubeUrl = $("#video").attr("src");
  $(document).on("click", ".popup__close", function () {
    $(".youtube-video").find("iframe").attr("src", "");
    $(".youtube-video").find("iframe").attr("src", youTubeUrl);
  });

  //start reviews_form //
  $(document).on("click", ".button-submit-review:not(.active)", function () {
    $("#reviews_form").slideUp(300);
    $(this).next().slideDown(300);
    $(this).addClass("active");
  });
  $(document).on("click", ".button-submit-review.active", function () {
    $(this).next().slideUp(300);
    $(this).removeClass("active");
  });
  //End reviews_form //

  // $(document).on('click', '.popup_visible', function() {
  //     $('html, body').css('overflow', 'visible');
  //     $(".overlay").removeClass("overlay_visible");
  //     $(".popup").removeClass("popup_visible");
  //     return false;
  // });

  $(document).on("click", ".nav_single_title", function () {
    var ths = $(this);
    var trgt = $($(this).attr("href"));
    $(".single_faq").removeClass("active");
    $(".nav_single_title").removeClass("active");
    $(this).addClass("active");
    trgt.addClass("active");

    return false;
  });

  // $(document).on('click', '.js-popup-closer', function() {
  //     $('.popup').removeClass('popup_visible');
  //     $('#overlay').removeClass('overlay_visible');
  //     $('html, body').css('overflow', 'visible');
  //     return false;
  // });

  // Start Filter Mobile //

  $(".form-checkbox  .read-more").on("click", function () {
    $(this).parent().addClass("active");
  });
  $(".form-checkbox  .read-less").on("click", function () {
    $(this).parent().removeClass("active");
  });

  $(document).on(
    "click",
    "#popup-filter .section_action .botton_filter",
    function () {
      $(".popup").removeClass("popup_visible");
      $("#overlay").removeClass("overlay_visible");
      $("html, body").css("overflow", "visible");
    }
  );

  // End Filter Mobile //

  $("#playvideo").on("click", function () {
    let videoData = $(this).data("video");
    let videoSource = $("#videoclip").find("#video1");
    videoSource.attr("src", videoData);
    let autoplayVideo = $("#videoclip").get(0);
    autoplayVideo.load();
    autoplayVideo.play();
  });
});

//start cart count //

function increaseValue() {
  var value = parseInt(document.getElementById("number").value, 10);
  value = isNaN(value) ? 0 : value;
  if (
    document.getElementById("number").value !== $("#number").attr("data-max")
  ) {
    value++;
  } else {
    $("#increase").addClass("disabled");
  }
  document.getElementById("number").value = value;
  if (value > 1) {
    $("#decrease").removeClass("disabled");
  }
}

function decreaseValue() {
  var value = parseInt(document.getElementById("number").value, 10);
  value = isNaN(value) ? 1 : value;
  value < 2 ? (value = 2) : "";
  value--;
  document.getElementById("number").value = value;
  if (value == 1) {
    $("#decrease").addClass("disabled");
  }
}
function increaseValueByID(element_id) {
  var value = parseInt(document.getElementById(element_id).value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById(element_id).value = value;
  // if(document.getElementById('number').value!==$('#number').attr('data-max')){
  //     value++;
  //     }
  //     else{
  //         $('#increase').addClass('disabled');
  //     }
  //     document.getElementById('number').value = value;
  if (value > 1) {
    $("#decrease").removeClass("disabled");
  }
  window.location.reload(); //added
}

function decreaseValueByID(element_id) {
  var value = parseInt(document.getElementById(element_id).value, 10);
  value = isNaN(value) ? 1 : value;
  value < 2 ? (value = 2) : "";
  value--;
  document.getElementById(element_id).value = value;
  if (value == 1) {
    $("#decrease").addClass("disabled");
  }
  window.location.reload(); //added
}
//End cart count //

// if($('.player__button').length){
// // start Hero Video //
//         $(document).on('click', '.player__button', function() {
//             $('.player__controls').toggleClass('paused');
//             return false;
//         });
//         $(document).on('click', '.player__video', function() {
//             $('.player__controls').toggleClass('paused');
//             return false;
//         });

//         /* Get our elements */
//         const player       =  document.querySelector('.player');
//         const video        =  player.querySelector('.viewer');
//         const toggle       =  player.querySelector('.toggle');

//         // toggle play/pause
//         function togglePlay() {
//         const method = video.paused ? 'play' : 'pause';
//         video[method]();
//         }

//         // Detect press of spacebar, toggle play
//         function detectKeypress(e) {
//             if (e.keyCode == 32) {
//             togglePlay();
//             } else {
//             return;
//             }
//         }

//         // Update button on play/pause
//         // function updateButton() {
//         // const icon = this.paused ? '' : '';
//         // toggle.textContent = icon;
//         // }

//         /* Hook up the event listeners */

//         // Click events
//         video.addEventListener('click', togglePlay);
//         toggle.addEventListener('click', togglePlay);
//         // fullscreen.addEventListener('click', toggleFullscreen);

//         // Keypress (Play/Pause)
//         window.addEventListener('keydown', detectKeypress);

//         // // Play/Pause events
//         // video.addEventListener('play', updateButton);
//         // video.addEventListener('pause', updateButton);

//         // Detect how far mouse has moved
//         // ranges.forEach(range => range.addEventListener('change', handleRangeUpdate));
//         // ranges.forEach(range => range.addEventListener('mousemove', handleRangeUpdate));

//         // Track scrubbing
//         // let mousedown = false;
//         // progress.addEventListener('click', scrub);
//         // progress.addEventListener('mousemove', (e) => mousedown && scrub(e));

//         // Track when mouse is up/down on scrub bar
//         // progress.addEventListener('mousedown', () => mousedown = true);
//         // progress.addEventListener('mouseup', () => mousedown = false);

// // End Hero Video //
//     }

$(".reviews-info-tabs .tabs li").click(function () {
  $(this).addClass("active").siblings().removeClass("active");
});

$(".reviews-info-tabs .tabs li#trigger-reviews").click(function () {
  $(".review-section").addClass("active");
  $(".product-info").removeClass("active");
});

$(".reviews-info-tabs .tabs li#trigger-info").click(function () {
  $(".product-info").addClass("active");
  $(".review-section").removeClass("active");
});
