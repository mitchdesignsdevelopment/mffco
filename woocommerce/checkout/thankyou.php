<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */
// require_once 'header.php';
// require_once 'theme-parts/main-menu.php';

defined( 'ABSPATH' ) || exit;
?>
<button id="btn-generate">طباعه</button>
<div class="woocommerce-order" id="woocommerceOrder">

	<?php
	if ( $order ) :
global $theme_settings;
		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>
				<?php echo " sorry... failed order please order next time "; ?>

		<?php else : ?>
			<div class="thanks_page">
				<div class="section_head">
					<!-- <img src="<?php //echo $theme_settings['theme_url']; ?>/assets/img/icons/done_order.png" alt=""> -->
					<span class="check_circle_outline"></span>
					<p class="title">طلبكم قيد التنفيذ</p>
					<p class="subtitle">ستصلكم رساله بعد قليل بتأكيد الطلب</p>
				</div>
				<div class="section_thanks_page">
				
					<div class="order_review-content">
						<div class="box-con sticky-box">
							<div id="order_review_thanks" class="woocommerce-checkout-review-order">
								<div class="order-title" style="display:none">
									<h3>
										<?php
										if(!empty($backorder_obj)){
											echo 'Processing Order #'.$order->get_order_number().'<br>';
											echo 'Pre Order #'.$backorder_id;
										}else{
											echo 'Order #'.$order->get_order_number();
										}
										?>
										<span>
											<?php
											if(!empty($backorder_obj) || $order->get_status() == 'backorder'){
												echo '7-12 Working Days for Delivery in Cairo & 7-15 Working Days Outside Cairo';
											}else{
												echo '2-7 Working Days for Delivery in Cairo & 4-10 Working Days Outside Cairo';
											}
											?>
										</span>
									</h3>
									<!-- <a>PAID</a> -->
								</div>
								<table class="shop_table woocommerce-checkout-review-order-table">
									<tbody>
										<tr class="cart_item">
										<?php
										$order_items = $order->get_items();
										if(!empty($backorder_obj)){
											$backorder_items = $backorder_obj->get_items();
											$order_items     = array_merge($order_items, $backorder_items);
										}
										foreach($order_items as $item_id => $item){
											// print_r($item);
											$product    = $item->get_product();
											$price      = $product->get_price();
											$product_id = $item->get_product_id();
											$data       = $item->get_data();
											$quantity   = $data['quantity']; 
											// $order_items_data = array_map( );
											// $custom_field = get_post_meta( $product_id, '_tmcartepo_data', true);
											// print_r($data);
											
											?>
											<td class="product-name">
												<div class="product-thumb-name">
													<a href="<?php echo get_the_permalink($product_id);?>" class="cart_pic"><img width="300" height="300" src="<?php echo get_the_post_thumbnail_url($product_id)?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" sizes="(max-width: 300px) 100vw, 300px"></a>
													<div class="product-name-container">
														<div class="right">
															<h3 class="product-title"><?php echo $product->get_name();?></h3>
															<?php $note = get_field('note',$product->get_id()); if($note):?>
															<span class="product-note"><?php echo $note; ?></span>
															<?php endif;
																if( $product->is_type('variation') ){
																	// Get the variation attributes
																?>
																<dl class="variation">
																<?php
																$variation_attributes = $product->get_variation_attributes();
																// Loop through each selected attributes
																	foreach($variation_attributes as $attribute_taxonomy => $term_slug ){
																		// Get product attribute name or taxonomy
																		$taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
																		// The label name from the product attribute
																		$attribute_name = wc_attribute_label( $taxonomy, $product );
																		// The term name (or value) from this attribute
																		if( taxonomy_exists($taxonomy) ) {
																			$attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
																		} else {
																			$attribute_value = $term_slug; // For custom product attributes
																		}
																		?>
																		<dt class="variation-Fields"><?php //echo $attribute_name;?></dt>
																			<dd class="variation-Fields"><p><?php echo $attribute_value;?></p>
																		</dd>
																		<?php
																	}
																?>
															</dl>
															<?php
																}
															?>
																<div class="single-price">
																<span class="woocommerce-Price-amount amount">
																	<span>سعر المنتج</span>
																	<bdi><?php echo number_format($price,0);?>
																		<span class="woocommerce-Price-currencySymbol"><?php echo $theme_settings['current_currency'];?></span>
																	</bdi>
																</span>
															</div>
														</div>
														<div class="left">
															<div class="product-quantity">
																<span><?php echo 'x'.$quantity;?></span>

															</div>
															<div class="total-price">
																<span class="woocommerce-Price-amount amount">
																	<span>السعر الكلي</span>
																	<bdi><?php echo number_format(intval($price)*intval($quantity),0);?>
																		<span class="woocommerce-Price-currencySymbol"><?php echo $theme_settings['current_currency'];?></span>
																	</bdi>
																</span>
															</div>
														</div>
													</div>
												</div>
											</td>
										<?php } ?>
										</tr>
									</tbody>
									<tfoot>
									<tr class="order-total">
										<th>الشحن</th>
											<td>
												<?php 
												$shipping_total = number_format(floatval($order->get_shipping_total()),0,'.',',');
												echo $shipping_total.' '.$theme_settings['current_currency'];												?>
											</td>
										</tr>
										<tr class="order-total">
											<th>الإجمالي</th>
											<td>
												<?php
													$total = number_format($order->get_total(),0);
													echo $total.' '.$theme_settings['current_currency'];
												?>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="left">
						<div class="woocommerce-customer-details">
							<div class="row">
								<label for="">بياناتي</label>
								<p><?php echo $order->get_billing_first_name().' '.$order->get_billing_last_name();?></p>
								<p><?php echo $order->get_billing_phone();?></p>
								<p><?php echo $order->get_billing_email();?></p>
							</div>
							<div class="row">
								<label for="">عنوان الشحن:</label>
								<p><?php echo $order->get_formatted_billing_address();?></p>
							</div>
							<div class="row">
								<label for="">تفاصيل الدفع:</label>
								<!-- <p>الدفع عند الاستلام<span class="img"></span> <span class="img"></span></p> -->
								<p><?php echo $order->get_payment_method_title();?><span class="img"></span> <span class="img"></span></p>
								
							</div>
							
						</div>

						<?php //do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
					</div>
				</div>
			</div>

		<?php endif; ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>


<!-- <?php //require_once 'footer.php';?> -->
