<?php
require_once 'header.php';
mitch_validate_logged_in();
global $current_user;
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_myaccount">
    <div class="grid">
      <div class="section_title">
        <h1><?php echo $fixed_string['myaccount_page_title'];?></h1>
      </div>
      <div class="page_content">
        <?php include_once 'theme-parts/myaccount-sidebar.php';?>
        <div class="dashbord">
            <div class="profile_edit">
              <div class="top">
                <?php echo get_avatar( $current_user->ID, 80 );?>
                <!-- <a href=""  class="edit_img_pro" >تعديل</a> -->
              </div>
              <form id="profile_settings" action="#" method="post">
                    <div class="field">
                        <label for=""><?php echo $fixed_string['checkout_form_firstname'];?><span>*</span></label>
                        <input type="text" name="first_name" value="<?php echo get_user_meta($current_user->ID, 'first_name', true );?>" required>
                    </div>
                    <div class="field">
                        <label for=""><?php echo $fixed_string['checkout_form_family'];?><span>*</span></label>
                        <input type="text" name="last_name" value="<?php echo get_user_meta($current_user->ID, 'last_name', true );?>" required>
                    </div>
                    <div class="field full">
                        <label for=""><?php echo $fixed_string['checkout_form_email'];?><span>*</span></label>
                        <input type="email" name="user_email" value="<?php echo $current_user->user_email;?>" required>
                    </div>
                    <div class="field full">
                        <label for=""><?php echo $fixed_string['checkout_form_phone'];?><span>*</span></label>
                        <input type="text" name="phone_number" value="<?php echo get_user_meta($current_user->ID, 'phone_number', true);?>" required>
                    </div>
                    <div class="field full">
                        <label>كلمة المرور الحالية (اترك الحقل فارغاً إذا كنت لا تودّ تغييرها)</label>
                        <input type="password" name="old_password">
                    </div>
                    <div class="field full">
                        <label for="">كلمة المرور الجديدة (اترك الحقل فارغاً إذا كنت لا تودّ تغييرها)<span>*</span></label>
                        <input type="password" name="new_password">
                    </div>
                    <div class="field full">
                        <label for="">تأكيد كلمة المرور الجديدة<span>*</span></label>
                        <input type="password" name="confirm_password">
                    </div>
                    <button type="submit">حفظ التغييرات</button>
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
