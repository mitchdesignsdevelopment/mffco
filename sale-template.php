<?php
/* Template Name: Deals & Offers */
require_once 'header.php';
global $post; //var_dump($post);
$allproducts = 100;
$posts_per_page = 20;
$custom_sale_products = get_field('sale_products');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php'; ?>
  <!--start page-->
  <div class="site-content page_list">
    <div class="grid">
      <div class="list_content sale_templet">
        <div class="product list">
          <div class="grid">
            <div class="section_title">
              <div class="text">
              <h2><?php echo $post->post_title; ?></h2>
              </div>
              
            </div>
            <div class="product_container">
              <ul class="products_list products" data-slug="shop" data-cat="" data-type="sale" data-count="20" data-page="1" data-posts="<?php echo $allproducts; ?>" data-search="<?php echo $_GET['s'] ?>" data-sort="<?php echo (isset($_GET['orderby'])) ? $_GET['orderby'] : 'date'; ?>">
                <?php
                // $sale_products = wc_get_product_ids_on_sale();
                /*$args = array(
                    'fields'         => 'ids',
                    'post_type'      => 'product',
                    'posts_per_page' => 8,
                    'meta_query'     => array(
                        'relation' => 'OR',
                        array( // Simple products type
                            'key'           => '_sale_price',
                            'value'         => 0,
                            'compare'       => '>',
                            'type'          => 'numeric'
                        ),
                        array( // Variable products type
                            'key'           => '_min_variation_sale_price',
                            'value'         => 0,
                            'compare'       => '>',
                            'type'          => 'numeric'
                        )
                    ),
                    'post__in' => wc_get_product_ids_on_sale()
                );
                
                $sale_products = get_posts($args);*/
                $args = array(
                  'status' => 'publish',
                  'limit' => 20,
                  'return' => 'ids',
                  'meta_query' => array(
                    'relation' => 'OR',
                    array(
                      'key' => '_sale_price',
                      'value' => 0,
                      'compare' => '>',
                      'type' => 'numeric',
                    ),
                    array(
                      'key' => '_min_variation_sale_price',
                      'value' => 0,
                      'compare' => '>',
                      'type' => 'numeric',
                    ),
                  ),
                );

                $sale_products = wc_get_products($args);

                // var_dump($sale_products);
                // exit();
                foreach ($custom_sale_products as $product_id) {
                  $product_data = mitch_get_short_product_data($product_id);
                  // $product = wc_get_product($product_id);

                  // $product_data = array(
                  //   'product_id'            => $product_id,
                  //   'product_title'         => get_the_title($product_id),
                  //   'product_type'         => $product->get_type(),
                  //   'product_price'         => $product->get_price_html(),
                  //   'product_widget_background'  => get_field('widget_background_color', $product_id),
                  //   'hide_add_to_cart'  => get_field('hide_add_to_cart_button', $product_id),
                  //   // 'produ÷ct_price_b4_sale' => $product->get_regular_price(),
                  //   'product_sale_price'    => $product->get_sale_price(),
                  //   'product_image'         => (wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0]) ? wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0] : $theme_settings['theme_url'] . '/assets/img/place-holder.jpg',
                  //   'product_url'           => get_permalink($product_id),
                  // );

                  // // var_dump($product_id);
                  // $product = wc_get_product($product_id);
                  // $product_data = array(
                  //   'product_id'            => $product_id,
                  //   'product_title'         => get_the_title($product_id),
                  //   'product_type'         => $product->get_type(),
                  //   'product_price'         => $product->get_price_html(),
                  //   'product_widget_background'  => get_field('widget_background_color', $product_id),
                  //   'hide_add_to_cart'  => get_field('hide_add_to_cart_button', $product_id),
                  //   // 'produ÷ct_price_b4_sale' => $product->get_regular_price(),
                  //   'product_sale_price'    => $product->get_sale_price(),
                  //   'product_image'         => (wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0]) ? wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0] : $theme_settings['theme_url'] . '/assets/img/place-holder.jpg',
                  //   'product_url'           => get_permalink($product_id),
                  // );

                  // var_dump($product_data);

                  // $product_data = mitch_get_short_product_data($product_id);
                  // var_dump($product_data);
                  // if ($args) {
                  //   $product_data = $args;
                  //   // $product_id=$product_data['product_id'];
                  //   // $discount = get_post_meta($product_id, '_sale_price', true); // Get the discount price
                  //   // $regular_price = get_post_meta($product_id, '_price', true); // Get the regular price
                  //   // if($discount&&$regular_price){
                  //   // $discount_percentage = round((($regular_price - $discount) / $regular_price) * 100); }// Calculate the discount percentage

                  //   // echo $discount_percentage . '%'; // Display the discount percentage with a percent sign

                  // }
                  // print_r($product_data);
                  // include_once 'theme-parts/product-widget.php';
                  wc_get_template( 'theme-parts/product-widget.php',$product_data );
                ?>
                  
                <?php  }
                ?>

              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php
// if ($allproducts > $posts_per_page) : 
?>

<!-- <div class="spinner" data-slug="<?php // echo (!$cat_slug) ? '' : $cat_slug; 
                                      ?>" data-type="<?php //echo (!$cat_slug) ? 'shop' : $term->taxonomy; 
                                                      ?>" data-count="20" data-page="1" data-posts="<?php //echo $allproducts; 
                                                                                                                                                ?>" data-sort="<?php //echo (isset($_GET['orderby'])) ? $_GET['orderby'] : 'popularity'; 
                                                                                                                                                                                                                      ?>"> -->
<!--  </div> -->
<?php //endif; 
?>
<?php require_once 'footer.php'; ?>