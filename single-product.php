<?php require_once 'header.php';?>
<?php
$single_product_data = mitch_get_product_data(get_the_id());
// echo "<pre>";var_dump($single_product_data['main_data']->get_gallery_image_ids()[0]);echo "</pre>";exit;
// mitch_validate_single_product($single_product_data['main_data']);
// mitch_test_vars(array());
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div id="product_<?php echo $single_product_data['main_data']->get_id();?>_block" class="single_page" data-sku="<?php echo $single_product_data['main_data']->get_sku();?>" data-id="<?php echo $single_product_data['main_data']->get_id();?>">
    <div class="section_item grid">
      <div id="single_product_alerts" class="ajax_alerts"></div>
      <div class="content">
        <?php include_once 'theme-parts/single-product/gallary-section.php';?>
        <?php include_once 'theme-parts/single-product/info-section.php';?>
      </div>
    </div>
   
    <?php include_once 'theme-parts/related-single.php';?>
    <?php include_once 'theme-parts/reviews-products.php';?>
    <?php include_once 'theme-parts/related-products.php';?>
    <?php //include_once 'theme-parts/accessories.php';?>
  </div>
  <!--end page-->
</div>
<?php //require_once 'theme-parts/footer-banners.php';?>
<?php require_once 'footer.php';?>
<script>
function select_star(start_value){
  $("#rating").val(start_value);
}
</script>
