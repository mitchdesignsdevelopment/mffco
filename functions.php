<?php
require_once 'includes/global-functions.php';
require_once 'includes/translate-functions.php';
require_once 'includes/products-functions.php';
require_once 'includes/wishlist-functions.php';
require_once 'includes/cart-functions.php';
require_once 'includes/checkout-functions.php';
require_once 'includes/myaccount-functions.php';
require_once 'includes/wpadmin-functions.php';
require_once 'includes/pages-functions.php';
// require_once 'includes/cities.php';
add_action('wp_ajax_order_again', 'order_again');

function order_again()
{

	//global $woocommerce;
	WC()->cart->empty_cart();
	$order_id = $_POST['order_id'];
	$order = new WC_Order($order_id);
	$order_items = $order->get_items();
	foreach ($order->get_items() as $item_id => $item) {
		$product_obj = $item->get_product();
		echo $product_obj->get_id() . '<br>';
		WC()->cart->add_to_cart($product_obj->get_id(), $item->get_quantity());
	}
	wp_die();
}