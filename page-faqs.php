<?php
require_once 'header.php';
$faq_items = get_field('faq_items', get_the_id());
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content style_page_form">
    <div class="grid">
      <div class="page_faq">
          <div class="section_title">
            <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/mffco_icon.png" alt="" width="60">
            <h1><?php echo $fixed_string['faq_page_title'];?></h1>
          </div>
          <div class="faq_content">
            <?php if(!empty($faq_items)){  $count=1; if(!empty($faq_items[0]['title'])):?>
                <div class="nav_menu">
                  <?php  foreach($faq_items as $faq_item){ 
                  ?>
                      <a href="#single_faq_<?php echo $count; ?>" class="nav_single_title <?php echo($count == '1')? 'active' : ''; ?>" ><?php echo $faq_item['title'];?></a>
                      <?php $count++;  } ?>
                </div>
            <?php endif;} ?>

            <div class="section_faq">
              <?php if(!empty($faq_items)) { $count=1;
                    foreach($faq_items as $faq_item){ ?>
                  <div id="single_faq_<?php echo $count; ?>" class="single_faq <?php echo($count == '1')? 'active' : ''; ?>">
                      <!-- <h3 class="title_faq"><?php // echo $faq_item['title'];?></h3> -->
                      <?php if($faq_item['questions_&_answers']): foreach($faq_item['questions_&_answers'] as $qa):?>
                      <div class="content faq">
                        <h4 class="question">
                          <?php echo $qa['question'] ?>
                        </h4>
                        <p class="answer">
                          <?php echo $qa['answer'] ?>
                        </p>
                      </div>
                      <?php endforeach; endif;?>
                  </div>
              <?php $count++; } } ?>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
