<?php
require_once 'header.php';
$page_content = get_field('home_page');
// $product = wc_get_product( 7702 );
// $discounts = $product->get_discounts();
// var_dump($discounts);
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
    <div class="site-content home">
        <div class="page_home">
                <div class="hero_slider">
                    <?php if($page_content['hero_section']['banners']): foreach($page_content['hero_section']['banners'] as $banner):?>
                        <?php if($banner['title2'] ){?>
                        <div class="single_hero">
                            <img src="<?php echo $banner['banner'];?>" alt="">
                            <div class="section_hero_box">
                                <div class="hero_box">
                                    <div class="sec_top">
                                        <h4 class="title"><?php echo $banner['title2'];?></h4>
                                        <p class="price"><?php echo $banner['price'];?>
                                            <?php if($banner['discount']):?>
                                            <span class="discount"><?php echo $banner['discount'];?></span>
                                            <?php endif;?>
                                        </p>
                                    </div>
                                
                                    <?php //echo $banner['points'];?>
                                
                                    <?php if($banner['button']):?>
                                    <a href="<?php echo $banner['button']['url'];?>"><?php echo $banner['button']['title'];?></a>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <?php  } else {  ?>

                            <a class="single_hero" href="<?php echo $banner['button']['url'];?>" >
                                <img src="<?php echo $banner['banner'];?>" alt="">
                            </a>
                        <?php } ?>
                    <?php endforeach; endif;?>
                </div>
                <div class="grid">
                    <div class="section_category">
                        <div class="section_title">
                            <p class="subtitle"><?php echo $page_content['categories_section']['title'];?></p>
                            <h3 class="title"><?php echo $page_content['categories_section']['subtitle'];?></h3>
                        </div>
                        <div class="all_category">
                            <?php if($page_content['categories_section']['categories']): foreach($page_content['categories_section']['categories'] as $category):?>
                                    <a href="<?php echo get_term_link($category);?>" class="single_category">
                                        <img src="<?php echo get_field('home_image',$category);?>" alt="">
                                        <h5><?php echo $category->name;?></h5>
                                    </a>
                            <?php endforeach; endif;?>
                        </div>
                    </div>
                    <div class="section_new">
                        <div class="section_title">
                            <p class="subtitle">أثاث عالي الجودة </p>
                            <h3 class="title">المنتجات الأحدث من مفكو</h3>
                        </div>
                        <div class="product home">
                            <div class="product_container">
                                <ul class="products_list">
                                    <?php
                                    $new_arrival_ids = mitch_get_new_arrival_products_ids(4);
                                    if(!empty($new_arrival_ids)){
                                    //    $prod_anim_count = 1000;
                                    foreach($new_arrival_ids as $new_arrival_id){
                                        $product_data = mitch_get_short_product_data($new_arrival_id);
                                        include 'theme-parts/product-widget.php';
                                    }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section_slider_image">
                    <div class="grid">
                        <div class="section_head">
                            <div class="section_title">
                                <h3>أثاث ممتاز للعائلة الحديثة</h3>
                                <p>في مفكو نقدم أثاث عالي الجودة بأسلوب أنيق وفاخر منذ ٥٠ عام</p>
                            </div>
                            <div class="section_nav_image">
                            <?php if($page_content['bundle_product_section']['bundle_banners']): $count=1; foreach($page_content['bundle_product_section']['bundle_banners'] as $bundle_banner):?>
                                <div href="#single_nav_<?php echo $count;?>" class="single_nav_img <?php echo($count==1)? 'active':'';?>">
                                    <img src="<?php echo $bundle_banner['image'];?>" alt="">
                                </div>
                            <?php $count++; endforeach; endif;?>
                            </div>
                        </div>
                        <div class="section_image_hotspot">
                        <?php if($page_content['bundle_product_section']['bundle_banners']): $count=1; foreach($page_content['bundle_product_section']['bundle_banners'] as $bundle_banner):?>
                            <div id="single_nav_<?php echo $count;?>"  class="image_hotspot <?php echo($count==1)? 'active':'';?>">
                                <img src="<?php echo $bundle_banner['image'];?>" alt="">
                                <?php if($bundle_banner['bundle_products']): foreach($bundle_banner['bundle_products'] as $bundle_product):?>
                                <div class="single_point" style="<?php echo $bundle_product['point_style'];?>">
                                    <div class="point_icon" >
                                        <span class="icon"></span>
                                    </div>
                                    <div class="product_widget <?php echo ($bundle_product['right_left'])?'right':'left';?>" style="<?php echo $bundle_product['widget_style'];?>">
                                        <a href="<?php echo get_the_permalink($bundle_product['product']);?>" class="product_widget_box">
                                            <div class="img">
                                                <img src="<?php echo get_the_post_thumbnail_url($bundle_product['product']);?>" alt="">
                                            </div>
                                            <div class="text">
                                                <div class="sec_info">
                                                    <h3 class="title"><?php echo get_the_title($bundle_product['product']);?></h3>
                                                    <p class="price"><?php echo get_post_meta($bundle_product['product'],'_price',true).' EGP';?></p>
                                                </div>
                                                <div class="open_widget">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <?php endforeach; endif; ?>
                                <p class="text"><?php echo $bundle_banner['text'];?></p>
                                <a href="<?php echo $bundle_banner['button']['url'];?>" class="link"><?php echo $bundle_banner['button']['title'];?></a>
                            </div>
                            <?php $count++; endforeach; endif;?>
                        </div>
                    </div>
                </div>
                <div class="grid">
                    <div class="section_offers">
                        <div class="all_offers">
                        <?php if($page_content['banners_section']['banners']): foreach($page_content['banners_section']['banners'] as $banner):?>
                            <div class="single_offer">
                                <a href="<?php echo $banner['button_url'];?>" class="content">
                                    <img src="<?php echo $banner['image'];?>" alt="">
                                    <p><?php echo $banner['title'];?></p>
                                    <span><?php echo $banner['button_text'];?></span>
                                </a>
                            </div>
                        <?php endforeach; endif;?>
                        </div>
                    </div>
                    <div class="section_video">
                        <div class="player">
                            <div class="info">
                                <h3><?php echo $page_content['about_section']['title'];?></h3>
                                <p><?php echo $page_content['about_section']['content'];?></p>
                            </div>
                            <div class="video-box js-videoWrapper">
                                <div  class="bg" style="background-image: url(<?php echo $page_content['about_section']['video_image'];?>);"><button id="playvideo" class="player js-videoPlayer"></button></div>
                                <div class="youtube-video">
                                    <video id="videoclip" class="videoIframe js-videoIframe" width="100%" height="100%" controls>
                                        <source id="video1" src="<?php echo $page_content['about_section']['video'];?>" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                           
                        </div>
                        <a href="<?php echo $page_content['about_section']['button']['url'];?>"><?php echo $page_content['about_section']['button']['title'];?></a>
                    </div>
                </div>
                <div class="section_bk">
                    <div class="grid">
                        <div class="section_new">
                            <div class="section_title">
                                <p class="subtitle">أثاث عالي الجودة </p>
                                <h3 class="title">المنتجات الأكثر مبيعا من مفكو</h3>
                            </div>
                            <div class="product home">
                                <div class="product_container">
                                    <ul class="products_list">
                                        <?php
                                            $new_arrival_ids = mitch_get_new_arrival_products_ids(4);
                                            if(!empty($new_arrival_ids)){
                                        //    $prod_anim_count = 1000;
                                            foreach($new_arrival_ids as $new_arrival_id){
                                                $product_data = mitch_get_short_product_data($new_arrival_id);
                                                include 'theme-parts/product-widget.php';
                                            }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <?php //require_once 'theme-parts/footer-banners.php';?>
    </div>
  <!--end page-->
  <a class="js-popup-opener" href="#popup-banner" style="display:none;"></a>
</div>
<?php require_once 'footer.php';?>
