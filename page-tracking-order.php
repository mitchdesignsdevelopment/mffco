<?php
require_once 'header.php';
mitch_validate_logged_in();
?>
<div id="page" class="site" style="">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_track_order">
    <div class="grid">
      <?php
      if(!isset($_GET['order_id'])){
        ?>
        <div class="section_track_order">
          <?php include 'theme-parts/tracking-order-form.php';?>
        </div>
        <?php
      }else{
        $order_id = intval($_GET['order_id']);
        if(!empty($order_id)){
          if(get_post_meta($order_id, '_customer_user', true) != get_current_user_id()){
            // wp_redirect(home_url('my-account/tracking-order'));
            // exit;
            ?>
            <div class="section_track_order">
              <div class="alert alert-danger"><?php echo $fixed_string['alert_tracking_order_not_found'];?></div>
              <?php include 'theme-parts/tracking-order-form.php';?>
            </div>
            <?php
          }else{
            $order_obj    = wc_get_order($order_id);
            $order_status = str_replace('wc-','',get_post_status($order_id));
            if($order_status == 'processing'){
              $processing_completed = 'completed';
            }elseif($order_status == 'ready'){
              $processing_completed = 'completed';
              $ready_completed      = 'completed';
            }elseif($order_status == 'shipped'){
              $processing_completed = 'completed';
              $ready_completed      = 'completed';
              $shipped_completed    = 'completed';
            }elseif($order_status == 'cancelled'){
              $cancelled_completed = 'completed';
            }elseif($order_status == 'completed'){
              $processing_completed = 'completed';
              $ready_completed      = 'completed';
              $shipped_completed    = 'completed';
              $completed_completed  = 'completed';
            }
            ?>
            <div class="section_track_order">
              <div class="box" style="margin-top: 20px;">
                <div class="section_title order_num">
                  <h2><?php echo $fixed_string['myaccount_page_orders_order_no2'];?> <?php echo $order_id;?></h2>
                  <p>
                  <?php
                  if($order_status == 'cancelled'){
                    echo $fixed_string['order_cancelled_txt1'].get_post_meta($order_id, 'order_cancelled_date', true);
                  }elseif($order_status == 'refunded'){
                    echo $fixed_string['order_refunded_txt1'].get_post_meta($order_id, 'order_refunded_date', true);
                  }else{
                    echo $fixed_string['order_status_'.$order_status];
                  }
                  ?>
                  </p>
                </div>
                <?php if(in_array($order_status, array('processing', 'ready', 'shipped', 'completed'))){ ?>
                <div class="container">
                  <div class="section_order_status">
                    <div class="order-tracking <?php if(isset($processing_completed)){echo $processing_completed;}?>">
                      <span class="is-complete"></span>
                      <p>
                        <?php echo $fixed_string['order_processing_txt'];?>
                        <span><?php echo $order_obj->get_date_created()->date("F j, Y");?> <?php echo $fixed_string['order_processing_txt2'];?></span></p>
                    </div>
                    <div class="order-tracking <?php if(isset($ready_completed)){echo $ready_completed;}?>">
                      <span class="is-complete "></span>
                      <p><?php echo $fixed_string['order_ready_txt1'];?><span><?php echo $fixed_string['order_ready_txt2'];?></span></p>
                    </div>
                    <div class="order-tracking <?php if(isset($shipped_completed)){echo $shipped_completed;}?>">
                      <span class="is-complete"></span>
                      <p><?php echo $fixed_string['order_shipped_txt1'];?><span><?php echo $fixed_string['order_shipped_txt2'];?></span></p>
                    </div>
                    <div class="order-tracking <?php if(isset($completed_completed)){echo $completed_completed;}?>">
                      <span class="is-complete"></span>
                      <p><?php echo $fixed_string['order_completed_txt1'];?><span><?php echo $fixed_string['order_completed_txt2'];?></span></p>
                    </div>
                  </div>
                </div>
                <?php }?>
              </div>
              <div class="order_links">
                <ul>
                  <li>
                    <a href="<?php echo home_url('my-account/orders-list/?order_id='.$order_obj->get_id().'');?>">عرض تفاصيل الطلب</a>
                  </li>
                  <li>
                    <a id="cancel_order_button" data-order-id="<?php echo $order_obj->get_id();?>" class="remove js-popup-opener" href="#popup-remove-order<?php //echo home_url('my-account/cancel-order/?order_id='.$order_obj->get_id().'');?>">إلغاء الطلب</a>
                  </li>
                  <li>
                    <a href="<?php echo home_url('my-account/tracking-order/');?>">تتبع حالة طلب آخر</a>
                  </li>
                </ul>
              </div>
            </div>
            <?php
          }
        }else{
          ?>
          <div class="section_track_order">
            <div class="alert alert-danger"><?php echo $fixed_string['alert_global_error'];?></div>
          </div>
          <?php
        }
      }
      ?>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script>
$('#cancel_order_button').on('click', function () {
  var order_id     = parseInt($(this).data('order-id'));
  var proceed_url  = '<?php echo home_url('my-account/cancel-order/');?>?order_id='+order_id;
  var tracking_url = '<?php echo home_url('my-account/tracking-order/');?>?order_id='+order_id;
  $('#cancel_order_proceed_button').attr('href', proceed_url);
  $('#cancel_order_track_button').attr('href', tracking_url);
});
</script>
